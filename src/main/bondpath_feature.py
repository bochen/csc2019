import numpy as np
import scipy.stats
import sys
import myutils
from rdkit import Chem
import molutils

class BondPathFeat():
    
    def __init__(self, mol, bondpath):
        self.mol = mol
        self.bondpath = bondpath
        self.pad_bondpath = list(bondpath)
        while len(self.pad_bondpath) < 4:
            self.pad_bondpath.append(None)
        self.conformer = mol.GetConformer()

    def set_if_not_exists(self, name, fun):
        if hasattr(self, name):
            return getattr(self, name)
        else:
            # print ("call fun for " + name)
            value = fun()
            setattr(self, name, value)
            return value 

    def _xyz(self, i):
        return molutils.mol_xyz(self.mol, i, self.conformer)
    
    def _atom_distance(self, i, j):
        return molutils.distance_2points(self._xyz(i), self._xyz(j))
    
    def _triangle_distance(self, i, j, k):
        return molutils.area_3points(self._xyz(i), self._xyz(j), self._xyz(k))    
    
    def _bond_length(self, i, j):
        assert molutils.has_bond(self.mol, i, j), str(i) + " " + str(j)
        return self._atom_distance(i, j)
    
    def _GetBondLengths(self):

        def f():
            pairs = list(zip(self.bondpath[:-1], self.bondpath[1:]))
            ret = []
            for i, j in pairs:
                ret.append(self._bond_length(i, j))
            while len(ret) < 4:
                ret.append(np.nan)
            return ret
            
        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)

    def GetAtomDistance(self):

        def f():
            ret = []
            for i, vi in enumerate(self.pad_bondpath):
                for j, vj in enumerate(self.pad_bondpath):
                    if (i < j):
                        if(vi is not None and vj is not None):
                            ret.append(self._atom_distance(vi, vj))
                        else:
                            ret.append(np.nan)
            return ret
            
        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)
            
    def GetAtomDistanceStat(self):
        return [np.nanmean(self.GetAtomDistance()), np.nanmin(self.GetAtomDistance()), np.nanmax(self.GetAtomDistance())]

    def GetTriangleArea(self):

        def f():
            ret = []
            for i, vi in enumerate(self.pad_bondpath):
                for j, vj in enumerate(self.pad_bondpath):
                    for k, vk in enumerate(self.pad_bondpath):
                        if (i < j and j < k):
                            if(vi is not None and vj is not None and vk is not None):
                                ret.append(self._triangle_distance(vi, vj, vk))
                            else:
                                ret.append(np.nan)
            return ret
            
        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)

    def GetTriangleAreaStat(self):
        return [np.nanmean(self.GetTriangleArea()), np.nanmin(self.GetTriangleArea()), np.nanmax(self.GetTriangleArea())]
    
    def GetTorsionAngle(self):

        def f():
            if len(self.bondpath) != 4:
                return np.nan
            
            points = [self._xyz(i) for i in self.bondpath]
            
            return molutils.torsion_angle(*points)
            
        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)

    def GetVolume(self):

        def f():
            if len(self.bondpath) != 4:
                return np.nan
            
            points = [self._xyz(i) for i in self.bondpath]
            
            return molutils.tetrahedron_volume(*points)
            
        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)
        
    def GetBondAngles(self):

        def f():
            pairs = list(zip(self.bondpath[:-2], self.bondpath[1:-1], self.bondpath[2:]))
            ret = []
            for i, j, k in pairs:
                a, b, c = self._xyz(i), self._xyz(j), self._xyz(k)
                
                ret.append(molutils.angle_3points(a, b, c))
            while len(ret) < 2:
                ret.append(np.nan)
            return ret
            
        prop_name = "_" + sys._getframe().f_code.co_name        
        return self.set_if_not_exists(prop_name, f)
            
    def GetBondPathLength(self):
        return [np.nansum(self._GetBondLengths()), np.nanmin(self._GetBondLengths()), np.nanmax(self._GetBondLengths())]

    
FUNCTIONS = None
COLUMNS = None


def make_feature(mol, bondpath):
    
    assert len(bondpath) > 1 and len(bondpath) < 5

    obj = BondPathFeat(mol, bondpath)
    
    global FUNCTIONS
    global COLUMNS
    
    if not FUNCTIONS:
        FUNCTIONS = [u for u in dir(obj) if u.startswith("Get")]
    
    feats = []
    names = []

    def f(u, obj):
        fn = getattr(obj, u)
        v = fn()
        if isinstance(v, list):
            for i, vv in enumerate(v):
                names.append("{}_{}".format(u, i))
                feats.append(vv)
        else:
            if not COLUMNS:
                names.append(u)
            feats.append(v)         

    for name in FUNCTIONS:
        f(name, obj)

    if not COLUMNS:
        COLUMNS = names
    # print(list(zip(COLUMNS, feats)))
    return np.array(feats, dtype=np.float32)
    

if __name__ == '__main__':
    from rdkit.Chem import AllChem
    mol123 = Chem.MolFromSmiles('c1nccc2n1ccc2')
    AllChem.Compute2DCoords(mol123)
    AllChem.EmbedMolecule(mol123)
    # print(Chem.mol123Tomol123Block(mol123))
    lst = []
    feat = make_feature(mol123, [ 1, 0])
    print(len(feat))
    lst.append(feat)
    feat = make_feature(mol123, [ 1, 0, 5])
    print(len(feat))
    lst.append(feat)
    feat = make_feature(mol123, [ 1, 0, 5, 6])
    print(len(feat))
    lst.append(feat)
    import pandas as pd 
    df = pd.DataFrame(lst, columns=COLUMNS)    
    print (df)

