import fastparquet 
import pandas as pd 
import numpy as np
import config
import myutils
from task import Task
import os
from joblib import Parallel, delayed
from multiprocessing import Pool
import sys
import data
import molutils
from rdkit import Chem
from tqdm import tqdm 
import pickle
import molecule_feature

logger = myutils.get_logger("step11_make_generated_mols")


def process_one_for_make_generated_mol(ab):
    mol, sp = ab
    mols = molutils.induced_mols(mol, sp)
    # ##ret = [Chem.MolToMolBlock(u) for u in mols]
    ret = [u.ToBinary() for u in mols]
    while len(ret) < 30:
        ret.append(b"")
    
    return ret


def make_generated_mols():
        
    def f(logger):
        molsdict = data.load_mols()
        bondpaths = data.load_bond_paths()
        
        lst = []
        for i in tqdm(range(len(bondpaths))[:]):
            row = bondpaths.iloc[i]
            molname = row['molname']
            sp = row['shorted_path']
            idx = row['id']
            if molname not in molsdict:
                logger.error("mol {} not found".format(molname))
            submolblocks = process_one_for_make_generated_mol((molsdict[molname], sp))
            submolblocks.insert(0, idx)
            lst.append(submolblocks)
        df = pd.DataFrame(lst, columns=['id'] + ["mol-" + str(u) for u in range(30)]).set_index('id')
        
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
            
        outname = os.path.join(config.INPUT_PATH, "traintest_submols.parq")
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY', row_group_offsets=1000 * 1000)
        
    task = Task('make_generated_mols', lambda u: f(u))
    task()
    

def transform_mols_to_pkl():

    def save_part(part, outpath, logger):
        logger.info("write {} records to {} ".format(len(part), outpath)) 
        myutils.save_zipped_pickle(part, outpath)
        return len(part)
        
    def f(logger):
        filepath = os.path.join(config.INPUT_PATH, "traintest_submols.parq")
        df = fastparquet.ParquetFile(filepath).to_pandas()
        logger.info("df.shape=" + str(df.shape))
        n_row_per_file = int(df.shape[0] / 10) + 1;
        indexes = list(df.index)
        np.random.shuffle(indexes)
        
        part = {}
        k = 0
        N = 0
        for i in range(len(indexes)):
            idx = indexes[i]
            row = list(df.loc[idx])
            part[idx] = row 
            if len(part) >= n_row_per_file:
                outpath = os.path.join(config.INPUT_PATH, config.NNI_PATH, "traintest_submols_part_{}.pklz".format(k))
                N = N + save_part(part, outpath, logger)
                part = {}
                k = k + 1
        
        if len(part) > 0:
            outpath = os.path.join(config.INPUT_PATH, config.NNI_PATH, "traintest_submols_part_{}.pklz".format(k))
            N = N + save_part(part, outpath, logger)
            
        logger.info("Total written {} records".format(N))
                
    task = Task('transform_mols_to_pkl', lambda u: f(u))
    task()


def make_mol_feat_mean():
    
    import molecule_feature
    functions = [molecule_feature.make_RDKitDescriptors_feature, molecule_feature.make_CoulombMatrixEig_feature,
               molecule_feature.make_MorganFingerprint_feature, molecule_feature.make_AtomPairFingerprint_feature,
               molecule_feature.make_TopologicalTorsionFingerprint_feature]
    function_names = [str(fun.__name__) for fun in functions]
    
    def make_features(mol):
        lst = {}
        for fun, funname in zip(functions, function_names):
            a = fun(mol)
            lst[funname] = a.astype(np.float64);
        return lst
            
    def update_sum(feat, sum_x, sum_x2, N):
        this_sum_x = feat 
        this_sum_x2 = {k:v * v for k, v in feat.items()}
        
        if sum_x is None:
            return this_sum_x, this_sum_x2, 1
        else:
            N += 1
            sum_x = {name: feat[name] + sum_x[name] for name in function_names}
            sum_x2 = {name: feat[name] ** 2 + sum_x2[name] for name in function_names}
            return sum_x, sum_x2, N 
        
    def f(logger):
        filepath = os.path.join(config.INPUT_PATH, "traintest_submols.parq")
        df = fastparquet.ParquetFile(filepath).to_pandas()
        logger.info("df.shape=" + str(df.shape))
        
        sum_x = None 
        sum_x2 = None
        N = 0
        indexes = list(df.index)
        np.random.shuffle(indexes)
        for i in tqdm(indexes[:1000 * 100]):
            row = df.loc[i].values
            for b in row:
                if(b):
                    mol = Chem.Mol(b)
                    feat = make_features(mol)
                    sum_x, sum_x2, N = update_sum(feat, sum_x, sum_x2, N);
        
        feat_mean = {k:v / N for k, v in sum_x.items()}
        feat_stdev = {k: np.sqrt(sum_x2[k] / N - feat_mean[k] ** 2)  for k in function_names} 
        
        results = {'mean': feat_mean, 'std': feat_stdev, "n":N}
        
        outname = os.path.join(config.INPUT_PATH, "traintest_submols_feat_mean.pkl")

        with open(outname, 'wb') as fout: 
            pickle.dump(results, fout)
                
    task = Task('make_mol_feat_mean', lambda u: f(u))
    task()


def process_one_4_make_mol_seq_features(idx, vv):

    def make_features(mol):
        lst = []
        for fun in [molecule_feature.make_RDKitDescriptors_feature, molecule_feature.make_CoulombMatrixEig_feature,
                   molecule_feature.make_MorganFingerprint_feature, molecule_feature.make_AtomPairFingerprint_feature,
                   molecule_feature.make_TopologicalTorsionFingerprint_feature]:
            a = fun(mol)
            # print (str(fun.__name__))
            lst.append(a)
        return np.concatenate(lst).astype(np.float32)

    subarr = []
    for v in vv:
        if v:
            a = Chem.Mol(v)
            b = make_features(a)
            subarr.append(b)
        else:
            subarr.append(np.zeros(3213))
    subarr = np.array(subarr, dtype=np.float32)
    assert subarr.shape[1] == 3213
    assert subarr.shape[0] == 30
    subarr = myutils.compress_np_array(subarr)
    return idx, subarr

    
def make_mol_seq_features(file_no):

    def read_part(no, logger):
        
        outpath = os.path.join(config.INPUT_PATH, config.NNI_PATH, "traintest_submols_part_{}.pklz".format(no))
        logger.info("reading " + outpath)
        part = myutils.load_zipped_pickle(outpath) 
        return part

    def f(logger):
        
        part = read_part(file_no, logger)
        if False:
            arr = []
            indexes = []
            for idx, vv in tqdm(part.items()):
                idx, subarr = process_one_4_make_mol_seq_features(idx, vv)
                indexes.append(idx)
                arr.append(subarr)
        else:
            results = Parallel(n_jobs=myutils.get_num_thread())(delayed(process_one_4_make_mol_seq_features)(idx, vv) for idx, vv in tqdm(part.items()))
            arr = [u[1] for u in results] 
            indexes = [u[0] for u in results]
        
        outpath = os.path.join(config.INPUT_PATH, config.NNI_PATH, "traintest_submols_feat_part_{}.pklz".format(file_no))
        myutils.save_zipped_pickle({'value':arr, "index":indexes}, outpath) 

        # np.savez_compressed(outpath, value=arr, index=indexes)
                
    task = Task('make_mol_seq_features_{}'.format(file_no), lambda u: f(u))
    task()


def process_one_4_make_molseq_feat_mean(i, fpath):
    
    data = myutils.load_zipped_pickle(fpath)['value']
    n = 3213
    sum_x = np.zeros(n, dtype=np.float64)
    sum_x2 = np.zeros(n, dtype=np.float64)
    max_x = []
    min_x = []
    N = 0
    
    for i, b in (tqdm(enumerate(data)) if i == 0  else enumerate(data)): 
        x = myutils.uncompress_np_array(b).astype(np.float64)
        N += x.shape[0]
        max_x.append(np.max(x, 0))
        min_x.append(np.min(x, 0))
        x = np.sum(x, 0)
        sum_x += x 
        sum_x2 += x * x
        if(i > 100): break
         
    return sum_x, sum_x2, N, np.min(min_x, 0), np.max(max_x, 0) 


def make_molseq_feat_mean():

    def f(logger):
        filepaths = [ os.path.join(config.INPUT_PATH, config.NNI_PATH, "traintest_submols_feat_part_{}.pklz".format(file_no)) for file_no in range(10)]
        results = Parallel(n_jobs=myutils.get_num_thread())(delayed(process_one_4_make_molseq_feat_mean)(i, vv) for i, vv in enumerate(filepaths))
        
        sum_x = [u[0] for u in results]
        sum_x2 = [u[1] for u in results]
        N = [u[2] for u in results]
        max_x = [u[3] for u in results]
        min_x = [u[4] for u in results]
        
        sum_x = np.sum(sum_x, 0)
        sum_x2 = np.sum(sum_x2, 0)
        N = np.sum(N, 0)
        
        x_max = np.max(max_x, 0) 
        x_min = np.min(min_x, 0)
                
        x_mean = sum_x / N 
        x_stdev = np.sqrt((sum_x2 / N - x_mean ** 2))
        
        results = {'mean': x_mean, 'std': x_stdev, "n":N, 'max': x_max, 'min':x_min}
        
        for v in results.values():
            assert len(v) == 3213
            
        df = pd.DataFrame(results.values(), index=results.keys()).T 
        with pd.option_context("display.max_rows", 100000):
            print(df)

        outname = os.path.join(config.INPUT_PATH, "traintest_submols_seq_feat_mean.pkl")
        with open(outname, 'wb') as fout: 
            pickle.dump(results, fout)
                
    task = Task('make_molseq_feat_mean', lambda u: f(u))
    task()

                   
def run():
    make_generated_mols()
    transform_mols_to_pkl()
    for no in range(10):
        make_mol_seq_features(no)
    make_molseq_feat_mean()
    make_mol_feat_mean()



if __name__ == '__main__':
    run()

