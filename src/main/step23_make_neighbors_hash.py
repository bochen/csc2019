import fastparquet 
import config
import myutils
from task import Task
import os
from tqdm import tqdm 

from joblib import Parallel, delayed

from functools import reduce
from collections import Counter
import data

logger = myutils.get_logger("step23_make_neighbors_hash")


def get_neighbors(mol, idx):
    atom = mol.GetAtomWithIdx(idx)
    ret = [ (a.GetIdx(),) for  a in atom.GetNeighbors()]
    return ret


def get_neighbors2(mol, idx):
    atoms = get_neighbors(mol, idx)
    ret = [ [(a[0], b[0]) for b in get_neighbors(mol, a[0]) if b != idx] for  a in atoms]
    return reduce(lambda u, v: u + v, ret)


def index_to_symbols(mol, lst):
    ret = ["".join([mol.GetAtomWithIdx(i).GetSymbol() for i in u]) for u in lst]
    return tuple(sorted(list(Counter(ret).items())))


def get_neighbors2_by_path(mol, path):
    ret = []
    for idx in path:
        a = index_to_symbols(mol, get_neighbors(mol, idx)), index_to_symbols(mol, get_neighbors2(mol, idx))    
        ret.append(a)
    return tuple(ret)


def get_neighbors1_by_path(mol, path):
    ret = []
    for idx in path:
        a = index_to_symbols(mol, get_neighbors(mol, idx))
        ret.append(a)
    return tuple(ret)


def hash_neighbors1_by_path(mol, path):
    lst = get_neighbors1_by_path(mol, path)
    a = hash(lst)
    b = hash(lst[::-1])
    return max(a, b)


def hash_neighbors2_by_path(mol, path):
    lst = get_neighbors2_by_path(mol, path)
    a = hash(lst)
    b = hash(lst[::-1])
    return max(a, b)

    
def make_input_data():
    bp = data.load_bond_paths()
    bp = bp.set_index("id")
    mols = data.load_mols()
    
    bp['mol'] = bp['molname'].map(lambda u: mols[u])
    return bp 


def encode_categories(lst):
    m = {v:i for i, v in enumerate(set(lst))}
    return [ m[u] for u in lst]


def make_neighbors_hash_neighbor():               

    def f(logger):
        
        bp = make_input_data()
        
        result1 = Parallel(n_jobs=myutils.get_num_thread())(delayed(hash_neighbors1_by_path)(mol, sp) for mol, sp in tqdm(bp[['mol', 'shorted_path']].values))
        result2 = Parallel(n_jobs=myutils.get_num_thread())(delayed(hash_neighbors2_by_path)(mol, sp) for mol, sp in tqdm(bp[['mol', 'shorted_path']].values))
        
        bp['neighbor1_hash'] = encode_categories(result1)
        bp['neighbor2_hash'] = encode_categories(result2)
        
        df = bp[['neighbor1_hash', 'neighbor2_hash']]
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
            
        outname = os.path.join(config.FEAT_PATH, "traintest_bp_neighbors_hash.parq")
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY')
            
    task = Task('make_neighbors_hash_neighbor' , lambda u: f(u))
    task()    

                                  
def run():
    make_neighbors_hash_neighbor()
    

if __name__ == '__main__':
    run()

