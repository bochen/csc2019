import numpy as np
import config
import myutils
from task import Task
import os
import data
import longstuf
from tqdm import tqdm 
from train import __train_a_model, medpred
import shutil
import pandas as pd 
import joblib 

logger = myutils.get_logger("step40_make_train_and_predict")


def get_index(bp):
    a = len(bp)-1
    assert a in [1, 2, 3]
    if a == 1:
        fun = longstuf.get_1j_useless_columns
        feats = ['atom', 'bond', '1j_quadruples_feat', '1j_triplets_feat']
    elif a == 2:
        fun = longstuf.get_2j_useless_columns
        feats = ['atom', 'bond', '2j_quadruples_feat', '2j_triplets_feat']

    elif a == 3:
        fun = longstuf.get_3j_useless_columns
        feats = ['atom', 'bond', '3j_quadruples_feat', '3j_triplets_feat']

    return feats, fun()

    
def prepare_data(bp):
    feat_categories, exclude_columns = get_index(bp)
    traindf, trainy, trainmols, testdf, _, _ = data.load_data(njnum=None, njtype=None, bonpath=bp,
                                             feat_categories=feat_categories, exclude_columns=exclude_columns,
                                             na_fill=-9999, random_state=None, selected_index=None, sample=None)        
    return traindf, trainy, trainmols, testdf
        
  
def make_train_and_predict(bp, n_estimators):

    def __train(traindf, trainy, logger):
            
        logger.info("data shape: " + str([u.shape for u in [traindf, trainy]]))
        
        if 1:
            X_train = traindf.values 
            y_train = trainy.values
            regressor = __train_a_model(X_train, y_train, 'mse', random_state=None, n_estimators=n_estimators, model_type='erf', bootstrap=False, n_jobs=myutils.get_num_thread())
        
            return regressor
            
    def f(logger):
        outpath = os.path.join(config.PRED_PATH, "step30_{}_{}.csv.gz".format(bp, n_estimators))
        if os.path.exists(outpath):
            logger.info("Skip " + outpath)
            return 
        
        logger.info("reading data ...")
        traindf, trainy, trainmols, testdf = prepare_data(bp)
        regressor = __train(traindf, trainy, logger)
        
        modeloutpath = os.path.join(config.PRED_PATH, "model_step30_{}_{}.joblib".format(bp, n_estimators))
        #joblib.dump(regressor, modeloutpath)
        
        testpred = medpred(regressor, testdf.values)
        testpred = pd.Series(testpred, index=testdf.index).to_frame().reset_index(drop=False)
        testpred.columns = ['id', 'scalar_coupling_constant']
        testpred.to_csv(outpath + ".tmp", index=None, compression='gzip')
        shutil.move(outpath + ".tmp", outpath)
        
    task = Task('make_train_and_predict-{}-{}'.format(bp, n_estimators), lambda u: f(u))
    task()    

                                          
def run():
    bond_path_list = ['HCCC', 'HCC', 'HC', 'HCCH', 'HCH', 'HCCN', 'HCNC', 'HCN', 'HCOC', 'HNCC', 'HNC', 'HOCC', 'HN', 'HCNH', 'HOC', 'HCOH', 'HNCN', 'HNH', 'HCNN', 'HNN', 'HOCN', 'HNNC', 'HONC', 'HON', 'HCON', 'HNNN', ]
    bond_path_list = bond_path_list[::-1]
    for bp in bond_path_list:
        make_train_and_predict(bp, n_estimators=501)


if __name__ == '__main__':
    run()

