import numpy as np
import scipy.stats
import sys
import myutils
from rdkit import Chem

COLUMNS = [
 'GetAtomicNum',
 'GetChiralTag',
 'GetDegree',
 'GetExplicitValence',
 'GetFormalCharge',
 'GetHybridization',
 'GetImplicitValence',
 'GetIsAromatic',
 'GetIsotope',
 'GetMass',
 'GetMonomerInfo',
 'GetNoImplicit',
 'GetNumExplicitHs',
 'GetNumImplicitHs',
 'GetNumRadicalElectrons',
 'GetPDBResidueInfo',
 'GetTotalDegree',
 'GetTotalNumHs',
 'GetTotalValence',
 'IsInRing',
 ]            


def invert_map(my_map):
    return {v: k for k, v in my_map.items()}

 
CHIRAL_TYPE_DICT = invert_map(
    {0: Chem.rdchem.ChiralType.CHI_UNSPECIFIED, 1: Chem.rdchem.ChiralType.CHI_TETRAHEDRAL_CW, 2: Chem.rdchem.ChiralType.CHI_TETRAHEDRAL_CCW, 3: Chem.rdchem.ChiralType.CHI_OTHER}
    )

HybridizationType_dict = invert_map({0: Chem.rdchem.HybridizationType.UNSPECIFIED, 1: Chem.rdchem.HybridizationType.S, 2: Chem.rdchem.HybridizationType.SP, 3: Chem.rdchem.HybridizationType.SP2, 4: Chem.rdchem.HybridizationType.SP3, 5: Chem.rdchem.HybridizationType.SP3D, 6: Chem.rdchem.HybridizationType.SP3D2, 7: Chem.rdchem.HybridizationType.OTHER})


def to_number(v):
    if isinstance(v, Chem.rdchem.ChiralType):
        return CHIRAL_TYPE_DICT[v]
    elif isinstance(v, Chem.rdchem.HybridizationType):
        return HybridizationType_dict[v]
    else:
        return v


def make_feature(mol, atom_index):
    if atom_index is None:
        return np.zeros(len(COLUMNS), dtype=np.float32)
    
    feats = []
    # names = []

    def f(u, atom):
        # names.append(u)
        fn = getattr(atom, u)
        v = fn()
        feats.append(to_number(v))         

    atom = mol.GetAtomWithIdx(atom_index)
    for name in COLUMNS:
        f(name, atom)
    return np.array(feats, dtype=np.float32)


if __name__ == '__main__':
    mol = Chem.MolFromSmiles('[C@H](Cl)(F)Br')
    feat = make_feature(mol, 3)
    print (len(feat))
    print (feat)

