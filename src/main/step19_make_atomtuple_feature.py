import numpy as np
import config
import myutils
from task import Task
import os
from joblib import Parallel, delayed
import sys
import data
import molutils
from tqdm import tqdm 
import pandas as pd 
import fastparquet
import dask.dataframe as dd 

from step10_make_atom_feature import process_one_for_make_atom_feat, \
    process_one_for_make_bond_feat, process_one_for_make_bondpath_feat

logger = myutils.get_logger("step19_make_atomtuple_feature")


def make_1j_quadruples():
        
    def f(logger):
        molsdict = data.load_mols()
        bondpaths = data.load_bond_paths()
        
        lst = []
        for i in tqdm(range(len(bondpaths))[:]):
            row = bondpaths.iloc[i]
            sp = row['shorted_path']
            if(len(sp) != 2): 
                continue 
            molname = row['molname']
            idx = row['id']
            if molname not in molsdict:
                logger.error("mol {} not found".format(molname))
            neighbor_paths = molutils.mol_1jpath_neighbor_paths(molsdict[molname], sp)
            lst.append([idx, neighbor_paths])
        
        logger.info("# of records: {}".format(len(lst)))
            
        outname = os.path.join(config.INPUT_PATH, "traintest_1j_quadruples.pklz")
        logger.info("write " + outname)
        myutils.save_zipped_pickle(lst, outname)
        
    task = Task('make_1j_quadruples', lambda u: f(u))
    task()


def make_2j_quadruples():
        
    def f(logger):
        molsdict = data.load_mols()
        bondpaths = data.load_bond_paths()
        
        lst = []
        for i in tqdm(range(len(bondpaths))[:]):
            row = bondpaths.iloc[i]
            sp = row['shorted_path']
            if(len(sp) != 3): 
                continue 
            molname = row['molname']
            idx = row['id']
            if molname not in molsdict:
                logger.error("mol {} not found".format(molname))
            neighbor_paths = molutils.mol_2jpath_neighbor_paths(molsdict[molname], sp)
            lst.append([idx, neighbor_paths])
        
        logger.info("# of records: {}".format(len(lst)))
            
        outname = os.path.join(config.INPUT_PATH, "traintest_2j_quadruples.pklz")
        logger.info("write " + outname)
        myutils.save_zipped_pickle(lst, outname)
        
    task = Task('make_2j_quadruples', lambda u: f(u))
    task()


def make_3j_quadruples():
        
    def f(logger):
        molsdict = data.load_mols()
        bondpaths = data.load_bond_paths()
        
        lst = []
        for i in tqdm(range(len(bondpaths))[:]):
            row = bondpaths.iloc[i]
            sp = row['shorted_path']
            if(len(sp) != 4): 
                continue 
            molname = row['molname']
            idx = row['id']
            if molname not in molsdict:
                logger.error("mol {} not found".format(molname))
            neighbor_paths = molutils.mol_3jpath_neighbor_paths(molsdict[molname], sp)
            lst.append([idx, neighbor_paths])
        
        logger.info("# of records: {}".format(len(lst)))
            
        outname = os.path.join(config.INPUT_PATH, "traintest_3j_quadruples.pklz")
        logger.info("write " + outname)
        myutils.save_zipped_pickle(lst, outname)
        
    task = Task('make_3j_quadruples', lambda u: f(u))
    task()

    
def make_nj_quadruples(i):
    if i == 1:
        make_1j_quadruples()
    elif i == 2:
        make_2j_quadruples()
    elif i == 3:
        make_3j_quadruples()    

    
def process_one_4_nj_quad_feat(mol, lst):

    def process_three(mol, quad):
        a = process_one_for_make_atom_feat((mol, quad))
        b = process_one_for_make_bond_feat((mol, quad))
        c = process_one_for_make_bondpath_feat((mol, quad))
        return np.concatenate([a, b, c])

    def process_two(mol, quadlst):
        if len(quadlst) == 0:
            return np.zeros(121 * 3, dtype=np.float32) 
        else:
            arr = [process_three(mol, quad) for quad in quadlst]
            a = np.nanmean(arr, 0)
            b = np.nanmin(arr, 0)
            c = np.nanmax(arr, 0)
            
            return np.concatenate([a, b, c])
    
    arr = []
    if isinstance(lst, dict):
        lst = [lst]
    for path_idx, d in enumerate(lst):
        for i in [1, 2, 3]:
            row = process_two(mol, d[i])
            arr.append(row)
    if len(arr) == 0:
        return np.zeros(121 * 3 * 3 * len(lst), dtype=np.float32)
    else:
        return np.concatenate(arr).astype(np.float32)

        
def __part_make_nj_quadruples_feat(njno, numpart, partno):
                                
    def f(logger):
        filepath = os.path.join(config.INPUT_PATH, "traintest_{}j_quadruples.pklz".format(njno))
        logger.info("loading from " + filepath)
        quadruples = myutils.load_zipped_pickle(filepath)
        id_to_molname = data.load_bond_paths().set_index('id')['molname'].to_dict()
        logger.info("loaded {} mol names".format(len(id_to_molname)))
        molsdict = data.load_mols()
        logger.info("loaded {} mols".format(len(molsdict)))

        ret = [] 
        indexes = [] 
        tmp = tqdm(quadruples) if partno == 0 else quadruples
        for idx, lst in tmp:
            if(idx % numpart == partno):
                mol = molsdict[id_to_molname[idx]]
                row = process_one_4_nj_quad_feat(mol, lst)
                ret.append(row)
                indexes.append(idx)

        columns = ['q{}j_'.format(njno) + str(i) for i in range(len(ret[0]))]
        df = pd.DataFrame(ret, columns=columns, index=indexes)
               
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
            
        outname = os.path.join(config.FEAT_PATH, "traintest_{}j_quadruples_feat.parq".format(njno), "__part_traintest_{}j_quadruples_feat_p{}.parq".format(njno, partno))
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY')
    
    task = Task('make_{}j_quadruples_feat_part{}'.format(njno, partno), lambda u: f(u))
    task()

    
def make_nj_quadruples_feat(njno):
                                
    def f(logger):
        outname = os.path.join(config.FEAT_PATH, "traintest_{}j_quadruples_feat.parq".format(njno))
        myutils.create_dir_if_not_exists(outname)
        if True:
            numpart = 32
            Parallel(n_jobs=8)(delayed(__part_make_nj_quadruples_feat)(njno, numpart, partno) for partno in range(numpart))
            if False:
                g = lambda njno, partno:  os.path.join(config.FEAT_PATH, "__part_traintest_{}j_quadruples_feat_p{}.parq".format(njno, partno))
                df = dd.concat([dd.read_parquet(g(njno, i)) for i in range(numpart)]).sort_index().compute()
                       
                logger.info("df.shape={}\n{}".format(df.shape, df.head()))
        
                logger.info("write " + outname)
                fastparquet.write(outname, df, compression='SNAPPY')
        
    task = Task('make_{}j_quadruples_feat'.format(njno), lambda u: f(u))
    task()

                       
def run():
    [make_nj_quadruples(i) for i in [1, 2, 3]]
    for i in [1, 2, 3]:
        make_nj_quadruples_feat(i)


if __name__ == '__main__':
    run()

