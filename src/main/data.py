'''
Created on Feb 7, 2019

@author:  Lizhen Shi
'''
import config
import myutils
import dask.dataframe as dd 
import os
import pandas as pd
import numpy as np  
import fastparquet
import longstuf
import pickle
from joblib import Parallel, delayed

try:
    from rdkit import Chem
except:
    pass

import lz4.frame

logger = myutils.get_logger("data")


def load_mols():
    fpath = os.path.join(config.INPUT_PATH, 'merged_mols.sdf')
    mergedmols = {}
    suppl = Chem.SDMolSupplier(fpath, removeHs=False)
    for mol in suppl:
        name = mol.GetProp("_Name").replace(".xyz", "")
        mergedmols[name] = mol
    logger.info("loaded {} mols from {}".format(len(mergedmols), fpath))
    return mergedmols


def load_bond_paths():
    fpath = os.path.join(config.INPUT_PATH, 'traintest_shortest_path.csv')
    df = pd.read_csv(fpath)

    df['molname'] = df['key'].map(lambda u: u.split('-')[0])
    df['shorted_path'] = df['shorted_path'].map(lambda u: tuple([int(v) for v in u.split('-')]))
    
    logger.info("loaded {} bond paths from {}".format(len(df), fpath))
    return df


def load_structures():
    fpath = os.path.join(config.INPUT_PATH, 'structures.csv')
    df = pd.read_csv(fpath)
    logger.info("loaded {} structures from {}".format(len(df), fpath))
    return df

        
def load_traintest():
    fpath1 = os.path.join(config.INPUT_PATH, 'train.csv')
    fpath2 = os.path.join(config.INPUT_PATH, 'test.csv')
    traintest = pd.concat([pd.read_csv(fpath1, index_col=0),
                 pd.read_csv(fpath2, index_col=0)])
    return traintest


category_map = {
    'bond_paths':
    [{0: 'HC', 1: 'HCC', 2: 'HCCC', 3: 'HCCH', 4: 'HCCN', 5: 'HCH', 6: 'HCN', 7: 'HCNC', 8: 'HCNH', 9: 'HCNN', 10: 'HCOC', 11: 'HCOH', 12: 'HCON', 13: 'HN', 14: 'HNC', 15: 'HNCC', 16: 'HNCN', 17: 'HNH', 18: 'HNN', 19: 'HNNC', 20: 'HNNN', 21: 'HOC', 22: 'HOCC', 23: 'HOCN', 24: 'HOH', 25: 'HON', 26: 'HONC'},
     {'HC': 0, 'HCC': 1, 'HCCC': 2, 'HCCH': 3, 'HCCN': 4, 'HCH': 5, 'HCN': 6, 'HCNC': 7, 'HCNH': 8, 'HCNN': 9, 'HCOC': 10, 'HCOH': 11, 'HCON': 12, 'HN': 13, 'HNC': 14, 'HNCC': 15, 'HNCN': 16, 'HNH': 17, 'HNN': 18, 'HNNC': 19, 'HNNN': 20, 'HOC': 21, 'HOCC': 22, 'HOCN': 23, 'HOH': 24, 'HON': 25, 'HONC': 26}
     ],
    'type':
    [{0: '1JHC', 1: '1JHN', 2: '2JHC', 3: '2JHH', 4: '2JHN', 5: '3JHC', 6: '3JHH', 7: '3JHN'},
     {'1JHC': 0, '1JHN': 1, '2JHC': 2, '2JHH': 3, '2JHN': 4, '3JHC': 5, '3JHH': 6, '3JHN': 7}]
    }

get_useless_columns = longstuf.get_useless_columns
get_1j_useless_columns = longstuf.get_1j_useless_columns
get_2j_useless_columns = longstuf.get_2j_useless_columns
get_3j_useless_columns = longstuf.get_3j_useless_columns

ALL_NJ_NUMBERS = [1, 2, 3]
ALL_NJ_TYPES = ['3JHC', '2JHC', '1JHC', '3JHH', '2JHH', '3JHN', '2JHN', '1JHN']
ALL_BOND_PATHS = ['HCCC',
'HCC',
'HC',
'HCCH',
'HCH',
'HCCN',
'HCNC',
'HCN',
'HCOC',
'HNCC',
'HNC',
'HOCC',
'HN',
'HCNH',
'HOC',
'HCOH',
'HNCN',
'HNH',
'HCNN',
'HNN',
'HOCN',
'HNNC',
'HONC',
'HON',
'HCON',
'HNNN',
]

categories = [
"atom_mullikencharge",
"atom",
"bond",
"general",
"mol_AtomPairFingerprint",
"mol_CoulombMatrixEig",
"mol_MorganFingerprint",
"mol_qm9_feat",
"mol_rdkitDesc",
"mol_TopologicalTorsionFingerprint",
  ]


class LZ4CacheBackup():

    def __init__(self):
        self.cache = {}
        self.excludes = (get_useless_columns())
    
    def update(self, filename, colname):
        logger.info("reading {} from {}".format(colname, filename))
        df = dd.read_parquet(filename)
        cols = list(set(df.columns).difference(self.excludes))

        df = df[cols].compute()
        idx = list(df.index)

        for col in cols:
            s = df[col]
            if col in category_map:
                d = category_map[col][1]
                s = s.map(lambda u: d[u]).astype(np.float32)
                
            val = list(s.astype(np.float32))
            arr = np.zeros(2 ** 23, dtype=np.float32) + np.nan
            arr[idx] = val
            self.cache[filename][col] = lz4.frame.compress(arr.tobytes())
        
    def get_col(self, filename, colname):
        cache = self.cache
        if filename not in cache:
            cache[filename] = {}
        if colname not in cache[filename]:
            self.update(filename, colname)
        compressed = cache[filename][colname]
        val = lz4.frame.decompress(compressed)
        return np.frombuffer(val, dtype=np.float32)


class LZ4Cache():

    def __init__(self):
        self.cache = {}
        self.excludes = (get_useless_columns())
    
    def update(self, filename, colname):
        if filename.endswith('*'):
            basename=filename.split('/')[-2]
        else:
            basename=filename.split('/')[-1]

        newfilename = os.path.join(config.COMPRESSED_PATH,  basename, colname)
        logger.info("reading {} from {}".format(colname, newfilename))
        with open(newfilename, 'rb') as fin: 
            compressed = pickle.load(fin)
        self.cache[filename][colname] = compressed
        
    def get_col(self, filename, colname):
        cache = self.cache
        if filename not in cache:
            cache[filename] = {}
        if colname not in cache[filename]:
            self.update(filename, colname)
        compressed = cache[filename][colname]
        val = lz4.frame.decompress(compressed)
        return np.frombuffer(val, dtype=np.float32)


g_lz4_cache = LZ4Cache()

parq_columns = {}


def get_parq_columns(filename):
    if 0:
        if filename not in parq_columns:
            df = dd.read_parquet(filename) 
            parq_columns[filename] = set(df.columns)
        return parq_columns[filename]
    else:
        return set(longstuf.get_columns(filename))


use_lz4_cache = False

        
def load_data(njnum=None, njtype=None, bonpath=None, feat_categories=None , exclude_columns=None , return_test=True,
              return_target=True, na_fill=None, sample=None, selected_index=None, random_state=None):
    
    assert int(njnum is None) + int(njtype is None) + int(bonpath is None) + int(selected_index is None) == 3, "only one can be set"
    
    if feat_categories is None: 
        feat_categories = categories
        
    if (exclude_columns != None):
        exclude_columns = set(exclude_columns)
    else: 
        exclude_columns = set()
    exclude_columns.update(get_useless_columns())
    logger.info("Exclude {} columns".format(len(exclude_columns)))
    
    # fine_structures = dd.read_parquet(os.path.join(config.FEAT_PATH, "fine_structure.parq")).compute()
    fine_structures = fastparquet.ParquetFile(os.path.join(config.FEAT_PATH, "fine_structure.parq")).to_pandas()

    def get_indexes(njnum, njtype, bonpath, selected_index):
        if selected_index is not None:
            return selected_index
        
        indexes = None 
        if(njnum is not None):
            if(isinstance(njnum, int)):
                njnum = [njnum]
            njnum = set(njnum)
            indexes = fine_structures['nj'].isin(njnum)
            
        elif(njtype is not None):
            if(isinstance(njtype, str)):
                njtype = [njtype]
            njtype = set(njtype)
            indexes = fine_structures['type'].isin(njtype)
        elif(bonpath is not None):
            if(isinstance(bonpath, str)):
                bonpath = [bonpath]
            bonpath = set(bonpath)
            indexes = fine_structures['bond_paths'].isin(bonpath)
        else:
            raise Exception("should not be here")       
        assert np.sum(indexes) > 0
        
        return fine_structures.loc[indexes].index
    
    indexes = get_indexes(njnum, njtype, bonpath, selected_index)
    indexes = list(indexes)
    if sample is not None and sample < len(indexes):
        if random_state is not None:
            np.random.seed(random_state)

        indexes = list(indexes)
        np.random.shuffle(indexes)
        indexes = indexes[:sample]
    
    fine_structures = fine_structures.loc[indexes]
            
    def load_cat(category, indexes):
        if category == "ac1":
            filepath = 'traintest_autoencoder_feat-model_epoch_024_loss_1.1168.hdf5.parq'
            filepath = os.path.join(config.FEAT_PATH, filepath)
        elif category == "ac2":
            filepath = 'traintest_autoencoder_feat-model_epoch_023_loss_1.1209.hdf5.parq'
            filepath = os.path.join(config.FEAT_PATH, filepath)
        elif category == "ac3":
            filepath = 'traintest_autoencoder_feat-model_epoch_021_loss_1.1205.hdf5.parq'
            filepath = os.path.join(config.FEAT_PATH, filepath)
        elif category == "ac4":
            filepath = 'traintest_autoencoder_feat-model_epoch_020_loss_1.1212.hdf5.parq'
            filepath = os.path.join(config.FEAT_PATH, filepath)                        
        elif category == "sac1":
            filepath = 'traintest_autoencoder_shortseq_feat-model_shortseq_epoch_020_loss_5.4097.hdf5.parq' 
            filepath = os.path.join(config.FEAT_PATH, filepath)            
        elif category == "sac2":
            filepath = 'traintest_autoencoder_shortseq_feat-model_shortseq_epoch_019_loss_5.4113.hdf5.parq' 
            filepath = os.path.join(config.FEAT_PATH, filepath)
        elif category == "mac0":
            filepath = 'traintest_autoencoder_mol2_feat_molno0-model_mol2_epoch_020_loss_8.1866.hdf5.parq' 
            filepath = os.path.join(config.FEAT_PATH, filepath)            
        elif category == "mac1":
            filepath = 'traintest_autoencoder_mol2_feat_molno1-model_mol2_epoch_020_loss_8.1866.hdf5.parq' 
            filepath = os.path.join(config.FEAT_PATH, filepath)            
        elif category.endswith('j_quadruples_feat') or category.endswith('j_triplets_feat'):
            filepath = os.path.join(config.FEAT_PATH, "traintest_{}.parq".format(category), '*')                        
        else:
            filepath = os.path.join(config.FEAT_PATH, "traintest_{}.parq".format(category))
            
        if category.startswith("mol_"): 
            df = dd.read_parquet(filepath)
            df = df.loc[:, ~df.columns.isin(exclude_columns)]
            df = df.compute()
            logger.info("loaded {} from {}".format(df.shape, filepath))
            tmpdf = fine_structures[['molecule_name']]
            df['molecule_name'] = df.index
            df=df.reset_index(drop=True)
            tmpdf = pd.merge(tmpdf.reset_index(), df, on="molecule_name", how='left').set_index('id')
            tmpdf.drop('molecule_name', axis=1, inplace=True)

            tmpdf = tmpdf.loc[indexes].sort_index()
            return tmpdf
        else:
            if not use_lz4_cache or  ( category.startswith("layer3_") or category.startswith("layer4_")):
                df = dd.read_parquet(filepath)
                df_selected = df.map_partitions(lambda x: x[x.index.isin(set(indexes))])
                tmpdf = df_selected.loc[:, ~df.columns.isin(exclude_columns)].compute().sort_index()
                logger.info("loaded {} from {}".format(tmpdf.shape, filepath))
                return tmpdf
            else:
                cols = get_parq_columns(filepath).difference(exclude_columns)
                indexes = list(indexes)
                fun = lambda col: g_lz4_cache.get_col(filepath, col)[indexes] 
                lst=Parallel(n_jobs=myutils.get_num_thread(), prefer="threads")(delayed(fun)(col) for col in cols)
                #lst = [g_lz4_cache.get_col(filepath, col)[indexes] for col in cols]
                df = pd.DataFrame(np.array(lst).T, columns=cols, index=indexes)
                df.index.name = 'id'
                return df 
        
    df = pd.concat([load_cat(category, indexes) for category in feat_categories], axis=1, copy=False)
    # df.sort_index(inplace=True)
    
    target = dd.read_parquet(os.path.join(config.FEAT_PATH, "traintest_target.parq"))['scalar_coupling_constant'].compute().iloc[indexes]
    
    assert df.shape[0] == target.shape[0], str(df.shape) + " " + str(target.shape)

    if na_fill is not None:
        df.fillna(na_fill, inplace=True)
        pass

    if not use_lz4_cache:
        for colname in df.columns:
            if colname in category_map:
                d = category_map[colname][1]
                df[colname] = df[colname].map(lambda u: d[u]).astype(np.float32)
    
    traindf = df[df.index < config.IDX_TEST_START]
    if return_test:
        testdf = df[df.index >= config.IDX_TEST_START]
    else:
        testdf = None 
    
    if(return_target):    
        target = target.loc[df.index]
        trainy = target[target.index < config.IDX_TEST_START]
        if return_test:
            testy = target[target.index >= config.IDX_TEST_START]
        else:
            testy = None 
    else:
        trainy = None
        testy = None 
    
    trainmols = fine_structures.loc[traindf.index, 'molecule_name']
    if(return_target and return_test):
        testmols = fine_structures.loc[testdf.index, 'molecule_name']
    else:
        testmols = None 
    
    return traindf, trainy, trainmols, testdf, testy, testmols 
    
