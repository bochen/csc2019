import fastparquet 
import pandas as pd 
import numpy as np
import config
import myutils
from task import Task
import os
from tqdm import tqdm 

import keras
from keras.models import load_model
from keras import Model
from keras.layers import Lambda

loss = lambda y_true, y_pred: 1000 * keras.losses.mse(y_true, y_pred)

def make_mol2_autoencoder_prediction(modelfile):               

    def predict(model, data_std, lst, molno):
        values = []
        predlist = []
        for v in tqdm(lst[:]):
            values.append(myutils.uncompress_np_array(v)[molno])
            if len(values)>=51200:
                values=np.array(values) / data_std
                pred = model.predict(values, batch_size=512,  verbose=1)
                predlist.append(pred) 
                values=[] 
        if len(values)>0:
            values=np.array(values) / data_std
            pred = model.predict(values, batch_size=512,  verbose=1)
            predlist.append(pred) 
            values=[] 

        ret= np.concatenate(predlist)
        assert len(lst) == len(ret)
        return ret

    def g(logger,datafiles,hidden_model,data_std, molno):
        dflist = []
        for filename in datafiles[:]:
            logger.info(" reading " + filename)
            tmp = myutils.load_zipped_pickle(filename)
            values = tmp['value']
            keys = tmp['index']
            assert len(values) == len(keys)
            logger.info("# records: " + str(len(values)))
            
            arr = predict(hidden_model, data_std, values, molno)
            dflist.append(pd.DataFrame(arr, index=keys[:]))
            
        df = pd.concat(dflist)
        df.index.name = 'id'
        df.columns = ['mae{}_'.format(molno) + str(i) for i in range(df.shape[1])]
        df=df.sort_index()
        
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
        
        outname = os.path.join(config.FEAT_PATH, "traintest_autoencoder_mol2_feat_molno{}-{}.parq".format(molno, modelfile))
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY')            

    def f(logger):
        modelpath = os.path.join(config.FEAT_PATH, modelfile)
        logger.info("loading model from " + modelpath)
        model = load_model(modelpath, custom_objects={'<lambda>': loss})
        hidden_model = Model(inputs=model.inputs, outputs=model.get_layer("repr_hidden").output)
        
        datafiles = myutils.get_mol2_datafiles()
        data_std = myutils.get_mol2_feat_std()
        data_std = np.reshape(data_std, [1, -1]).astype(np.float32)

        for molno in [0,1]:
            g(logger,datafiles,hidden_model,data_std, molno)
        
    task = Task('make_mol2_autoencoder_prediction-'+modelfile, lambda u: f(u))
    task()    

                                  
def run():
    files= ['model_mol2_epoch_020_loss_8.1866.hdf5']
    for modelfile in files[:]:
        make_mol2_autoencoder_prediction(modelfile)


if __name__ == '__main__':
    run()

