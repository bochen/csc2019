'''
Created on Feb 7, 2019

@author:  Lizhen Shi
'''
from __future__ import print_function

import numpy as np 
import config
import myutils
import json
import os
import pandas as pd 

from rdkit import Chem
from rdkit.Chem import AllChem
import networkx as nx
from rdkit import Geometry

dict_atomic_nums = {'C':6, 'F':9, 'H':1, 'N':7, 'O':8}
dict_num_bond = {
    1:Chem.rdchem.BondType.SINGLE,
    1.5:Chem.rdchem.BondType.ONEANDAHALF,
    2:Chem.rdchem.BondType.DOUBLE,
    2.5:Chem.rdchem.BondType.TWOANDAHALF,
    3:Chem.rdchem.BondType.TRIPLE,
    3.5:Chem.rdchem.BondType.THREEANDAHALF,
}


def nx_to_mol(G, check_nid=True, sanitization=True):
    '''
    modified from https://github.com/dakoner/keras-molecules/blob/dbbb790e74e406faa70b13e8be8104d9e938eba2/convert_rdkit_to_networkx.py
    '''
    mol = Chem.RWMol()
    atomic_nums = nx.get_node_attributes(G, 'atomic_num')
    chiral_tags = nx.get_node_attributes(G, 'chiral_tag')
    formal_charges = nx.get_node_attributes(G, 'formal_charge')
    node_is_aromatics = nx.get_node_attributes(G, 'is_aromatic')
    node_hybridizations = nx.get_node_attributes(G, 'hybridization')
    num_explicit_hss = nx.get_node_attributes(G, 'num_explicit_hs')
    node_to_idx = {}
    for node in sorted(list(G.nodes())):
        a = Chem.Atom(atomic_nums[node])
        if chiral_tags: a.SetChiralTag(chiral_tags[node])
        if formal_charges: a.SetFormalCharge(formal_charges[node])
        if node_is_aromatics: a.SetIsAromatic(node_is_aromatics[node])
        if node_hybridizations: a.SetHybridization(node_hybridizations[node])
        if num_explicit_hss: a.SetNumExplicitHs(num_explicit_hss[node])
        idx = mol.AddAtom(a)
        if check_nid:
            assert (node == idx)
        node_to_idx[node] = idx
    
    bond_types = nx.get_edge_attributes(G, 'bond_type')
    # print (node_to_idx)
    # print (bond_types)

    for edge in G.edges():
        first, second = edge
        ifirst = node_to_idx[first]
        isecond = node_to_idx[second]
        if (first, second) in bond_types:
            bond_type = bond_types[(first, second)]
            # print (first,second,ifirst,isecond,bond_type)
            mol.AddBond(ifirst, isecond, bond_type)

    # Chem.SanitizeMol(mol)
    mol.UpdatePropertyCache()
    AllChem.Compute2DCoords(mol)
    # AllChem.EmbedMolecule(mol)
    xyz = nx.get_node_attributes(G, 'xyz')
    if True and xyz:
        conformer = mol.GetConformer()
        for node in G.nodes():
            idx = node_to_idx[node]
            x, y, z = xyz[node]
            pos = Geometry.rdGeometry.Point3D(x, y, z)
            conformer.SetAtomPosition(idx, pos)
    if sanitization:
        Chem.SanitizeMol(mol)
    return mol


def mol_xyz(mol, i, conformer=None):
    if not conformer:
        conformer = mol.GetConformer()
    b = conformer.GetAtomPosition(i)
    return [b.x, b.y, b.z]

    
def mol_to_nx(mol):
    G = nx.Graph()

    for atom in mol.GetAtoms():
        G.add_node(atom.GetIdx(),
                   atomic_num=atom.GetAtomicNum(),
                   formal_charge=atom.GetFormalCharge(),
                   chiral_tag=atom.GetChiralTag(),
                   hybridization=atom.GetHybridization(),
                   num_explicit_hs=atom.GetNumExplicitHs(),
                   is_aromatic=atom.GetIsAromatic(),
                   xyz=mol_xyz(mol, atom.GetIdx()))
    for bond in mol.GetBonds():
        G.add_edge(bond.GetBeginAtomIdx(),
                   bond.GetEndAtomIdx(),
                   bond_type=bond.GetBondType())
    return G


def mol_with_atom_index(mol):
    atoms = mol.GetNumAtoms()
    for idx in range(atoms):
        mol.GetAtomWithIdx(idx).SetProp('molAtomMapNumber', str(mol.GetAtomWithIdx(idx).GetIdx()))
    return mol


def mol_shortest_path(mol, i, j):
    return Chem.rdmolops.GetShortestPath(mol, int(i), int(j))


def has_bond(mol, i, j):
    if mol.GetBondBetweenAtoms(i, j):
        return True
    else:
        return False


def nx_induced_graph(G, nodes):
    nodes = set(nodes)
    g_nodes = set(G.nodes)
    for u in nodes:
        assert u in g_nodes 
    if nodes == g_nodes:
        return None
    newnodes = []
    for u in nodes:
        for v in G.neighbors(u):
            newnodes.append(v)
    newnodes = set(newnodes)
    return G.subgraph(newnodes)


def induced_mols(mol, nodeIds):
    G = mol_to_nx(mol)
    
    subgraph = G.subgraph(nodeIds)
    graphs = [subgraph]
    for _ in range(mol.GetNumAtoms() * 2):
        subgraph = nx_induced_graph(G, subgraph)
        if subgraph is None:
            break
        graphs.append(subgraph)
        
    return [nx_to_mol(u, check_nid=False, sanitization=False) for u in graphs]


def nx_1jpath_triplet(G, paths):
    assert len(paths) == 2, paths
    paths = list(paths)
    i, j = paths
    ret = {}
    if True: 
        newpaths = []
        for u in G.neighbors(i):
            a = [u] + paths
            newpaths.append(tuple(a))
        newpaths = [u for u in newpaths if len(set(u)) == 3]
        newpaths = list(set(newpaths))
        ret[1] = newpaths
        
    if True:       
        newpaths = []         
        for u in G.neighbors(j):
            a = paths + [u]
            newpaths.append(tuple(a))
        newpaths = [u for u in newpaths if len(set(u)) == 3]
        newpaths = list(set(newpaths))
        ret[2] = newpaths

    return ret

def mol_1jpath_neighbor_triplet(mol, paths):
    G = mol_to_nx(mol)
    return nx_1jpath_triplet(G, paths)


def mol_2jpath_neighbor_triplet(mol, paths):
    assert len(paths) == 3
    G = mol_to_nx(mol)
    ret = [] 
    for i, j  in zip(paths[:-1], paths[1:]):
        ret.append(nx_1jpath_triplet(G, [i, j]))
    return ret


def mol_3jpath_neighbor_triplet(mol, paths):
    assert len(paths) == 4
    G = mol_to_nx(mol)
    ret = [] 
    for i, j  in zip(paths[:-1], paths[1:]):
        ret.append(nx_1jpath_triplet(G, [i, j]))
    return ret


def nx_1jpath_neighbors(G, paths):
    assert len(paths) == 2, paths
    paths = list(paths)
    i, j = paths
    ret = {}
    if True: 
        newpaths = []
        for u in G.neighbors(i):
            for v in G.neighbors(u):
                a = [v, u] + paths
                newpaths.append(tuple(a))
        newpaths = [u for u in newpaths if len(set(u)) == 4]
        newpaths = list(set(newpaths))
        ret[1] = newpaths
        
    if True:       
        newpaths = []         
        for u in G.neighbors(i):
            for v in G.neighbors(j):
                a = [u, i, j, v]
                newpaths.append(tuple(a))
        newpaths = [u for u in newpaths if len(set(u)) == 4]
        newpaths = list(set(newpaths))
        ret[2] = newpaths

    if True:       
        newpaths = []         
        for u in G.neighbors(j):
            for v in G.neighbors(u):
                a = paths + [u, v]
                newpaths.append(tuple(a))
        newpaths = [u for u in newpaths if len(set(u)) == 4]
        newpaths = list(set(newpaths))
        ret[3] = newpaths

    return ret
    
    
def mol_1jpath_neighbor_paths(mol, paths):
    G = mol_to_nx(mol)
    return nx_1jpath_neighbors(G, paths)


def mol_2jpath_neighbor_paths(mol, paths):
    assert len(paths) == 3
    G = mol_to_nx(mol)
    ret = [] 
    for i, j  in zip(paths[:-1], paths[1:]):
        ret.append(nx_1jpath_neighbors(G, [i, j]))
    return ret


def mol_3jpath_neighbor_paths(mol, paths):
    assert len(paths) == 4
    G = mol_to_nx(mol)
    ret = [] 
    for i, j  in zip(paths[:-1], paths[1:]):
        ret.append(nx_1jpath_neighbors(G, [i, j]))
    return ret
    
    
###################################
def tetrahedron_volume(p1, p2, p3, p4):
    x1, y1, z1 = p1
    x2, y2, z2 = p2
    x3, y3, z3 = p3
    x4, y4, z4 = p4
    
    v = (x4 - x1) * ((y2 - y1) * (z3 - z1) - (z2 - z1) * (y3 - y1)) \
        +(y4 - y1) * ((z2 - z1) * (x3 - x1) - (x2 - x1) * (z3 - z1)) \
        +(z4 - z1) * ((x2 - x1) * (y3 - y1) - (y2 - y1) * (x3 - x1))
    return v / 6.0


def distance_2points(p1, p2):
    return np.sqrt(np.sum((np.array(p1) - np.array(p2)) ** 2))


def angle_3points(p1, p2, p3):  # p2 is povit
    d21 = distance_2points(p1, p2)
    d23 = distance_2points(p3, p2)
    d13 = distance_2points(p3, p1)
    a = (d21 ** 2 + d23 ** 2 - d13 ** 2) / (2 * d21 * d23)
    return np.arccos(a)


def area_3points(p1, p2, p3):
    # let p3 be original piont
    p1 = np.array(p1) - np.array(p3)
    p2 = np.array(p2) - np.array(p3)
    if 0:
        a1, a2, a3 = p1
        b1, b2, b3 = p2
        area = np.array([(a2 * b3 - a3 * b2), a1 * b3 - a3 * b1, a1 * b2 - a2 * b1])
    else:
        area = np.cross(p1, p2)
    return np.sqrt(np.sum(area ** 2)) / 2.0

    
def plane_equation(p1, p2, p3):
    # let p3 be original piont
    p1 = np.array(p1) - np.array(p3)
    p2 = np.array(p2) - np.array(p3)
    a, b, c = np.cross(p1, p2)
    d = -(a * p3[0] + b * p3[1] + c * p3[2])
    return a, b, c, d


def dihedral_angle(p123, q123):
    x = np.array(plane_equation(*p123)[:3])
    y = np.array(plane_equation(*q123)[:3])
    a = np.abs(np.sum(x * y)) / np.sqrt(np.sum(x * x)) / np.sqrt(np.sum(y * y))
    return np.arccos(a)


def torsion_angle(p1, p2, p3, p4):
    return dihedral_angle((p1, p2, p3), (p2, p3, p4))

    
if __name__ == '__main__':
    
    def show_image(path):
        import matplotlib.pyplot as plt
        import matplotlib.image as mpimg
        img = mpimg.imread(path)
        imgplot = plt.imshow(img)
        plt.show()
        
    def mol_with_atom_index(mol):
        atoms = mol.GetNumAtoms()
        for idx in range(atoms):
            mol.GetAtomWithIdx(idx).SetProp('molAtomMapNumber', str(mol.GetAtomWithIdx(idx).GetIdx()))
        return mol        
        
    v = tetrahedron_volume([0, 0, 0], [2, 0, 0], [0, 3, 0], [1, 1, 2])
    assert v == 2, str(v)
    
    d = distance_2points([0, 0, 0], [1, 1, 1])
    assert d == np.sqrt(3), str(d)
    
    d = angle_3points([0, 0, 0], [1, 0, 0], [0, 1, 0])
    print ("angle", str(d), np.pi / 4)
    
    d = area_3points([0, 0, 0], [1, 0, 0], [1, 1, 0])
    print ("area", str(d), 0.5)
    
    a, b, c, d = plane_equation([1, 2, -2], [3, -2, 1], [5, 1, -4])
    print ("plan equation {}x+{}y+{}z+{}=0".format(a, b, c, d))   

    d = torsion_angle([0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1],)
    print ("torsion angle", str(d), np.arctan(np.sqrt(2)))

    mol = mol_with_atom_index(Chem.AddHs(Chem.MolFromSmiles('Cn1nccn1')))
    Chem.AllChem.Compute2DCoords(mol)
    print("triplets:")
    print (mol_1jpath_neighbor_triplet(mol, [1, 2]))
    print (mol_1jpath_neighbor_triplet(mol, [0, 1]))
    
    print (mol_2jpath_neighbor_triplet(mol, [5, 1, 2]))
    print (mol_3jpath_neighbor_triplet(mol, [4, 5, 1, 2]))
    
    print("quadruples:")
    print (mol_1jpath_neighbor_paths(mol, [1, 2]))
    print (mol_1jpath_neighbor_paths(mol, [0, 1]))
    
    print (mol_2jpath_neighbor_paths(mol, [5, 1, 2]))
    print (mol_3jpath_neighbor_paths(mol, [4, 5, 1, 2]))
    # Chem.AllChem.EmbedMolecule(mol)
    from rdkit.Chem import Draw
    tmppath = '/tmp/testmol.png'
    Draw.MolToFile(mol, tmppath)
    show_image(tmppath)
    
