import numpy as np 
import myutils
import config
import keras 
import time, os, gc

class DataGeneratorPar(keras.utils.Sequence):
    'Generates data for Keras'

    def __init__(self, datafiles, data_size, data_std, batch_size=32, model_type=0):
        'Initialization'
        self.datafiles = list(datafiles)
        self.batch_size = batch_size
        self.data_size = data_size
        self.data_std = np.reshape(data_std, [1, 1, -1]).astype(np.float32)
        self.model_type = model_type  # 0 autoencoder, 1 prediction, 2 both
        
        self.current_datafile = None
        self.data = None
        
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor((self.data_size) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'

        # Generate data
        X = self.__data_generation_next_batch() / self.data_std
        if self.model_type == 0:
            return X, X
        elif self.model_type == 1:
            return X, X[:, 1:, :]
        elif self.model_type == 2:
            return X, [X, X[:, 1:, :]]

    def on_epoch_end(self):
        'Updates indexes after each epoch'

        pass
        #self.current_datafile = None
        #self.data = None
    
    def __set_seed(self):
        i = int(time.time()) + os.getpid()
        np.random.seed(i)
    
    def __on_read_next_file(self):
        self.__set_seed()
        np.random.shuffle(self.datafiles)
        
        self.current_datafile = self.datafiles[0]
        self.curr_file_idx = np.random.permutation(range(len(self.datafiles)))[0]
        
        filename = self.current_datafile
        #print(" reading " + filename)
        self.data = myutils.load_zipped_pickle(filename)['value']
        n = len(self.data)
        #print("# records: " + str(n))
        self.indexes_in_file = list(range(n))
        np.random.shuffle(self.indexes_in_file)
        self.curr_idx_in_file = 0
        gc.collect()
        
    def __data_generation_next_instance(self):
        if self.curr_idx_in_file < len(self.data):
            pass 
        else:
            self.data = None 
            self.__on_read_next_file()

        i = self.indexes_in_file[self.curr_idx_in_file]
        b = self.data[i]
        self.curr_idx_in_file += 1
        
        return myutils.uncompress_np_array(b)
    
    def __data_generation_next_batch(self):
        if self.data is None:
            self.__on_read_next_file()
        X = []
        while len(X) < self.batch_size:
            X.append(self.__data_generation_next_instance())
        
        return np.array(X, dtype=np.float32)


from keras.models import Sequential, Model
from keras.layers import LSTM, Bidirectional, CuDNNLSTM
from keras.layers import Dense, Input, Lambda, Activation
from keras.layers import RepeatVector
from keras.layers import TimeDistributed

def create_lstm(x, hidden_units, use_cudnn=False, activation="relu", bidirectional=False, return_sequences=False):
    if use_cudnn:
        layer = CuDNNLSTM(hidden_units, return_sequences=return_sequences)
    else:
        layer =  LSTM(hidden_units, return_sequences=return_sequences)
    
    if bidirectional:
        layer = Bidirectional(layer) 
    
    x=layer(x)
    x=Activation(activation)(x)                
    return x
    
def make_model(model_type, hidden_units=50, activation='relu', input_shape=[5, 3213], bidirectional=False,  use_cudnn=False):
    if model_type == 0:
        return make_model_0(hidden_units, activation, input_shape, bidirectional, use_cudnn)


def make_model_0(hidden_units=50, activation='relu', input_shape=[5, 3213], bidirectional=False, use_cudnn=False):
    if False:
        pass
    else:
        input_sequences = Input(shape=input_shape, name="main_input")
        x = input_sequences
        #lstm_layer = create_CuDNNLSTM(hidden_units, activation=activation)
        #if(bidirectional):
         #   x = Bidirectional(lstm_layer)(x)
        #else:
         #   x = lstm_layer(x)
        x= create_lstm(x, hidden_units, use_cudnn=use_cudnn, activation=activation, bidirectional=bidirectional,return_sequences=False)
        
        x = Lambda(lambda u:u, name='repr_hidden')(x)
        
        x = RepeatVector(input_shape[0])(x)
        #x = CuDNNLSTM(hidden_units, activation=activation, return_sequences=True)(x)
        x= create_lstm(x,hidden_units, use_cudnn=use_cudnn, activation=activation, bidirectional=False,return_sequences=True)
        x = TimeDistributed(Dense(input_shape[-1]))(x)
        model = Model(inputs=[input_sequences], outputs=[x])
        return model
