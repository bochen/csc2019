import numpy as np
import config
import myutils
from task import Task
import os
import fastparquet
from tqdm import tqdm 
import shutil
import pandas as pd 

bond_path_list = ['HCCC', 'HCC', 'HC', 'HCCH', 'HCH', 'HCCN', 'HCNC', 'HCN', 'HCOC', 'HNCC', 'HNC', 'HOCC', 'HN', 'HCNH', 'HOC', 'HCOH', 'HNCN', 'HNH', 'HCNN', 'HNN', 'HOCN', 'HNNC', 'HONC', 'HON', 'HCON', 'HNNN', ]
TEST_SIZE = 2505543 - 1
TRAIN_SIZE = 4658148 - 2


def merge_predictions_s40():

    def f(logger):
        outname = os.path.join(config.PRED_PATH, "step30_merged_501.csv.gz")
        assert not os.path.exists(outname), outname
        
        files = [os.path.join(config.PRED_PATH, "step30_{}_501.csv.gz".format(u)) for u in bond_path_list]
        for u in files:
            assert os.path.exists(u), u 
            
        df = pd.concat([pd.read_csv(u, index_col=0) for u in files])
        
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
        assert len(df) == TEST_SIZE
        logger.info("write " + outname)
        df.to_csv(outname + ".tmp", compression='gzip')
        shutil.move(outname + ".tmp", outname)
                
    task = Task('merge_predictions_s40', lambda u: f(u))
    task()    


def merge_predictions_s50(fold):

    def f(logger):
        outname = os.path.join(config.PRED_PATH, "step50_merged_101_fold{}.csv.gz".format(fold))
        assert not os.path.exists(outname), outname
        
        files = [os.path.join(config.PRED_PATH, "step50_{}_101_fold{}.csv.gz".format(u, fold)) for u in bond_path_list]
        for u in files:
            assert os.path.exists(u), u 
            
        df = pd.concat([pd.read_csv(u, index_col=0) for u in files])
        
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
        assert len(df) == TEST_SIZE
        logger.info("write " + outname)
        df.to_csv(outname + ".tmp", compression='gzip')
        shutil.move(outname + ".tmp", outname)
                
    task = Task('merge_predictions_s50-{}'.format(fold), lambda u: f(u))
    task()    


def merge_predictions_s60(fold):

    def f1(logger):
        outname = os.path.join(config.PRED_PATH, "step60_merged_101_fold{}.csv.gz".format(fold))
        assert not os.path.exists(outname), outname
        
        files = [os.path.join(config.PRED_PATH, "step60_{}_101_fold{}.csv.gz".format(u, fold)) for u in bond_path_list]
        for u in files:
            assert os.path.exists(u), u 
            
        df = pd.concat([pd.read_csv(u, index_col=0) for u in files])
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
        assert len(df) == TEST_SIZE
        logger.info("write " + outname)
        df.to_csv(outname + ".tmp", compression='gzip')
        shutil.move(outname + ".tmp", outname)

    def f2(logger):
        outname = os.path.join(config.PRED_PATH, "step60_merged_101_validpred_fold{}.csv.gz".format(fold))
        assert not os.path.exists(outname), outname
        
        files = [os.path.join(config.PRED_PATH, "step60_{}_101_validpred_fold{}.csv.gz".format(u, fold)) for u in bond_path_list]
        for u in files:
            assert os.path.exists(u), u 
            
        df = pd.concat([pd.read_csv(u, index_col=0) for u in files])
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
        logger.info("write " + outname)
        df.to_csv(outname + ".tmp", compression='gzip')
        shutil.move(outname + ".tmp", outname)

    def f(logger):
        f1(logger)
        f2(logger)
                
    task = Task('merge_predictions_s60-{}'.format(fold), lambda u: f(u))
    task()

       
def make_s60_feat():

    def f1(logger):
            
        files = [os.path.join(config.PRED_PATH, "step60_merged_101_fold{}.csv.gz".format(fold)) for fold in range(5)]
        for u in files:
            assert os.path.exists(u), u 
            
        df = pd.concat([pd.read_csv(u) for u in files])
        df.columns = ['id', 'step60_valid_pred']
        df = df.groupby("id").median()
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))        
        assert len(df) == TEST_SIZE
        return df 

    def f2(logger):
            
        files = [os.path.join(config.PRED_PATH, "step60_merged_101_validpred_fold{}.csv.gz".format(fold)) for fold in range(5)]
        for u in files:
            assert os.path.exists(u), u 
        df = pd.concat([pd.read_csv(u, index_col=0) for u in files])
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))

        #assert len(df) == TRAIN_SIZE
        
        return df 
    
    def f(logger):
            
        outname = os.path.join(config.FEAT_PATH, "traintest_step60_pred.parq")
        assert not os.path.exists(outname), outname
        
        df1 = f1(logger)
        df2 = f2(logger)
        
        df = pd.concat([df1, df2]).sort_index().astype(np.float32)
        
        logger.info("df.shape={}\n{}\n{}".format(df.shape, df.head(), df.tail()))
        logger.info("write " + outname)
        fastparquet.write(outname + ".tmp", df, compression='SNAPPY')
        shutil.move(outname + ".tmp", outname)
                
    task = Task('make_s60_feat', lambda u: f(u))
    task()    

                                              
def run():
    merge_predictions_s40()
    for i in range(10):
        merge_predictions_s50(i)
        
    for i in range(5):
        merge_predictions_s60(i)        
        
    make_s60_feat()


if __name__ == '__main__':
    run()

