import numpy as np
import scipy.stats
import sys
import myutils
from rdkit import Chem

COLUMNS =[ 
 'GetBondDir',
 'GetBondTypeAsDouble',
 'GetIsAromatic',
 'GetIsConjugated',
 'GetStereo',
 'IsInRing',
 ]


def invert_map(my_map):
    return {v: k for k, v in my_map.items()}

 

BondDir_dict = invert_map(
 {0: Chem.rdchem.BondDir.NONE, 1: Chem.rdchem.BondDir.BEGINWEDGE, 2: Chem.rdchem.BondDir.BEGINDASH, 3: Chem.rdchem.BondDir.ENDDOWNRIGHT, 4: Chem.rdchem.BondDir.ENDUPRIGHT, 5: Chem.rdchem.BondDir.EITHERDOUBLE, 6: Chem.rdchem.BondDir.UNKNOWN}    
)
BondStereo_dict = invert_map(
{0: Chem.rdchem.BondStereo.STEREONONE, 1: Chem.rdchem.BondStereo.STEREOANY, 2: Chem.rdchem.BondStereo.STEREOZ, 3: Chem.rdchem.BondStereo.STEREOE, 4: Chem.rdchem.BondStereo.STEREOCIS, 5: Chem.rdchem.BondStereo.STEREOTRANS}
)

def to_number(v):
    if isinstance(v, Chem.rdchem.BondDir):
        return BondDir_dict[v]
    elif isinstance(v, Chem.rdchem.BondStereo):
        return BondStereo_dict[v]
    else:
        return v


def make_feature(mol, atom_index1, atom_index2):
    if atom_index1 is None:
        return np.zeros(len(COLUMNS), dtype=np.float32)
    
    feats = []
    # names = []

    def f(u, bond):
        # names.append(u)
        fn = getattr(bond, u)
        v = fn()
        feats.append(to_number(v))         

    bond = mol.GetBondBetweenAtoms(atom_index1,atom_index2)
    for name in COLUMNS:
        f(name, bond)
    #print(list(zip(COLUMNS,feats)))
    return np.array(feats, dtype=np.float32)


if __name__ == '__main__':
    mol = Chem.MolFromSmiles('[C@H](Cl)(F)Br')
    feat = make_feature(mol, 0,1)
    print (len(feat))
    print (feat)

