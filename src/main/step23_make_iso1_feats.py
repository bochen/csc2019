import fastparquet 
import pandas as pd 
import numpy as np
import config
import myutils
from task import Task
import os
from tqdm import tqdm 
import dask.dataframe as dd 
import shutil

from networkx.algorithms import isomorphism
import networkx as nx
import atom_feature
import bond_feature
import bondpath_feature

from rdkit import Chem
import multiprocessing

logger = myutils.get_logger("step23_make_iso1_feats")


def get_constant_feat(df):

    def find_max_percentile(df):
        lst = []
        for col in df.columns:
            a = len(df) - df[col].value_counts().max()
            lst.append([col, a])
        return pd.DataFrame(lst, columns=['name', 'df'])

    def is_nullOrZero(s):
        return (s == 0) | s.isnull()

    adf = find_max_percentile(df)
    return adf['name'][is_nullOrZero(adf['df'])].tolist()


def __helper_is_duplicate_feats(args):
    global g_df, g_maxdf, g_mindf, g_zerocnt
    col_1, col_2 = args
    if g_maxdf[col_1] == g_maxdf[col_2] and g_mindf[col_1] == g_mindf[col_2] and g_zerocnt[col_1] == g_zerocnt[col_2]:
        if g_df[col_1].equals(g_df[col_2]):
            return True, col_2 
    return False, col_2


def parallell_for(pool, vec, fun, n_jobs):
    vec = list(vec)
    n = len(vec)
    chunk_size = int(np.ceil(max(10, float(n) / n_jobs)))
    print ("use chunk size " + str(chunk_size))
    ret = pool.map(fun, vec, chunk_size)
    return ret


def get_duplicated_feats(df):
    if len(df) > 100000:
        df = df.sample(n=100000)
    logger.info("make max ...")
    maxdf = df.max()
    logger.info("make min ...")
    mindf = df.min()
    logger.info("make zerocnt...")
    zerocnt = (df == 0).sum()
    
    # check for duplicated features in the training set
    duplicated_feat = []
    set_names = []

    nullcount = df.isnull().mean().to_dict()
    
    for x, y in nullcount.items():
        if y == 1:
            set_names.append(x)
            duplicated_feat.append([x, x])

    logger.info('set_names:' + str(set_names))

    logger.info("start to find ...")

    global g_df, g_maxdf, g_mindf, g_zerocnt
    g_df = df;g_maxdf = maxdf; g_mindf = mindf; g_zerocnt = zerocnt

    pool = multiprocessing.Pool(myutils.get_num_thread())

    for i in tqdm(range(0, len(df.columns))):
        # if i % 100 == 0:  # this helps me understand how the loop is going
        # print(i, len(set_names), end=' ')
        col_1 = df.columns[i]
        if col_1 in set_names:
            continue
        if len(df.columns[i + 1:]) < 100:
            for col_2 in df.columns[i + 1:]:
                # print(col_2, end=' ')
                if col_2 in set_names:
                    continue            
                if maxdf[col_1] == maxdf[col_2] and mindf[col_1] == mindf[col_2] and zerocnt[col_1] == zerocnt[col_2]:
                    if df[col_1].equals(df[col_2]):
                        duplicated_feat.append([col_1, col_2])
                        set_names.append(col_2)
        else:
            cand_columns = []
            for col_2 in df.columns[i + 1:]:
                if col_2 in set_names:
                    continue
                else:
                    cand_columns.append((col_1, col_2))
            
            bools = parallell_for(pool, cand_columns, __helper_is_duplicate_feats, myutils.get_num_thread())
            for u, col_2 in bools:
                if u:
                    duplicated_feat.append([col_1, col_2])
                    set_names.append(col_2)

    pool.terminate()
    pool.join()
            
    return (duplicated_feat)


class IsoMatcher():

    def __init__(self, G0):
        self.G0 = G0
        self.nodes = sorted(list(G0.nodes))
        self.edges = sorted(list(G0.edges))
        self.triplets = self.__make_triplets(G0)
        self.quadruplets = self.__make_quadruplets(G0)

    def __make_triplets(self, G):
        ret = []
        for i, j in self.edges:
            for u in G.neighbors(i):
                a = [u, i, j] 
                ret.append(tuple(a))
                
            for u in G.neighbors(j):
                a = [i, j, u] 
                ret.append(tuple(a))
                
        ret = [u for u in ret if len(set(u)) == 3]

        ret2 = [] 
        for u in ret:
            b = u[::-1]
            if u < b: b = u
            ret2.append(b)
                
        ret2 = sorted(list(set(ret2)))
        return ret2
    
    def __make_quadruplets(self, G):
        ret = []
        for i, j, k in self.triplets:
            for u in G.neighbors(i):
                a = [u, i, j, k] 
                ret.append(tuple(a))
                
            for u in G.neighbors(k):
                a = [i, j, k, u] 
                ret.append(tuple(a))
                
        ret = [u for u in ret if len(set(u)) == 4]

        ret2 = [] 
        for u in ret:
            b = u[::-1]
            if u < b: b = u
            ret2.append(b)
                
        ret2 = sorted(list(set(ret2)))
        return ret2    
    
    def make_mapped(self, G):
        GM = isomorphism.MultiGraphMatcher(self.G0, G, node_match=isomorphism.numerical_edge_match('atomic_num', -1))
        if not GM.is_isomorphic():
            print("error, not isomorphic")
            raise("error, not isomorphic")
            return False, None, None, None, None
        else:
            m = GM.mapping
            nodes = [m[i] for i in self.nodes]
            edges = [ tuple([m[j] for j in u]) for u in self.edges]
            triplets = [ tuple([m[j] for j in u]) for u in self.triplets]
            quadruplets = [ tuple([m[j] for j in u]) for u in self.quadruplets]
            return True, nodes, edges, triplets, quadruplets
        

def mol_to_nx(mol):
    if mol is None:
        return None
    G = nx.Graph()

    for atom in mol.GetAtoms():
        G.add_node(atom.GetIdx(),
                   atomic_num=atom.GetAtomicNum()
                   )
    for bond in mol.GetBonds():
        G.add_edge(bond.GetBeginAtomIdx(),
                   bond.GetEndAtomIdx())
    return G


def process_one_for_make_atom_feat(nodes, mol):
    
    feat = [ atom_feature.make_feature(mol, atom_index) for atom_index in nodes]
    feat = np.concatenate(feat).astype(np.float32)
    return feat


def process_one_for_make_bond_feat(edges, mol):
    feat = [ bond_feature.make_feature(mol, atom_index[0], atom_index[1]) for atom_index in edges]
    feat = np.concatenate(feat).astype(np.float32)
    return feat     


def process_one_for_make_bondpath_feat(mol, splist):
    if len(splist) == 0: return np.zeros(1, dtype=np.float32) 
    feat = [bondpath_feature.make_feature(mol, sp) for sp in splist]
    feat = np.concatenate(feat).astype(np.float32)
    return feat


def process_one(layername, layerval, sublayers):
    if len(sublayers) == 0 or not sublayers.iloc[0]['submol']:
        logger.warn("{} no {} is empty".format(layername, layerval))
        return None
    
    outname = os.path.join(config.FEAT_PATH, "traintest_{}_{}_fullfeat.parq".format(layername, layerval))
    if os.path.exists(outname):
        logger.info("{} exists. Skip".format(outname))
        return

    sublayers['submol'] = [Chem.Mol(u) for u in tqdm(sublayers['submol'].values)]
    Glist = [mol_to_nx(u) for u in tqdm(sublayers['submol'].values)]
    G0 = Glist[0]
    
    logger.info("start iso matching, #={}".format(len(Glist)))
    logger.info("use instance {} as standard mol".format(sublayers.iloc[0].name))
    matcher = IsoMatcher(G0)
    matcher_results = []
    for i, G in enumerate(tqdm(Glist)):
        rowid = sublayers.iloc[i].name
        a = matcher.make_mapped(G)
        if a[0]:
            matcher_results.append([ rowid, a[1:]])
        else:
            logger.error("instance {} is not iso with standard".foramt(rowid))
    
    logger.info("end iso matching, #={}".format(len(matcher_results)))
    
    logger.info("start making feats")
    index = []
    feats = []
    for rowid, row_mapping in tqdm(matcher_results):
        index.append(rowid)
        nodes, edges, triplets, quadruplets = row_mapping
        mol = sublayers.loc[rowid, 'submol']
        a = process_one_for_make_atom_feat(nodes, mol)
        b = process_one_for_make_bond_feat(edges, mol)
        c = process_one_for_make_bondpath_feat(mol, triplets)
        d = process_one_for_make_bondpath_feat(mol, quadruplets)
        feat = np.concatenate([a, b, c, d])
        feats.append(feat)
    
    df = pd.DataFrame(feats, index=index)
    df.index.name = 'id'
    df.columns = ['ss_{}_{}_{}'.format(layername, layerval, u) for u in range(df.shape[1])]
    logger.info("end making feats. df.shape=" + str(df.shape))
    
    logger.info("to find constant columns")
    const_cols = get_constant_feat(df)
    logger.info("found constant #=" + str(len(const_cols)))
    logger.info("found constant " + str(const_cols))
    df = df.loc[:, ~df.columns.isin(const_cols)]
    
    logger.info("to find duplicate columns")
    dup_cols = [u[1] for u in get_duplicated_feats(df)]
    logger.info("found duplicates #=" + str(len(dup_cols)))
    logger.info("found duplicates " + str(dup_cols))
    df = df.loc[:, ~df.columns.isin(dup_cols)]
    
    df = df.sort_index()

    logger.info("df.shape={}\n{}".format(df.shape, df.head()))

    logger.info("write " + outname + ".bk")
    fastparquet.write(outname + ".bk", df, compression='SNAPPY')            
    shutil.move(outname + ".bk", outname)


def make_iso_mol_full_feats(submol_seqno):               

    def make_layer_info():
        layers = dd.read_parquet(os.path.join(config.FEAT_PATH, "traintest_layers.parq")).compute()        
        info = {}
        for colname in ['layer1', 'layer2', 'layer3', 'layer4']:
            vc = layers[colname].value_counts()
            vc = vc[vc > 500]
            info[colname] = vc.to_dict()
        return info, layers

    def f(logger):
        
        assert submol_seqno in [1, 2]
        
        info, layers = make_layer_info()
        
        layername = 'layer3' if submol_seqno == 1 else "layer4"
        
        layer_dict = info[layername]
        
        summols = dd.read_parquet(os.path.join(config.INPUT_PATH, "traintest_submols.parq")).iloc[:, submol_seqno].compute()
        layers['submol'] = summols
        
        for layerno in layer_dict:
            sublayers = layers[layers[layername] == layerno]
            logger.info("processing {} no {}, #={}".format(layername, layerno, len(sublayers)))
            process_one(layername, layerno, sublayers)
            # break
            
    task = Task('make_iso_mol_full_feats-' + str(submol_seqno), lambda u: f(u))
    task()    

                                  
def run():
    make_iso_mol_full_feats(1)
    make_iso_mol_full_feats(2)
    

if __name__ == '__main__':
    run()

