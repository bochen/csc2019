import fastparquet 
import pandas as pd 
import numpy as np
import config
import myutils
from task import Task
import os
from joblib import Parallel, delayed
from multiprocessing import Pool
import sys
import data
from rdkit import Chem
from tqdm import tqdm 
import molecule_feature


def make_qm9_feat():               

    def f(logger):
        fpath = os.path.join(config.INPUT_PATH, 'qm9', 'qm9.csv')

        def g(name):
            name = int(name.split("_")[-1])
            name = "dsgdb9nsd_{:06}".format(name)
            return name

        df = pd.read_csv(fpath)
        df['molecule_name'] = df['mol_id'].map(g)
        df = df.drop(['mol_id', 'smiles'], axis=1)
        
        logger.info("{}\n{}".format(len(df), df.head()))
        
        molnames = set(data.load_structures()['molecule_name'].values)
        
        logger.info("Total have {} molecules".format(len(molnames)))
        df = df[df['molecule_name'].isin(molnames)]

        logger.info("From qm9 there are {} molecules".format(len(set(df['molecule_name']))))
        df = df.set_index('molecule_name')
        df.columns = ["qm9_{}".format(u) for u in df.columns]        
        
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
            
        outname = os.path.join(config.FEAT_PATH, "traintest_mol_qm9_feat.parq")
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY')
        
    task = Task('make_qm9_feat', lambda u: f(u))
    task()


def make_rdKitDescriptors_feat():               

    def f(logger):
        molsdict = data.load_mols()

        molnames = sorted(list(molsdict.keys()))
        logger.info("Total have {} molecules".format(len(molnames)))
        
        feats = []
        N = len(molnames)
        for molname in tqdm(molnames[:N]):
            mol = molsdict[molname]
            feats.append(molecule_feature.make_RDKitDescriptors_feature(mol))
            
        columns = molecule_feature.rdKitDescriptors_COLUMNS
        df = pd.DataFrame(feats, columns=columns)
        
        df.index = molnames[:N]
        
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
            
        outname = os.path.join(config.FEAT_PATH, "traintest_mol_rdkitDesc.parq")
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY')            
        
    task = Task('make_rdKitDescriptors_feat', lambda u: f(u))
    task()


def make_CoulombMatrixEig_feat():               

    def f(logger):
        molsdict = data.load_mols()

        molnames = sorted(list(molsdict.keys()))
        logger.info("Total have {} molecules".format(len(molnames)))
        
        feats = []
        N = len(molnames)
        for molname in tqdm(molnames[:N]):
            mol = molsdict[molname]
            feats.append(molecule_feature.make_CoulombMatrixEig_feature(mol))
            
        columns = molecule_feature.dcCoulombMatrixEng_COLUMNS
        df = pd.DataFrame(feats, columns=columns)
        
        df.index = molnames[:N]
        
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
            
        outname = os.path.join(config.FEAT_PATH, "traintest_mol_CoulombMatrixEig.parq")
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY')            
        
    task = Task('make_CoulombMatrixEig_feat', lambda u: f(u))
    task()


def make_AtomPairFingerprint_feat():               

    def f(logger):
        molsdict = data.load_mols()

        molnames = sorted(list(molsdict.keys()))
        logger.info("Total have {} molecules".format(len(molnames)))
        
        feats = []
        N = len(molnames)
        for molname in tqdm(molnames[:N]):
            mol = molsdict[molname]
            feats.append(molecule_feature.make_AtomPairFingerprint_feature(mol))
            
        columns = molecule_feature.AtomPairFingerprint_COLUMNS
        df = pd.DataFrame(feats, columns=columns)
        
        df.index = molnames[:N]
        
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
            
        outname = os.path.join(config.FEAT_PATH, "traintest_mol_AtomPairFingerprint.parq")
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY')            
        
    task = Task('make_AtomPairFingerprint_feat', lambda u: f(u))
    task()

    
def make_MorganFingerprint_feat():               

    def f(logger):
        molsdict = data.load_mols()

        molnames = sorted(list(molsdict.keys()))
        logger.info("Total have {} molecules".format(len(molnames)))
        
        feats = []
        N = len(molnames)
        for molname in tqdm(molnames[:N]):
            mol = molsdict[molname]
            feats.append(molecule_feature.make_MorganFingerprint_feature(mol))
            
        columns = molecule_feature.MorganFingerprint_COLUMNS
        df = pd.DataFrame(feats, columns=columns)
        
        df.index = molnames[:N]
        
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
            
        outname = os.path.join(config.FEAT_PATH, "traintest_mol_MorganFingerprint.parq")
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY')            
        
    task = Task('make_MorganFingerprint_feat', lambda u: f(u))
    task()    


def make_TopologicalTorsionFingerprint_feat():               

    def f(logger):
        molsdict = data.load_mols()

        molnames = sorted(list(molsdict.keys()))
        logger.info("Total have {} molecules".format(len(molnames)))
        
        feats = []
        N = len(molnames)
        for molname in tqdm(molnames[:N]):
            mol = molsdict[molname]
            feats.append(molecule_feature.make_TopologicalTorsionFingerprint_feature(mol))
            
        columns = molecule_feature.TopologicalTorsionFingerprint_COLUMNS
        df = pd.DataFrame(feats, columns=columns)
        
        df.index = molnames[:N]
        
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
            
        outname = os.path.join(config.FEAT_PATH, "traintest_mol_TopologicalTorsionFingerprint.parq")
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY')            
        
    task = Task('make_TopologicalTorsionFingerprint_feat', lambda u: f(u))
    task()    

                                  
def run():
    make_qm9_feat()
    make_rdKitDescriptors_feat()
    make_CoulombMatrixEig_feat()
    make_AtomPairFingerprint_feat()
    make_MorganFingerprint_feat()
    make_TopologicalTorsionFingerprint_feat()


if __name__ == '__main__':
    run()

