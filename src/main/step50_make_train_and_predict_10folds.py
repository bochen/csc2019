import numpy as np
import config
import myutils
from task import Task
import os
import data
from tqdm import tqdm 
from train import __train_a_model, medpred, split_fold, log_abs_error
import shutil
import pandas as pd 
from step40_make_train_and_predict import prepare_data

logger = myutils.get_logger("step40_make_train_and_predict")


def make_train_and_predict_10fold(bp, n_estimators):

    def __train(X_train, y_train, logger):
            
        logger.info("data shape: " + str([u.shape for u in [X_train, y_train]]))
        
        if 1:
            regressor = __train_a_model(X_train, y_train, 'mse', random_state=None, n_estimators=n_estimators, model_type='erf', bootstrap=False, n_jobs=myutils.get_num_thread())
        
            return regressor
            
    def f(logger):
        
        logger.info("reading data ...")
        traindf, trainy, trainmols, testdf = prepare_data(bp)
        
        split_list = split_fold(traindf, trainy, trainmols, n_fold=10, random_state=None)
        for i, (X_train, X_valid, y_train, y_valid) in enumerate(split_list):
            logger.info("training fold " + str(i))
            regressor = __train(X_train, y_train, logger)
            validpred = medpred(regressor, X_valid)
            validloss = log_abs_error(validpred, y_valid.values)
            logger.info("fold [{}], valid loss: {}".format(i, validloss))
            
            testpred = medpred(regressor, testdf.values)
            testpred = pd.Series(testpred, index=testdf.index).to_frame().reset_index(drop=False)
            testpred.columns = ['id', 'scalar_coupling_constant']
            
            outpath = os.path.join(config.PRED_PATH, "step50_{}_{}_fold{}.csv.gz".format(bp, n_estimators, i))
            testpred.to_csv(outpath + ".tmp", index=None, compression='gzip')
            shutil.move(outpath + ".tmp", outpath)
        
    task = Task('make_train_and_predict_10fold-{}-{}'.format(bp, n_estimators), lambda u: f(u))
    task()    

                                          
def run():
    bond_path_list = ['HCCC', 'HCC', 'HC', 'HCCH', 'HCH', 'HCCN', 'HCNC', 'HCN', 'HCOC', 'HNCC', 'HNC', 'HOCC', 'HN', 'HCNH', 'HOC', 'HCOH', 'HNCN', 'HNH', 'HCNN', 'HNN', 'HOCN', 'HNNC', 'HONC', 'HON', 'HCON', 'HNNN', ]
    bond_path_list = bond_path_list[::-1]
    for bp in bond_path_list:
        make_train_and_predict_10fold(bp, n_estimators=101)


if __name__ == '__main__':
    run()

