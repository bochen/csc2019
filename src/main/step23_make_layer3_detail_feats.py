import fastparquet 
import config
import myutils
from task import Task
import os
from tqdm import tqdm 

from joblib import Parallel, delayed
import numpy as np 
import pandas as pd 
import shutil
import dask.dataframe as dd 
import data
import bondpath_feature
from step23_make_iso1_feats import get_constant_feat, get_duplicated_feats
import json
import molutils

logger = myutils.get_logger("step23_make_layer3_detail_feats")

    
def make_input_data():
    bp = data.load_bond_paths()
    bp = bp.set_index("id")
    mols = data.load_mols()
    layers = dd.read_parquet(os.path.join(config.FEAT_PATH, "traintest_layers.parq")).compute()
    
    return bp, mols, layers


def get_bonds(mol):
    lst = []
    for bond in mol.GetBonds():
        a, b = bond.GetBeginAtom(), bond.GetEndAtom()
        lst.append([a.GetIdx(), b.GetIdx()])
        lst.append([b.GetIdx(), a.GetIdx()])
    lst = [tuple(u) for u in lst]
    lst = list(set(lst))
    return lst


def get_triplets(mol):
    newlst = []
    lst = get_bonds(mol)
    for u in lst:
        a = mol.GetAtomWithIdx(u[-1])
        for b in a.GetNeighbors():
            newlst.append(list(u) + [b.GetIdx()])
    newlst = [u for u in newlst if len(set(u)) == 3]
    newlst = [tuple(u) for u in newlst]
    newlst = list(set(newlst))
    return newlst


def get_quadruplets(mol):
    newlst = []
    lst = get_triplets(mol)
    for u in lst:
        a = mol.GetAtomWithIdx(u[-1])
        for b in a.GetNeighbors():
            assert molutils.has_bond(mol, u[-1], b.GetIdx()), "{}-{}-{}".format(mol._Name, u[-1],b.GetIdx())
            newlst.append(list(u) + [b.GetIdx()])
    newlst = [u for u in newlst if len(set(u)) == 4]
    newlst = [tuple(u) for u in newlst]
    newlst = list(set(newlst))
    return newlst


def remove_duplicates(tuples):
    ret = []
    for a in tuples:
        if a > a[::-1]:
            a = a[::-1]
        ret.append(a)
    ret = list(set(ret))
    return ret


def tuple_to_tuple(a, b):  # b sp
    ret = [["out", []]]
    for i in a:
        if i in b:
            k = b.index(i)
            if ret[-1][0] == 'out':
                ret.append(['in', [(i, k)]])
            else:
                ret[-1][1].append((i, k))
        else:
            if ret[-1][0] == 'out':
                ret[-1][1].append((i, -1))
            else:
                ret.append(['out', [(i, -1)]])
    
    ret = [u for u in ret if len(u[1]) > 0]
    ret = [(u[0], tuple(u[1])) for u in ret]
    ret = tuple(ret)
    return ret, tuple([-len(u[1]) if u[0] == 'out' else len(u[1]) for u in ret])


def index_to_symbols(mol, a):
    return "".join([mol.GetAtomWithIdx(i).GetSymbol() for i in a])

    
def tuple_to_tuple_with_mol(mol, a, sp):  # b sp
    flatten = lambda l: tuple([item for sublist in l for item in sublist])
    ret, matchtype = tuple_to_tuple(a, sp)
    is_sym = False
    symbols = index_to_symbols(mol, a)
    if matchtype == matchtype[::-1]:
        inv_symbols = symbols[::-1]
        if symbols == inv_symbols:
            is_sym = True
        else:
            if symbols > inv_symbols:
                symbols = inv_symbols
                newret=[]
                for x in ret[::-1]:
                    newret.append((x[0],x[1][::-1]))
                ret=newret
    b = flatten([[v[1] for v in u[1]] for u in ret if u[0] == 'in'])
    if b:
        g = lambda t: ".".join([str(x) for x in t])
        thistuple = tuple(flatten([[v[0] for v in u[1]] for u in ret]))
        return ret, "_".join([symbols, g(b), g(matchtype)]), thistuple, is_sym
    else:
        return None


def process_one_4_tuples(sp, mol, tuplefun, aggfun):
    ret = {}
    tuples = remove_duplicates(tuplefun(mol))
    for a in tuples:
        tmp = tuple_to_tuple_with_mol(mol, a, sp)
        if tmp is not None:
            _, tuplename, thistuple, is_sym = tmp
            v = bondpath_feature.make_feature(mol, thistuple)
            if is_sym:
                v2 = bondpath_feature.make_feature(mol, thistuple[::-1])
                v = (v + v2) / 2.0
            assert len(v) == 23                
            if tuplename not in ret:
                ret[tuplename] = []
            ret[tuplename].append(v)
    ret = {k:aggfun(v) for k, v in ret.items()} 
    return ret

    
def process_one_4_triplets(sp, mol, aggfun):
    return process_one_4_tuples(sp, mol, get_triplets, aggfun)

 
def process_one_4_quadruplets(sp, mol, aggfun):
    return process_one_4_tuples(sp, mol, get_quadruplets, aggfun)

    
def process_one(idxid, sp, mol, columns, aggfunname):
    if aggfunname == 'mean':
        aggfun = lambda u: np.nanmean(u, 0)
    elif aggfunname == 'min':
        aggfun = lambda u: np.nanmin(u, 0)
    elif aggfunname == 'max':
        aggfun = lambda u: np.nanmax(u, 0)
                
    retdict = process_one_4_triplets(sp, mol, aggfun)
    retdict.update(process_one_4_quadruplets(sp, mol, aggfun))
    
    vec = []
    for col in columns:
        if col in retdict:
            vec.append(retdict[col])
        else:
            vec.append(np.zeros(23, dtype=np.float32) + np.nan)
    vec = np.concatenate(vec).astype(np.float32)
    
    return idxid, vec 

    
def make_layer3_detail_feat(layerval, columns, rawbp, mols, layers, aggfunname):               

    def f(logger):
        outname = os.path.join(config.FEAT_PATH, "traintest_{}_{}_detail_{}.parq".format('layer3', layerval, aggfunname))
        if os.path.exists(outname):
            logger.info("{} exists. Skip".format(outname))
            return
    
        logger.info("has {} columns".format(len(columns)))
        bp = rawbp.loc[layers[layers['layer3'] == layerval].index]
        bp['mol'] = bp['molname'].map(lambda u: mols[u])

        inputlst = []
        for i in range(len(bp)):
            row = bp.iloc[i]
            inputlst.append([row.name, row['shorted_path'], row['mol'], columns,aggfunname])
        
        results = Parallel(n_jobs=myutils.get_num_thread())(delayed(process_one)(*v) for v in tqdm(inputlst))

        index = [u[0] for u in results]
        arr = [u[1] for u in results]
        logger.info("make dataframe, shape1=" + str(len(arr[0])))
        
        df = pd.DataFrame(arr, index=index, dtype=np.float32, columns=[ "{}_{}_{}".format(u, v, aggfunname) for u in columns for v in range(23)])
        
        logger.info("to find constant columns")
        const_cols = get_constant_feat(df)
        logger.info("found constant #=" + str(len(const_cols)))
        logger.info("found constant " + str(const_cols))
        df = df.loc[:, ~df.columns.isin(const_cols)]
        
        logger.info("to find duplicate columns")
        dup_cols = [u[1] for u in get_duplicated_feats(df)]
        logger.info("found duplicates #=" + str(len(dup_cols)))
        logger.info("found duplicates " + str(dup_cols))
        df = df.loc[:, ~df.columns.isin(dup_cols)]
        
        df = df.sort_index()
    
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
    
        logger.info("write " + outname + ".bk")
        fastparquet.write(outname + ".bk", df, compression='SNAPPY')            
        shutil.move(outname + ".bk", outname)
            
    task = Task('make_layer3_detail_feat-{}-{}'.format(layerval, aggfunname) , lambda u: f(u))
    task()    

    
def make_path_detail_feat(bond_path, columns, rawbp, mols, layers, aggfunname):               

    def f(logger):
        outname = os.path.join(config.FEAT_PATH, "traintest_{}_{}_detail_{}.parq".format('bp', bond_path, aggfunname))
        if os.path.exists(outname):
            logger.info("{} exists. Skip".format(outname))
            return
    
        logger.info("has {} columns".format(len(columns)))
        bp = rawbp.loc[layers[layers['bond_paths'] == bond_path].index]
        bp['mol'] = bp['molname'].map(lambda u: mols[u])

        inputlst = []
        for i in range(len(bp)):
            row = bp.iloc[i]
            inputlst.append([row.name, row['shorted_path'], row['mol'], columns,aggfunname])
        
        results = Parallel(n_jobs=myutils.get_num_thread())(delayed(process_one)(*v) for v in tqdm(inputlst))

        index = [u[0] for u in results]
        arr = [u[1] for u in results]
        logger.info("make dataframe, shape1=" + str(len(arr[0])))
        
        df = pd.DataFrame(arr, index=index, dtype=np.float32, columns=[ "{}_{}_{}".format(u, v, aggfunname) for u in columns for v in range(23)])
        
        logger.info("to find constant columns")
        const_cols = get_constant_feat(df)
        logger.info("found constant #=" + str(len(const_cols)))
        logger.info("found constant " + str(const_cols))
        df = df.loc[:, ~df.columns.isin(const_cols)]
        
        logger.info("to find duplicate columns")
        dup_cols = [u[1] for u in get_duplicated_feats(df)]
        logger.info("found duplicates #=" + str(len(dup_cols)))
        logger.info("found duplicates " + str(dup_cols))
        df = df.loc[:, ~df.columns.isin(dup_cols)]
        
        df = df.sort_index()
    
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
    
        logger.info("write " + outname + ".bk")
        fastparquet.write(outname + ".bk", df, compression='SNAPPY')            
        shutil.move(outname + ".bk", outname)
            
    task = Task('make_path_detail_feat-{}-{}'.format(bond_path, aggfunname) , lambda u: f(u))
    task()    


def make_path_detail_feat_big(bond_path, columns, rawbp, mols, layers, aggfunname):               

    def run_one_part(k, bp, N, logger, columns):
        outname = os.path.join(config.FEAT_PATH, "traintest_{}_{}_detail_{}_part{}.parq".format('bp', bond_path, aggfunname, 0))
        need_check_columns=True
        if os.path.exists(outname):
            detail_columns = list(dd.read_parquet(outname).columns)
            assert len(detail_columns)>0
            need_check_columns=False
            logger.info("use previous columns")
            logger.info("#detail_columns=" + str(len(detail_columns)))
            logger.info("head detail_columns=" + str(detail_columns[:5]))
        logger.info("#columns=" + str(len(columns)))
        logger.info("head columns=" + str(columns[:5]))
            
        outname = os.path.join(config.FEAT_PATH, "traintest_{}_{}_detail_{}_part{}.parq".format('bp', bond_path, aggfunname, k))
        if os.path.exists(outname):
            logger.info("{} exists. Skip".format(outname))
            return outname
        
        bp = bp.loc[bp.index.map(lambda u: u % N == k)]
        logger.info("bp shape=" + str(bp.shape))
        inputlst = []
        for i in range(len(bp)):
            row = bp.iloc[i]
            inputlst.append([row.name, row['shorted_path'], row['mol'], columns,aggfunname])
        
        results = Parallel(n_jobs=myutils.get_num_thread())(delayed(process_one)(*v) for v in tqdm(inputlst))

        logger.info("making dataframe ...")
        index = [u[0] for u in results]
        arr = [u[1] for u in results]
        logger.info("make dataframe, shape1=" + str(len(arr[0])))
        
        df = pd.DataFrame(arr, index=index, dtype=np.float32, columns=[ "{}_{}_{}".format(u, v, aggfunname) for u in columns for v in range(23)])
        if need_check_columns:
            logger.info("to find constant columns")
            const_cols = get_constant_feat(df)
            logger.info("found constant #=" + str(len(const_cols)))
            logger.info("found constant " + str(const_cols))
            df = df.loc[:, ~df.columns.isin(const_cols)]
            
            logger.info("to find duplicate columns")
            dup_cols = [u[1] for u in get_duplicated_feats(df)]
            logger.info("found duplicates #=" + str(len(dup_cols)))
            logger.info("found duplicates " + str(dup_cols))
            df = df.loc[:, ~df.columns.isin(dup_cols)]
        else:
            df = df.loc[:, df.columns.isin(detail_columns)]
        
        df = df.sort_index()
    
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
    
        logger.info("write " + outname + ".bk")
        fastparquet.write(outname + ".bk", df, compression='SNAPPY')            
        shutil.move(outname + ".bk", outname)
        return outname

    def f(logger):
        outname = os.path.join(config.FEAT_PATH, "traintest_{}_{}_detail_{}.parq".format('bp', bond_path, aggfunname))
        if os.path.exists(outname):
            logger.info("{} exists. Skip".format(outname))
            return
    
        logger.info("has {} columns".format(len(columns)))
        
        bp = rawbp.loc[layers[layers['bond_paths'] == bond_path].index]
        bp['mol'] = bp['molname'].map(lambda u: mols[u])
        
        N = int(np.ceil(len(bp) / (100 * 1000)))
        outfiles = []
        for k in range(N):
            outfiles.append(run_one_part(k, bp, N, logger,columns))
        
        df = dd.read_parquet(outfiles).compute().sort_index()
    
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
    
        logger.info("write " + outname + ".bk")
        fastparquet.write(outname + ".bk", df, compression='SNAPPY')            
        shutil.move(outname + ".bk", outname)
        return outname
                    
    task = Task('make_path_detail_feat-{}-{}'.format(bond_path, aggfunname) , lambda u: f(u))
    task()    

    
def run_layer3(bp, mols, layers, aggfunname):
    vc = layers[layers['is_train']]['layer3'].value_counts().sort_values(ascending=False)
    vc = vc[vc > 1000]
    logger.info("have {} layer values to make".format(len(vc)))
    
    for layerval in vc.index:
        columns = [] 
        jsonfile = os.path.join(config.INFO_PATH, 'layer3_{}_triplet_column_counts.json'.format(layerval))
        if not os.path.exists(jsonfile):
            logger.warn("skip {} due to missing {}".format(layerval, jsonfile)) 
        else:
            with open(os.path.join(config.INFO_PATH, 'layer3_{}_triplet_column_counts.json'.format(layerval)), 'rt') as fout:
                columns += list(json.load(fout).keys())
            with open(os.path.join(config.INFO_PATH, 'layer3_{}_quadruplet_column_counts.json'.format(layerval)), 'rt') as fout:
                columns += list(json.load(fout).keys())
                            
            make_layer3_detail_feat(layerval, columns, bp, mols, layers, aggfunname)


def run_bond_path(bp, mols, layers, aggfunname):

    vc = layers[layers['is_train']]['bond_paths'].value_counts().sort_values(ascending=True)
    vc = vc[vc > 1000]
    #vc = vc.head()
    logger.info("have {} bond path values to make".format(len(vc)))
    
    for bond_path in vc.index:
        columns = [] 
        jsonfile = os.path.join(config.INFO_PATH, 'path_{}_triplet_column_counts.json'.format(bond_path))
        if not os.path.exists(jsonfile):
            logger.warn("skip {} due to missing {}".format(bond_path, jsonfile)) 
        else:
            with open(os.path.join(config.INFO_PATH, 'path_{}_triplet_column_counts.json'.format(bond_path)), 'rt') as fout:
                columns += list(json.load(fout).keys())
            with open(os.path.join(config.INFO_PATH, 'path_{}_quadruplet_column_counts.json'.format(bond_path)), 'rt') as fout:
                columns += list(json.load(fout).keys())
            if bond_path not in ['HCCH', 'HCH', 'HC', 'HCC', 'HCCC']:
                make_path_detail_feat(bond_path, columns, bp, mols, layers, aggfunname)
            else:
                make_path_detail_feat_big(bond_path, columns, bp, mols, layers, aggfunname)

def run_merge_bond_path(layers):

    def f(logger):
        vc = layers[layers['is_train']]['bond_paths'].value_counts().sort_values(ascending=True)
        vc = vc[vc > 1000]
        #vc = vc.head()
        logger.info("have {} bond path values to make".format(len(vc)))
        
        for bond_path in vc.index:
                           
            outname = os.path.join(config.FEAT_PATH, "traintest_{}_{}_detail_{}.parq".format('bp', bond_path, "merged"))
            if os.path.exists(outname):
                logger.info("{} exists. Skip".format(outname))
                return
            
            inputs = [
                os.path.join(config.FEAT_PATH, "traintest_{}_{}_detail_{}.parq".format('bp', bond_path, u))
                for u in ['mean','max','min']
                ]
            for u in inputs:
                assert os.path.exists(u), u
            
            logger.info("start "+bond_path)
            df=pd.concat([dd.read_parquet(u).compute() for u in inputs], axis=1)
            logger.info("to find duplicate columns for "+bond_path)
            dup_cols = [u[1] for u in get_duplicated_feats(df)]
            logger.info("found duplicates #=" + str(len(dup_cols)))
            logger.info("found duplicates " + str(dup_cols))
            df = df.loc[:, ~df.columns.isin(dup_cols)]

            logger.info("df.shape={}\n{}".format(df.shape, df.head()))
            fastparquet.write(outname + ".bk", df, compression='SNAPPY')            
            shutil.move(outname + ".bk", outname)
     
                    
    task = Task('run_merge_bond_path_abc' , lambda u: f(u))
    task()  
    
       
def run():
    bp, mols, layers = make_input_data()
    if 1:
        #for aggfunname in ['mean', 'min', 'max']:
        for aggfunname in ['max', 'min', 'mean']:
            run_bond_path(bp, mols, layers, aggfunname)
            
        run_merge_bond_path(layers)
    if 0:
        for aggfunname in ['mean', 'min', 'max']:
            run_layer3(bp, mols, layers, aggfunname)
    

if __name__ == '__main__':
    run()

