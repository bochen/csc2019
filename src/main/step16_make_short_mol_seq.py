import fastparquet 
import pandas as pd 
import numpy as np
import config
import myutils
from task import Task
import os
from joblib import Parallel, delayed
from multiprocessing import Pool
import sys
import data
from tqdm import tqdm 
import pickle

logger = myutils.get_logger("step16_make_short_mol_seq")

def process_one_4_transform_longseq_to_shortseq(fileidx, filename):
    fine_structure = fastparquet.ParquetFile(os.path.join(config.FEAT_PATH, "fine_structure.parq")).to_pandas()
    id_to_nj = fine_structure['nj'].to_dict()

    if 1:
        logger.info(" reading " + filename)
        tmp = myutils.load_zipped_pickle(filename)
        values = tmp['value']
        keys = tmp['index']
        assert len(values) == len(keys)
        logger.info("# records: " + str(len(values)))
        
        newvalues = []
        tmp = tqdm(zip(keys, values)) if fileidx==0 else zip(keys,values)
        for idx, vv in tmp:
            nj = id_to_nj[idx]
            vv = myutils.uncompress_np_array(vv)
            vv = vv[:5] 
            if nj == 2:
                vv[-1] = vv[-1] * 0
            elif nj == 3:
                vv[-2] = vv[-2] * 0
                vv[-1] = vv[-1] * 0
            newvalues.append(myutils.compress_np_array(vv))
        outpath =  filename.replace("_feat_", "_short_feat_").replace("nni",'nni2')
        logger.info("writing " + outpath)
        myutils.save_zipped_pickle({'value':newvalues, "index":keys}, outpath) 

def transform_longseq_to_shortseq():

    def f(logger):
        datafiles = myutils.get_datafiles()
        results = Parallel(n_jobs=myutils.get_num_thread())(delayed(process_one_4_transform_longseq_to_shortseq)(idx, vv) for idx, vv in enumerate(datafiles))
                    
    task = Task('transform_longseq_to_shortseq', lambda u: f(u))
    task()

                   
def run():
    transform_longseq_to_shortseq()


if __name__ == '__main__':
    run()

