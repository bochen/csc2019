import sys
import pandas as pd 
import numpy as np
import config
import myutils
import os
import sys
import data
import time
import keras
import net

from keras import backend as K
use_gpu = len(K.tensorflow_backend._get_available_gpus())>0
print ("use cudnn", use_gpu)

datafiles=myutils.get_datafiles()
print (datafiles)

data_std=myutils.get_feat_std()
print (data_std.shape)

model_type=0

gen = net.DataGeneratorPar(datafiles, data_size=7163633, batch_size=64, data_std=data_std, model_type=model_type)
ckpt = keras.callbacks.ModelCheckpoint("model_epoch_{epoch:03d}_loss_{loss:.4f}.hdf5", monitor='loss', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)

model=net.make_model(model_type,hidden_units=100,bidirectional=True, use_cudnn=True)

loss = lambda y_true, y_pred: 1000 * keras.losses.mse(y_true, y_pred)

model.compile(optimizer='adam', loss=loss)    

model.fit_generator(generator=gen, validation_data=None, epochs=100,use_multiprocessing=True,
                     workers=12, callbacks=[ckpt], max_queue_size=128)
