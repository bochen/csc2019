'''
Created on Apr 6, 2018

@author: bo
'''

import os
import myutils

if 'CSC_HOME' not in os.environ:
    HOME = os.path.join(os.environ['HOME'], 'csc2019')
else:
    HOME = os.environ['CSC_HOME']

assert HOME is not None 

INPUT_PATH = os.path.join(HOME, "input")
TASK_PATH = os.path.join(INPUT_PATH, 'task')
NNI_PATH = os.path.join(INPUT_PATH, 'nni')
NNI2_PATH = os.path.join(INPUT_PATH, 'nni2')
NNI3_PATH = os.path.join(INPUT_PATH, 'nni3')
INFO_PATH = os.path.join(INPUT_PATH, 'info')
FEAT_PATH = os.path.join(INPUT_PATH, 'feat')
TAR_PATH = os.path.join(INPUT_PATH, 'tar')
PRED_PATH = os.path.join(INPUT_PATH, 'pred')

COMPRESSED_PATH = os.path.join(INPUT_PATH, 'compressed')
[myutils.create_dir_if_not_exists(directory) for directory in [INPUT_PATH, TASK_PATH, INFO_PATH, FEAT_PATH, COMPRESSED_PATH, TAR_PATH, NNI_PATH, NNI2_PATH, NNI3_PATH, PRED_PATH]]

TRAIN_ZIP = os.path.join(INPUT_PATH, 'train.csv.zip')
TRAIN_PARQ = os.path.join(INPUT_PATH, 'train.parq')

TEST_ZIP = os.path.join(INPUT_PATH, 'test.zip')
TEST_PARQ = os.path.join(INPUT_PATH, 'test.parq')

IDX_TEST_START = 4658147

