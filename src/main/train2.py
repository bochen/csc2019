'''
Created on Jun 29, 2019

@author:  Lizhen Shi
'''

import sys, os
import pandas as pd
import numpy as np 
import data
import myutils

import config
from train import split_fold, __train_a_model, log_abs_error, medpred
from sklearn.metrics import mean_absolute_error

logger = myutils.get_logger("train2")
     
    
def train_kfold(njnum=None, njtype=None, bondpath=None, feat_categories_list=None, exclude_columns=None, model_type='tree',
          loss='mse', random_state=None, n_estimators=101, n_jobs=-1, selected_index=None, sample=None, n_fold=5, bootstrap=False):

    def prepare_data(feat_categories_list):
        data_dict = {}
        for feat_categories in feat_categories_list:
            traindf, trainy, trainmols, _, _, _ = data.load_data(njnum=njnum, njtype=njtype, bonpath=bondpath,
                                                     feat_categories=feat_categories, exclude_columns=exclude_columns,
                                                     na_fill=-9999, random_state=random_state, selected_index=selected_index, sample=sample)        
            data_dict[feat_categories] = (traindf, trainy, trainmols)
        return data_dict 

    def __train(sub_feat_categories_list):
        if len(sub_feat_categories_list) == 1:
            traindf, trainy, trainmols = data_dict[sub_feat_categories_list[0]]
        else:
            traindf = []
            trainy = None
            trainmols = None 
            for feat_categories in sub_feat_categories_list:
                a, b, c = data_dict[feat_categories]
                traindf.append(a)
                trainy = b
                trainmols = c
            traindf = pd.concat(traindf, axis=1)
            
        logger.info("data shape: " + str([u.shape for u in [traindf, trainy]]))
  
        datalist = split_fold(traindf, trainy, trainmols, n_fold=n_fold, random_state=random_state)
        losses = []
        for i, (X_train, X_test, y_train, y_test)  in enumerate(datalist):
            logger.info("start folder " + str(i))
            logger.info("train-valid shape: " + str([u.shape for u in [X_train, X_test, y_train, y_test ]]))
            
            regressor = __train_a_model(X_train, y_train, loss, random_state, n_estimators, model_type, bootstrap, n_jobs)
        
            testpred = regressor.predict(X_test)
     
            testloss1 = mean_absolute_error(y_test, testpred), log_abs_error(y_test, testpred)
                 
            logger.info ('mean test  losses ' + str(testloss1))
            if (model_type != "tree"):
                testpred = medpred(regressor, X_test)
                testloss2 = mean_absolute_error(y_test, testpred), log_abs_error(y_test, testpred)
                logger.info('med  test loss ' + str(testloss2))
            if (model_type != "tree"):
                losses.append((testloss1[-1], testloss2[-1]))
            else:
                losses.append((testloss1[-1], 9999))
        
        losses = tuple(pd.DataFrame(losses).mean(0).values)
        
        return losses    
    
    feat_categories_list = [tuple(u) for u in feat_categories_list]
    
    data_dict = prepare_data(feat_categories_list)
    
    current_list = [feat_categories_list[0]]
    logger.info("training base line, with " + str(current_list))
    current_score = __train(current_list)[-1]
    logger.info("current score: " + str(current_score))
    
    def train_round(featcatlist):
        bestlist = None
        bestscore = 99999
        for feat_categories in feat_categories_list:
            if feat_categories in featcatlist:
                pass 
            else:
                newlist = featcatlist + [feat_categories]
                logger.info("in round, train with " + str([newlist, bestscore]))
                s = __train(newlist)[-1]
                logger.info("in round, end training with " + str([newlist, s]))
                if bestscore > s: 
                    bestscore = s 
                    bestlist = newlist
        logger.info("find round best :" + str([bestlist, bestscore]))                    
        return bestlist, bestscore
    
    while True:
        logger.info("trying to start with " + str(current_list))
        logger.info("current score: " + str(current_score))
        lst, s = train_round(current_list)
        if lst is not None and s < current_score:
            current_list = lst
            current_score = s
        else:
            break

    logger.info("finish with with " + str(current_list))
    logger.info("current score: " + str(current_score))
            
    return current_list, current_score 

