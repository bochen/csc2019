'''
Created on Jun 29, 2019

@author:  Lizhen Shi
'''

import sys, os
import pandas as pd
import numpy as np 
import data
import myutils

from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor, ExtraTreesRegressor
from sklearn.metrics import mean_squared_error, mean_absolute_error
import config

logger = myutils.get_logger("train")


def log_abs_error(ytrue, ypred):
    
    a = np.abs(ytrue - ypred).astype(np.float64)
    a[a < 1e-9] = 1e-9
    return np.mean(np.log(a))


def ff(regressor, X, y):
    return np.sqrt(mean_squared_error(regressor.predict(X), y)), \
     (mean_absolute_error(regressor.predict(X), y)), \
     (log_abs_error(regressor.predict(X), y))


def my_median(v):
    return np.sort(v)[int(len(v) / 2)]


def medpred(rf, X):
    lst = []
    for tree in rf.estimators_:
        a = tree.predict(X)
        lst.append(a)
    if 1:
        return np.median(np.array(lst), 0)
    else:
        return np.apply_along_axis(my_median, 0, np.array(lst))

    
def medff(rf, X, y):
    pred = medpred(rf, X)
    return np.sqrt(mean_squared_error(y, pred)), mean_absolute_error(y, pred), log_abs_error(y, pred)
    

def split_data(dfx, dfy, dfmols, test_ratio=0.33, random_state=None):
    if random_state is not None:
        np.random.seed(random_state)
    mols = list(set(dfmols.values))
    np.random.shuffle(mols)
    n = int(len(mols) * test_ratio)
    moles1 = set(mols[:n])
    trainx = dfx[~dfmols.isin(moles1)]
    testx = dfx[dfmols.isin(moles1)]
    return trainx.values, testx.values, dfy.loc[trainx.index], dfy.loc[testx.index]

def split_fold(dfx, dfy, dfmols, n_fold=5, random_state=None):
    if random_state is not None:
        np.random.seed(random_state)
    mols = list(set(dfmols.values))
    np.random.shuffle(mols)
    testmolsList = []
    for i in range(n_fold):
        testmolsList.append([])
    for i in range(len(mols)):
        testmolsList[i % n_fold].append(mols[i])
             
    testmolsList = [set(u) for u in testmolsList]
    ret = [] 
    for testmols in testmolsList:
        trainx = dfx[~dfmols.isin(testmols)]
        testx = dfx[dfmols.isin(testmols)]
        ret.append([trainx.values, testx.values, dfy.loc[trainx.index], dfy.loc[testx.index]])
    return ret


NJ_NUMBERS_TO_TYPES = {1:{"1JHC", "1JHN"}, 2:{"2JHC", "2JHH", '2JHN'}, 3:{"3JHC", "3JHH", '3JHN'}}
NJTYPES_TO_BONDPATHS = {'3JHC':{'HCCC', "HCNC", 'HCOC', 'HNCC', 'HOCC', 'HNNC', 'HONC', },
                       '2JHC':{"HCC", 'HNC', 'HOC', },
                       '1JHC':{"HC", },
                       '3JHH':{"HCCH", 'HCNH', 'HCOH', },
                       '2JHH':{"HCH", 'HNH', },
                       '3JHN':{"HCCN", 'HNCN', 'HCNN', 'HOCN', 'HCON', 'HNNN', },
                       '2JHN':{"HCN", 'HNN', 'HON', },
                       '1JHN':{"HN", },
                       }


def get_nj_info():
    return pd.read_csv(os.path.join(config.INFO_PATH, 'njinfo.csv'))


def train(njnum=None, njtype=None, bondpath=None, feat_categories=None , exclude_columns=None, model_type='tree',
          loss='mse', random_state=None, n_estimators=101, n_jobs=-1, selected_index=None, sample=None, bootstrap=False):
    traindf, trainy, trainmols, _, _, _ = data.load_data(njnum=njnum, njtype=njtype, bonpath=bondpath,
                                             feat_categories=feat_categories, exclude_columns=exclude_columns,
                                             na_fill=-9999, random_state=random_state, selected_index=selected_index, sample=sample
                                             )
    
    logger.info("data shape: " + str([u.shape for u in [traindf, trainy]]))
    
    # X_train, X_test, y_train, y_test = train_test_split(traindf.values, trainy.values, test_size=0.33, random_state=random_state)
    X_train, X_test, y_train, y_test = split_data(traindf, trainy, trainmols, test_ratio=0.33, random_state=random_state)
    logger.info("train-valid shape: " + str([u.shape for u in [X_train, X_test, y_train, y_test ]]))

    regressor = __train_a_model(X_train, y_train, loss, random_state, n_estimators, model_type, bootstrap, n_jobs)
    
    print ('train losses', ff(regressor, X_train, y_train))
    testloss = ff(regressor, X_test, y_test)
    print ('test  losses', testloss)
    if (model_type != "tree"):
        print ('med train loss', medff(regressor, X_train, y_train))
        testloss = medff(regressor, X_test, y_test)
        print ('med test  loss', testloss)
        
    return testloss[-1]


def train_kfold(njnum=None, njtype=None, bondpath=None, feat_categories=None , exclude_columns=None, model_type='tree',
          loss='mse', random_state=None, n_estimators=101, n_jobs=-1, selected_index=None, sample=None, n_fold=5, bootstrap=False):
    traindf, trainy, trainmols, _, _, _ = data.load_data(njnum=njnum, njtype=njtype, bonpath=bondpath,
                                             feat_categories=feat_categories, exclude_columns=exclude_columns,
                                             na_fill=-9999, random_state=random_state, selected_index=selected_index, sample=sample)
    
    logger.info("data shape: " + str([u.shape for u in [traindf, trainy]]))
    
    # X_train, X_test, y_train, y_test = train_test_split(traindf.values, trainy.values, test_size=0.33, random_state=random_state)
    
    datalist = split_fold(traindf, trainy, trainmols, n_fold=n_fold, random_state=random_state)
    losses = []
    testpreds = []
    for i, (X_train, X_test, y_train, y_test)  in enumerate(datalist):
        logger.info("start folder " + str(i))
        logger.info("train-valid shape: " + str([u.shape for u in [X_train, X_test, y_train, y_test ]]))
        
        regressor = __train_a_model(X_train, y_train, loss, random_state, n_estimators, model_type, bootstrap, n_jobs)
    
        testpred = regressor.predict(X_test)
 
        testloss1 = mean_absolute_error(y_test, testpred), log_abs_error(y_test, testpred)
             
        logger.info ('mean test  losses ' + str(testloss1))
        if (model_type != "tree"):
            testpred = medpred(regressor, X_test)
            testloss2 = mean_absolute_error(y_test, testpred), log_abs_error(y_test, testpred)
            logger.info('med  test loss ' + str(testloss2))
        if (model_type != "tree"):
            losses.append((testloss1[-1], testloss2[-1]))
        else:
            losses.append((testloss1[-1], 9999))
    
        testpreds.append(pd.DataFrame(testpred, index=y_test.index))
    losses = tuple(pd.DataFrame(losses).mean(0).values)
    
    testpred =  pd.concat(testpreds).sort_index()
    testpred.columns=['pred'] 
    return losses, testpred


def __train_a_model(X_train, y_train, loss, random_state, n_estimators, model_type, bootstrap, n_jobs):
    if(model_type == "tree"):
        regressor = DecisionTreeRegressor(criterion=loss, random_state=random_state, min_samples_leaf=1, min_samples_split=2)
    elif (model_type == 'rf'):
        regressor = RandomForestRegressor(criterion=loss, random_state=random_state, min_samples_leaf=1, min_samples_split=2,
                                          n_estimators=n_estimators, n_jobs=n_jobs, bootstrap=bootstrap)
    elif (model_type == 'erf'):
        regressor = ExtraTreesRegressor(criterion=loss, random_state=random_state, min_samples_leaf=1, min_samples_split=2,
                                        n_estimators=n_estimators, n_jobs=n_jobs, bootstrap=bootstrap)
    elif (model_type == 'mrf'):
        regressor = MixedForestRegressor(criterion=loss, random_state=random_state, min_samples_leaf=1, min_samples_split=2,
                                        n_estimators=n_estimators, n_jobs=n_jobs, bootstrap=bootstrap)
    else:
        raise Exception("NA")
    
    regressor.fit(X_train, y_train)
    return regressor    


class MixedForestRegressor():

    def __init__(self, criterion='mse', random_state=None, min_samples_leaf=1, min_samples_split=2,
                                        n_estimators=101, n_jobs=1, bootstrap=False):
        
        if bootstrap is None:
            n1 = int(n_estimators / 2) 
            a = RandomForestRegressor(criterion=criterion, random_state=random_state, min_samples_leaf=1, min_samples_split=2,
                                                      n_estimators=n1, n_jobs=n_jobs )
            
            n2 = n_estimators - n1
            d = ExtraTreesRegressor(criterion=criterion, random_state=random_state, min_samples_leaf=1, min_samples_split=2,
                                                n_estimators=n2, n_jobs=n_jobs )
            
            self.regressors = [a, d]
            
        else:
            n1 = int(n_estimators / 2) 
            a = RandomForestRegressor(criterion=criterion, random_state=random_state, min_samples_leaf=1, min_samples_split=2,
                                                      n_estimators=n1, n_jobs=n_jobs, bootstrap=bootstrap)
            n2 = n_estimators - n1
            b = ExtraTreesRegressor(criterion=criterion, random_state=random_state, min_samples_leaf=1, min_samples_split=2,
                                                n_estimators=n2, n_jobs=n_jobs, bootstrap=bootstrap) 
            self.regressors = [a, b]
    
    def fit(self, X, y):
        for regressor in self.regressors:
            regressor.fit(X, y)
        return self

    def predict(self, X):
        return np. mean([u.predict(X) for u in self.regressors], axis=0)
        
    @property
    def estimators_(self):
        a = []
        for u in self.regressors:
            a += list(u.estimators_)
            
        return a 
    
