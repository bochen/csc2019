import fastparquet 
import pandas as pd 
import numpy as np
import config
import myutils
from task import Task
import os
from joblib import Parallel, delayed
from multiprocessing import Pool
import sys
import data
from rdkit import Chem
from tqdm import tqdm 
import atom_feature
import bond_feature
import bondpath_feature

logger = myutils.get_logger("step10_make_atom_feature")


def make_general_feat():
        
    def f(logger):
        traintest = data.load_traintest()

        traintest['nj'] = traintest['type'].map(lambda u: int(u[0]))

        df = traintest[['nj', 'type']]
        
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
            
        outname = os.path.join(config.FEAT_PATH, "traintest_general.parq")
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY')
        
        df = traintest[['scalar_coupling_constant']]
        
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
            
        outname = os.path.join(config.FEAT_PATH, "traintest_target.parq")
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY')
        
    task = Task('make_general_feat', lambda u: f(u))
    task()


def process_one_for_make_atom_feat(ab):
    mol, sp = ab
    sp = list(sp)
    while len(sp) < 4:
        sp.append(None)
    
    feat = [ atom_feature.make_feature(mol, atom_index) for atom_index in sp]
    feat = np.concatenate(feat).astype(np.float32)
    return feat

    
def make_atom_feat():
        
    def f(logger):
        
        molsdict = data.load_mols()
        bondpaths = data.load_bond_paths()
        
        feats = []
        
        for i in tqdm(range(len(bondpaths))[:]):
            row = bondpaths.iloc[i]
            molname = row['molname']
            sp = row['shorted_path']
            if molname not in molsdict:
                logger.error("mol {} not found".format(molname))
            feat = process_one_for_make_atom_feat((molsdict[molname], sp))
            feats.append(feat)
        columns = []
        for i in range(4):
            columns += ['a{}_{}'.format(i, u) for u in atom_feature.COLUMNS]
            
        df = pd.DataFrame(feats, columns=columns)
        df.index = bondpaths["id"][:]
        df['bond_paths'] = bondpaths['shorted_path_symbols'].values[:]
        
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
            
        outname = os.path.join(config.FEAT_PATH, "traintest_atom.parq")
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY')
        
    task = Task('make_atom_feat', lambda u: f(u))
    task()


def process_one_for_make_bond_feat(ab):
    mol, sp = ab
    sp = list(sp)
    sp = list(zip(sp[:-1], sp[1:]))
    while len(sp) < 3:
        sp.append((None, None))
    
    feat = [ bond_feature.make_feature(mol, atom_index[0], atom_index[1]) for atom_index in sp]
    feat = np.concatenate(feat).astype(np.float32)
    return feat

    
def make_bond_feat():
        
    def f(logger):
        
        molsdict = data.load_mols()
        bondpaths = data.load_bond_paths()
        
        feats = []
        N = len(bondpaths)
        for i in tqdm(range(N)):
            row = bondpaths.iloc[i]
            molname = row['molname']
            sp = row['shorted_path']
            assert len(sp) == len(row['shorted_path_symbols'])
            if molname not in molsdict:
                logger.error("mol {} not found".format(molname))
            feat = process_one_for_make_bond_feat((molsdict[molname], sp))
            feats.append(feat)
        columns = []
        for i in range(3):
            columns += ['b{}_{}'.format(i, u) for u in bond_feature.COLUMNS]
            
        df = pd.DataFrame(feats, columns=columns)
        df.index = bondpaths["id"][:N]
        
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
            
        outname = os.path.join(config.FEAT_PATH, "traintest_bond.parq")
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY')
        
    task = Task('make_bond_feat', lambda u: f(u))
    task()


def process_one_for_make_bondpath_feat(ab):
    mol, sp = ab
    feat = bondpath_feature.make_feature(mol, sp)
    return np.array(feat).astype(np.float32)

    
def make_bondpath_feat():
        
    def f(logger):
        
        molsdict = data.load_mols()
        bondpaths = data.load_bond_paths()
        
        feats = []
        N = len(bondpaths)
        for i in tqdm(range(N)):
            row = bondpaths.iloc[i]
            molname = row['molname']
            sp = row['shorted_path']
            assert len(sp) == len(row['shorted_path_symbols'])
            if molname not in molsdict:
                logger.error("mol {} not found".format(molname))
            feat = process_one_for_make_bondpath_feat((molsdict[molname], sp))
            feats.append(feat)

        columns = ['bp_{}'.format(u) for u in bondpath_feature.COLUMNS]
        df = pd.DataFrame(feats, columns=columns)
        df.index = bondpaths["id"][:N]
        
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
            
        outname = os.path.join(config.FEAT_PATH, "traintest_bond.parq")
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY')
        
    task = Task('make_bondpath_feat', lambda u: f(u))
    task()


def make_mulliken_charges_feat():               

    def f(logger):
        fpath = os.path.join(config.INPUT_PATH, 'ob_charges.csv')
        df = pd.read_csv(fpath).set_index(['molecule_name', 'atom_index'])
        charges_dict = df['eem2015bn'].to_dict()
        for u in charges_dict:
            print (u, charges_dict[u])
            break
        
        bondpaths = data.load_bond_paths()
        
        feats = []
        N = len(bondpaths)
        for i in tqdm(range(N)):
            row = bondpaths.iloc[i]
            molname = row['molname']
            sp = row['shorted_path']
            assert len(sp) == len(row['shorted_path_symbols'])
            feat = [ charges_dict[(molname, u)] for u in sp]
            while len(feat) < 4: feat.append(np.nan)
            feats.append(feat)

        columns = ['a{}_mullikencharge'.format(u) for u in range(4)]
        df = pd.DataFrame(feats, columns=columns)
        df.index = bondpaths["id"][:N]
        
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
            
        outname = os.path.join(config.FEAT_PATH, "traintest_atom_mullikencharge.parq")
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY')
        
    task = Task('make_mulliken_charges_feat', lambda u: f(u))
    task()

                              
def run():
    make_general_feat()
    make_atom_feat()
    make_bond_feat()
    make_bondpath_feat()
    make_mulliken_charges_feat()


if __name__ == '__main__':
    run()

