import fastparquet 
import pandas as pd 
import numpy as np
import config
import myutils
from task import Task
import os
from joblib import Parallel, delayed
from multiprocessing import Pool
import sys
import data
from tqdm import tqdm 
import dask.dataframe as dd 
from rdkit import Chem
from rdkit.Chem import rdMolDescriptors

logger = myutils.get_logger("step13_make_others")


def make_fine_structure():               

    def f(logger):
        
        filepath = os.path.join(config.FEAT_PATH, 'traintest_general.parq')
        njdf = dd.read_parquet(filepath).compute()
        logger.info("df.shape={}\n{}".format(njdf.shape, njdf.head()))
        filepath = os.path.join(config.FEAT_PATH, 'traintest_atom.parq')
        bondpath = dd.read_parquet(filepath)[['bond_paths']].compute()
        logger.info("df.shape={}\n{}".format(bondpath.shape, bondpath.head()))
        
        # trainmols=set(pd.read_csv(os.path.join(config.INPUT_PATH, "train.csv"), usecols=['molecule_name'])['molecule_name'])
        # logger.info("there are {} training mols".format(len(trainmols)))
        
        traintest = pd.concat([pd.read_csv(os.path.join(config.INPUT_PATH, "train.csv"), index_col=0, usecols=['id', 'molecule_name'])
                               , pd.read_csv(os.path.join(config.INPUT_PATH, "test.csv"), index_col=0, usecols=['id', 'molecule_name'])])
        
        logger.info("df.shape={}\n{}".format(traintest.shape, traintest.head()))
        
        df = pd.concat([njdf, bondpath, traintest], axis=1)
        df['is_train'] = df.index < config.IDX_TEST_START
        
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
        logger.info("\n{}".format(df.isnull().sum()))
        
        df = df.dropna()
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
        
        if 1:
            outname = os.path.join(config.FEAT_PATH, "fine_structure.parq")
            logger.info("write " + outname)
            fastparquet.write(outname, df, compression='SNAPPY')            
        
    task = Task('make_fine_structure', lambda u: f(u))
    task()    


def make_molseq_smiles():

    def read_part(no, logger):
        
        outpath = os.path.join(config.INPUT_PATH, config.NNI_PATH, "traintest_submols_part_{}.pklz".format(no))
        logger.info("reading " + outpath)
        part = myutils.load_zipped_pickle(outpath) 
        return part

    def get_smiles(mol):
        # Canonical hack
        smiles = Chem.MolToSmiles(mol, isomericSmiles=True)
        m = Chem.MolFromSmiles(smiles, sanitize=False)
        smiles = Chem.MolToSmiles(m, isomericSmiles=True)
        return smiles

    def g(idx, vv):
        subarr1 = []
        subarr2 = []
        for v in vv:
            if v:
                mol = Chem.Mol(v)
                a = get_smiles(mol)
                b = rdMolDescriptors.CalcMolFormula(mol)
            else:
                a = ""
                b = ""
            subarr1.append(a) 
            subarr2.append(b)
                
        return idx, subarr1, subarr2

    def f(logger):
        arr1 = []
        arr2 = []
        indexes = []        
        for file_no in range(10):
            part = read_part(file_no, logger)
            for i, (idx, vv) in tqdm(enumerate(part.items())):
                idx, subarr1, subarr2 = g(idx, vv)
                indexes.append(idx)
                arr1.append(subarr1)
                arr2.append(subarr2)
                # if i>100: break 
        
        columns = ["smiles_" + str(i) for i in range(len(arr1[0]))]
        df1 = pd.DataFrame(arr1, index=indexes, columns=columns)
        df1.sort_index(inplace=True) 
        df1.index.name = 'id'        
        logger.info("df.shape={}\n{}".format(df1.shape, df1.head()))        
        outpath = os.path.join(config.FEAT_PATH, "traintest_submols_smiles.parq")
        logger.info("write " + outpath)        
        fastparquet.write(outpath, df1, compression='SNAPPY')

        columns = ["formula_" + str(i) for i in range(len(arr1[0]))]
        df1 = pd.DataFrame(arr2, index=indexes, columns=columns)
        df1.sort_index(inplace=True) 
        df1.index.name = 'id'
        logger.info("df.shape={}\n{}".format(df1.shape, df1.head()))        
        outpath = os.path.join(config.FEAT_PATH, "traintest_submols_formulas.parq")
        logger.info("write " + outpath)                
        fastparquet.write(outpath, df1, compression='SNAPPY')            
                
    task = Task('make_molseq_smiles', lambda u: f(u))
    task()


def make_molseq_iso_bk(no_in_seq):

    import networkx.algorithms.isomorphism as iso
    import networkx as nx 
    
    def process_one(hcode, subdf, logger):
        logger.info("processing " + str(hcode))
        subdf = subdf.to_frame()
        subdf['nx'] = subdf['mol'].map(mol_to_nx)
        subdf['isono'] = -1
        nm = iso.numerical_edge_match('atomic_num', -1)
        iso_nxs = []

        def check_iso(G, iso_nxs):
            for i, u in enumerate(iso_nxs):
                if G is None and u is None:
                    return i
                if (G is not None) and (u is not None):
                    if nx.is_isomorphic(G, u, node_match=nm):
                        return i
            return -1

        tmp = (range(len(subdf))) if len(subdf) < 1000 else tqdm(range(len(subdf)))
        for i in tmp:
            row = subdf.iloc[i]
            G1 = row['nx']
            no = check_iso(G1, iso_nxs)
            if no < 0:
                iso_nxs.append(G1)
                subdf.loc[row.name, 'isono'] = len(iso_nxs) - 1
            else:
                subdf.loc[row.name, 'isono'] = no 
        subdf['isono'] = subdf['isono'].map(lambda u: '{}:{}'.format(hcode, u))
        return subdf.drop(['mol', 'nx'], axis=1)

    def process_two(tmpdf, logger):
        hcode = tmpdf.name
        tmpdf.name = 'mol'
        return process_one(hcode, tmpdf, logger)

    def mol_to_nx(mol):
        if mol is None:
            return None
        G = nx.Graph()
    
        for atom in mol.GetAtoms():
            G.add_node(atom.GetIdx(),
                       atomic_num=atom.GetAtomicNum()
                       )
        for bond in mol.GetBonds():
            G.add_edge(bond.GetBeginAtomIdx(),
                       bond.GetEndAtomIdx())
        return G

    def f(logger):
        df = dd.read_parquet(os.path.join(config.INPUT_PATH, "traintest_submols.parq")).iloc[:, [no_in_seq]].compute()
        df['mol'] = df.iloc[:, 0].map(lambda u: Chem.Mol(u) if u else None)
        df['len'] = df['mol'].map(lambda u:u.GetNumAtoms() if u else 0)

        def g1(m):
            if m:
                return "".join(sorted([a.GetSymbol() for a in m.GetAtoms()]))
            else:
                return None

        df['symb1'] = df['mol'].map(g1)

        def g2(m):
            if m:
                return "".join(sorted([str(a.GetDegree()) for a in m.GetAtoms()]))
            else:
                return None

        df['deg1'] = df['mol'].map(g2)
        
        def g3(m):
            if m:
                lst = []
                for u in m.GetBonds():
                    b = "".join(sorted([u.GetBeginAtom().GetSymbol(), u.GetEndAtom().GetSymbol()]))
                    lst.append(b)
                lst = sorted(lst)
                return tuple(lst)
            else:
                return None

        df['bond1'] = df['mol'].map(g3)

        df['hcode'] = df[['symb1', 'deg1', 'bond1']].apply(lambda u: hash(tuple(u.values)), axis=1)

        df1 = df.groupby('hcode')['mol'].apply(lambda u: process_two(u, logger)).sort_index()

        logger.info("df.shape={}\n{}".format(df1.shape, df1.head()))        
        outpath = os.path.join(config.FEAT_PATH, "traintest_submols_moliso{}.parq".format(no_in_seq))
        logger.info("write " + outpath)                
        fastparquet.write(outpath, df1, compression='SNAPPY')            
                
    task = Task('make_molseq_iso_{}'.format(no_in_seq), lambda u: f(u))
    task()


import networkx.algorithms.isomorphism as iso
import networkx as nx 

    
def process_one_4_make_molseq_iso(hcode, subdf):

    def mol_to_nx(mol):
        if mol is None:
            return None
        G = nx.Graph()
    
        for atom in mol.GetAtoms():
            G.add_node(atom.GetIdx(),
                       atomic_num=atom.GetAtomicNum()
                       )
        for bond in mol.GetBonds():
            G.add_edge(bond.GetBeginAtomIdx(),
                       bond.GetEndAtomIdx())
        return G
        
    print("processing {}, {}".format(hcode, len(subdf)), flush=True)
    
    subdf = subdf.to_frame()
    subdf['nx'] = subdf['mol'].map(mol_to_nx)
    subdf['isono'] = -1
    nm = iso.numerical_edge_match('atomic_num', -1)
    iso_nxs = []

    def check_iso(G, iso_nxs):
        for i, u in enumerate(iso_nxs):
            if G is None and u is None:
                return i
            if (G is not None) and (u is not None):
                if nx.is_isomorphic(G, u, node_match=nm):
                    return i
        return -1

    tmp = (range(len(subdf)))  
    for i in tmp:
        row = subdf.iloc[i]
        G1 = row['nx']
        no = check_iso(G1, iso_nxs)
        if no < 0:
            iso_nxs.append(G1)
            subdf.loc[row.name, 'isono'] = len(iso_nxs) - 1
        else:
            subdf.loc[row.name, 'isono'] = no 
    subdf['isono'] = subdf['isono'].map(lambda u: '{}:{}'.format(hcode, u))
    return subdf.drop(['mol', 'nx'], axis=1)


def make_molseq_iso(no_in_seq):

    def make_input(logger):
        df = dd.read_parquet(os.path.join(config.INPUT_PATH, "traintest_submols.parq")).iloc[:, [no_in_seq]].compute()
        df['mol'] = df.iloc[:, 0].map(lambda u: Chem.Mol(u) if u else None)
        df['len'] = df['mol'].map(lambda u:u.GetNumAtoms() if u else 0)

        def g1(m):
            if m:
                return "".join(sorted([a.GetSymbol() for a in m.GetAtoms()]))
            else:
                return None

        df['symb1'] = df['mol'].map(g1)

        def g2(m):
            if m:
                return "".join(sorted([str(a.GetDegree()) for a in m.GetAtoms()]))
            else:
                return None

        df['deg1'] = df['mol'].map(g2)
        
        def g3(m):
            if m:
                lst = []
                for u in m.GetBonds():
                    b = "".join(sorted([u.GetBeginAtom().GetSymbol(), u.GetEndAtom().GetSymbol()]))
                    lst.append(b)
                lst = sorted(lst)
                return tuple(lst)
            else:
                return None

        df['bond1'] = df['mol'].map(g3)

        df['hcode'] = df[['symb1', 'deg1', 'bond1']].apply(lambda u: hash(tuple(u.values)), axis=1)
        vc = df['hcode'].value_counts()
        logger.info("{}\n{}".format(vc.shape, vc.head()))

        lst = tuple(df.groupby('hcode')['mol'])
        
        return lst
                
    def f(logger):
        lst = make_input(logger)
        results = Parallel(n_jobs=myutils.get_num_thread())(delayed(process_one_4_make_molseq_iso)(hcode, subdf) for hcode, subdf in lst)
        df1 = pd.concat(results).sort_index()  
        logger.info("df.shape={}\n{}".format(df1.shape, df1.head()))        
        outpath = os.path.join(config.FEAT_PATH, "traintest_submols_moliso{}.parq".format(no_in_seq))
        logger.info("write " + outpath)                
        fastparquet.write(outpath, df1, compression='SNAPPY')            
                
    task = Task('make_molseq_iso_{}'.format(no_in_seq), lambda u: f(u))
    task()


def make_iso_feat():               

    def f(logger):
        lst = []
        for fname in ['traintest_submols_moliso1.parq', 'traintest_submols_moliso2.parq']:
            filepath = os.path.join(config.FEAT_PATH, fname)
            df = dd.read_parquet(filepath).compute()
            a = set(df['isono'])
            da = dict({v:u for u, v in enumerate(a)})
            s = df['isono'].map(da)
            lst.append(s)
            
        df = pd.concat(lst, axis=1)
        df.columns = ['isono1', "isono2"]

        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
        outname = os.path.join(config.FEAT_PATH, 'traintest_moliso_feat.parq')
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY')            
        
    task = Task('make_iso_feat', lambda u: f(u))
    task()  

    
def make_layers():               

    def make_fine():
        a = dd.read_parquet(os.path.join(config.FEAT_PATH, "fine_structure.parq")).compute()
        b = dd.read_parquet(os.path.join(config.FEAT_PATH, "traintest_submols_moliso1.parq")).compute()
        b.columns = ['isono1']
        c = dd.read_parquet(os.path.join(config.FEAT_PATH, "traintest_submols_moliso2.parq")).compute()
        c.columns = ['isono2']
        fine = pd.concat([a, b, c], axis=1)
        fine['isono1'] = fine[['bond_paths', 'isono1']].apply(lambda u: "_".join(u.values), axis=1)
        fine['isono2'] = fine[['isono1', 'isono2']].apply(lambda u: "_".join(u.values), axis=1)
        return fine
    
    def make_layer(df, colname):
        a = set(df[colname])
        da = dict({v:u + 1 for u, v in enumerate(a)})
        s = df[colname].map(da)
        return s

    def f(logger):
        fine = make_fine()
        fine['layer1'] = fine['nj']
        fine['layer2'] = make_layer(fine, 'bond_paths')
        fine['layer3'] = make_layer(fine, 'isono1')
        fine['layer4'] = make_layer(fine, 'isono2')

        df = fine[['nj', 'type', 'bond_paths', 'molecule_name', 'is_train', 'layer1', 'layer2', 'layer3', 'layer4']]

        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
        outname = os.path.join(config.FEAT_PATH, 'traintest_layers.parq')
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY')            
        
    task = Task('make_layers', lambda u: f(u))
    task()        

                                          
def run():
    make_fine_structure()
    make_molseq_smiles()
    for i in [1, 2]:
        make_molseq_iso(i)
        
    make_iso_feat()
    
    make_layers()


if __name__ == '__main__':
    run()

