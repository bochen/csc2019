import fastparquet 
import pandas as pd 
import numpy as np
import config
import myutils
from task import Task
import os
from joblib import Parallel, delayed
from multiprocessing import Pool
import sys
import dask.dataframe as dd 

logger = myutils.get_logger("step13b_make_additonal_layer")

from step13_make_others import make_molseq_iso

    
def make_layer4():               

    def make_fine():
        a = dd.read_parquet(os.path.join(config.FEAT_PATH, "fine_structure.parq")).compute()
        b = dd.read_parquet(os.path.join(config.FEAT_PATH, "traintest_submols_moliso1.parq")).compute()
        b.columns = ['isono1']
        c = dd.read_parquet(os.path.join(config.FEAT_PATH, "traintest_submols_moliso2.parq")).compute()
        c.columns = ['isono2']
        d = dd.read_parquet(os.path.join(config.FEAT_PATH, "traintest_submols_moliso3.parq")).compute()
        d.columns = ['isono3']        
        fine = pd.concat([a, b, c, d], axis=1)
        fine['isono1'] = fine[['bond_paths', 'isono1']].apply(lambda u: "_".join(u.values), axis=1)
        fine['isono2'] = fine[['isono1', 'isono2']].apply(lambda u: "_".join(u.values), axis=1)
        fine['isono3'] = fine[['isono2', 'isono3']].apply(lambda u: "_".join(u.values), axis=1)
        return fine
    
    def make_layer(df, colname):
        a = set(df[colname])
        da = dict({v:u + 1 for u, v in enumerate(a)})
        s = df[colname].map(da)
        return s

    def f(logger):
        fine = make_fine()
        fine['layer4'] = make_layer(fine, 'isono2')

        df = fine[['layer4']]

        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
        outname = os.path.join(config.FEAT_PATH, 'traintest_layer4.parq')
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY')            
        
    task = Task('make_layer4', lambda u: f(u))
    task()        

                                          
def run():
    for i in [3]:
        make_molseq_iso(i)
        
    make_layer4()


if __name__ == '__main__':
    run()

