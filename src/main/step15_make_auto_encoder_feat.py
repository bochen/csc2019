import fastparquet 
import pandas as pd 
import numpy as np
import config
import myutils
from task import Task
import os
from tqdm import tqdm 

import keras
from keras.models import load_model
from keras import Model
from keras.layers import Lambda
import net

loss = lambda y_true, y_pred: 1000 * keras.losses.mse(y_true, y_pred)

def make_prediction(modelfile):               

    def predict(model, data_std, lst):
        values = []
        predlist = []
        for v in tqdm(lst[:]):
            values.append(myutils.uncompress_np_array(v))
            if len(values)>=64000:
                values=np.array(values) / data_std
                pred = model.predict(values, batch_size=64,  verbose=1)
                predlist.append(pred) 
                values=[] 
        if len(values)>0:
            values=np.array(values) / data_std
            pred = model.predict(values, batch_size=64,  verbose=1)
            predlist.append(pred) 
            values=[] 

        ret= np.concatenate(predlist)
        assert len(lst) == len(ret)
        return ret

    def f(logger):
        modelpath = os.path.join(config.FEAT_PATH, modelfile)
        logger.info("loading model from " + modelpath)
        model = load_model(modelpath, custom_objects={'<lambda>': loss})
        hidden_model = Model(inputs=model.inputs, outputs=model.get_layer("repr_hidden").output)
        
        datafiles = myutils.get_datafiles()
        data_std = myutils.get_feat_std()
        data_std = np.reshape(data_std, [1, 1, -1]).astype(np.float32)

        dflist = []
        for filename in datafiles[:]:
            logger.info(" reading " + filename)
            tmp = myutils.load_zipped_pickle(filename)
            values = tmp['value']
            keys = tmp['index']
            assert len(values) == len(keys)
            logger.info("# records: " + str(len(values)))
            
            arr = predict(hidden_model, data_std, values)
            dflist.append(pd.DataFrame(arr, index=keys[:]))
            
        df = pd.concat(dflist)
        df.index.name = 'id'
        df.columns = ['ae_' + str(i) for i in range(df.shape[1])]
        df=df.sort_index()
        
        logger.info("df.shape={}\n{}".format(df.shape, df.head()))
        
        outname = os.path.join(config.FEAT_PATH, "traintest_autoencoder_feat-{}.parq".format(modelfile))
        logger.info("write " + outname)
        fastparquet.write(outname, df, compression='SNAPPY')            
        
    task = Task('make_prediction-'+modelfile, lambda u: f(u))
    task()    

                                  
def run():
    files= ['model_epoch_024_loss_1.1168.hdf5']
    for modelfile in files[:]:
        make_prediction(modelfile)


if __name__ == '__main__':
    run()

