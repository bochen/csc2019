import numpy as np
import scipy.stats
import sys
import myutils
from rdkit import Chem
import molutils
from deepchem import feat as deepchem_feat

rdKitDescriptors_COLUMNS = None
rdKitDescriptors = deepchem_feat.RDKitDescriptors()


def make_RDKitDescriptors_feature(mol):
    
    global rdKitDescriptors_COLUMNS

    feat = rdKitDescriptors.featurize([mol])[0]
    
    if not rdKitDescriptors_COLUMNS:
        rdKitDescriptors_COLUMNS = ['rdkit_' + u for u in rdKitDescriptors.descriptors]
    # print(list(zip(COLUMNS, feats)))
    return feat.astype(np.float32)


dcCoulombMatrixEng_COLUMNS = ["CoulombMatrixEng_{}".format(u) for u in range(30)]
dcCoulombMatrixEng = deepchem_feat.CoulombMatrixEig(max_atoms=30)


def make_CoulombMatrixEig_feature(mol):
    feat = dcCoulombMatrixEng.featurize([mol])[0][0]    
    return feat.astype(np.float32)


MorganFingerprint_COLUMNS = ["MorganFingerprint_{}".format(u) for u in range(1024)]


def make_MorganFingerprint_feature(mol):
    feat = Chem.rdMolDescriptors.GetMorganFingerprintAsBitVect(mol, 2, nBits=1024)
    return np.asarray(feat, dtype=np.float32)


AtomPairFingerprint_COLUMNS = ["AtomPairFingerprint_{}".format(u) for u in range(1024)]


def make_AtomPairFingerprint_feature(mol):
    feat = Chem.rdMolDescriptors.GetHashedAtomPairFingerprintAsBitVect(mol, nBits=1024)
    return np.asarray(feat, dtype=np.float32)


TopologicalTorsionFingerprint_COLUMNS = ["TopologicalTorsionFingerprint_{}".format(u) for u in range(1024)]


def make_TopologicalTorsionFingerprint_feature(mol):
    feat = Chem.rdMolDescriptors.GetHashedTopologicalTorsionFingerprintAsBitVect(mol, nBits=1024)
    return np.asarray(feat, dtype=np.float32)


if __name__ == '__main__':
    from rdkit.Chem import AllChem
    mol123 = Chem.MolFromSmiles('c1nccc2n1ccc2')
    AllChem.Compute2DCoords(mol123)
    AllChem.EmbedMolecule(mol123)

    # print(Chem.mol123Tomol123Block(mol123))
    feat = make_RDKitDescriptors_feature(mol123)
    print (list(zip(rdKitDescriptors_COLUMNS, feat)))
    
    feat = make_CoulombMatrixEig_feature(mol123)
    print (list(zip(dcCoulombMatrixEng_COLUMNS, feat)))

    feat = make_MorganFingerprint_feature(mol123)
    print (list(zip(MorganFingerprint_COLUMNS, feat)))
    
    feat = make_AtomPairFingerprint_feature(mol123)
    print (list(zip(AtomPairFingerprint_COLUMNS, feat)))

    feat = make_TopologicalTorsionFingerprint_feature(mol123)
    print (list(zip(TopologicalTorsionFingerprint_COLUMNS, feat)))
    
    
