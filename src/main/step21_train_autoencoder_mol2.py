from task import Task
import myutils
import config 
from tqdm import tqdm
import os 
import numpy as np 
import pickle 
import pandas as pd 
from joblib import Parallel, delayed


def train_auto_encoder_mol2():               
    from keras import backend as K
    import keras
    import net3, config 

    def f(logger):
        use_gpu = len(K.tensorflow_backend._get_available_gpus()) > 0
        print ("use cudnn", use_gpu)
        
        datafiles = myutils.get_mol2_datafiles()
        print (datafiles)
        
        data_std = myutils.get_mol2_feat_std()
        print (data_std.shape)
        
        model_type = 0
        
        gen = net3.DataGeneratorPar(datafiles, data_size=7163633, batch_size=256, data_std=data_std, model_type=model_type)
        basepath = config.FEAT_PATH
        ckpt = keras.callbacks.ModelCheckpoint(basepath + "/model_mol2_epoch_{epoch:03d}_loss_{loss:.4f}.hdf5", monitor='loss', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)
        
        model = net3.make_model(model_type)
        
        loss = lambda y_true, y_pred: 1000 * keras.losses.mse(y_true, y_pred)
        
        model.compile(optimizer='adam', loss=loss)    
        
        model.fit_generator(generator=gen, validation_data=None, epochs=20, use_multiprocessing=True,
                             workers=15, callbacks=[ckpt], max_queue_size=128)
        
    task = Task('train_auto_encoder_mol2', lambda u: f(u))
    task()    


def make_auto_encoder_mol2_input():               

    def f(logger):
        datafiles = myutils.get_datafiles()
        print (datafiles)
        
        for filename in datafiles[:]:
            logger.info(" reading " + filename)
            tmp = myutils.load_zipped_pickle(filename)
            values = tmp['value']
            keys = tmp['index']
            assert len(values) == len(keys)
            logger.info("# records: " + str(len(values)))
            newvalues = []
            for v in tqdm(values[:]):
                v = myutils.uncompress_np_array(v)[1:3]
                newvalues.append(myutils.compress_np_array(v))

            outpath = filename.replace("_feat_", "_mol2_feat_").replace("nni", 'nni3')
            logger.info("writing " + outpath)
            myutils.save_zipped_pickle({'value':newvalues, "index":keys}, outpath)
        
    task = Task('make_auto_encoder_mol2_input', lambda u: f(u))
    task()    


def process_one_4_make_mol2_feat_mean(ino, fpath):
    
    data = myutils.load_zipped_pickle(fpath)['value']
    n = 3213
    sum_x = np.zeros(n, dtype=np.float64)
    sum_x2 = np.zeros(n, dtype=np.float64)
    max_x = []
    min_x = []
    N = 0
    for i, b in (tqdm(enumerate(data)) if ino == 0  else enumerate(data)): 
        x = myutils.uncompress_np_array(b).astype(np.float64)
        N += len(x)
        max_x.append(np.max(x, 0))
        min_x.append(np.min(x, 0))
        x = np.sum(x, 0)
        sum_x += x 
        sum_x2 += x * x
        #if i > 1000: break
         
    return sum_x, sum_x2, N, np.min(min_x, 0), np.max(max_x, 0) 
    
def make_mol2_feat_mean():


    def f(logger):
        filepaths = [ os.path.join(config.INPUT_PATH, config.NNI3_PATH, "traintest_submols_mol2_feat_part_{}.pklz".format(file_no)) for file_no in range(10)]
        n_jobs=myutils.get_num_thread()
        results = Parallel(n_jobs=n_jobs)(delayed(process_one_4_make_mol2_feat_mean)(i, vv) for i, vv in enumerate(filepaths))
        
        sum_x = [u[0] for u in results]
        sum_x2 = [u[1] for u in results]
        N = [u[2] for u in results]
        max_x = [u[3] for u in results]
        min_x = [u[4] for u in results]
        
        sum_x = np.sum(sum_x, 0)
        sum_x2 = np.sum(sum_x2, 0)
        N = np.sum(N, 0)
        
        x_max = np.max(max_x, 0) 
        x_min = np.min(min_x, 0)
                
        x_mean = sum_x / N 
        x_stdev = np.sqrt((sum_x2 / N - x_mean ** 2))
        
        #results = {'mean': x_mean, 'std': x_stdev, "n":N, 'max': x_max, 'min':x_min}
        results = {'mean': x_mean, 'std': x_stdev, 'max': x_max, 'min':x_min}
        logger.info("total #record = " + str(N))
        
        for v in results.values():
            assert len(v) == 3213
            
        df = pd.DataFrame(results.values(), index=results.keys()).T 
        with pd.option_context("display.max_rows", 100000):
            print(df)

        outname = os.path.join(config.INPUT_PATH, "traintest_mol2_feat_mean.pkl")
        with open(outname, 'wb') as fout: 
            pickle.dump(results, fout)
                
    task = Task('make_mol2_feat_mean', lambda u: f(u))
    task()

                                  
def run():
    make_auto_encoder_mol2_input()
    make_mol2_feat_mean()
    train_auto_encoder_mol2()               


if __name__ == '__main__':
    run()

