import numpy as np
import config
import myutils
from task import Task
import os
from joblib import Parallel, delayed
import data
import dask.dataframe as dd 
import longstuf
import lz4.frame
import pickle
from tqdm import tqdm 

logger = myutils.get_logger("step30_make_compressed_feat")


def process_one_4_compress_feat_bk(filename, is_dir, col):
    filepath = os.path.join(config.FEAT_PATH, filename)

    df = dd.read_parquet(filepath + "/*.parq") if is_dir else dd.read_parquet(filepath)
            
    s = df[col].compute()
    
    idx = list(s.index)
    if col in data.category_map:
        d = data.category_map[col][1]
        s = s.map(lambda u: d[u]).astype(np.float32)
        
    val = list(s.astype(np.float32))
    arr = np.zeros(2 ** 23, dtype=np.float32) + np.nan
    arr[idx] = val
    compressed = lz4.frame.compress(arr.tobytes())

    dirpath = os.path.join(config.COMPRESSED_PATH, filename)
    myutils.create_dir_if_not_exists(dirpath)
    outname = os.path.join(dirpath, col)
    logger.info("write " + outname)
    with open(outname, 'wb') as fout:
        pickle.dump(compressed, fout)            

    
def compress_feat_bk(filename, is_dir=False):               

    def f(logger):
        filepath = os.path.join(config.FEAT_PATH, filename)
        logger.info("reading {}".format(filepath))
        df = dd.read_parquet(filepath + "/*.parq") if is_dir else dd.read_parquet(filepath)
        cols = list(set(df.columns).difference(longstuf.get_useless_columns()))

        Parallel(n_jobs=myutils.get_num_thread())(delayed(process_one_4_compress_feat_bk)(filename, is_dir, col) for  col in cols)

    task = Task('compress_feat-{}'.format(filename), lambda u: f(u))
    task()    

 
def process_one_4_compress_feat2(filename, df, is_dir, col):
    dirpath = os.path.join(config.COMPRESSED_PATH, filename)
    myutils.create_dir_if_not_exists(dirpath)
    outname = os.path.join(dirpath, col)
    if os.path.exists(outname): 
        logger.info("skip "+outname)
        return
    
    s = df[col]
    
    idx = list(s.index)
    if col in data.category_map:
        d = data.category_map[col][1]
        s = s.map(lambda u: d[u]).astype(np.float32)
        
    val = list(s.astype(np.float32))
    arr = np.zeros(2 ** 23, dtype=np.float32) + np.nan
    arr[idx] = val
    compressed = lz4.frame.compress(arr.tobytes())

    logger.info("write " + outname)
    with open(outname, 'wb') as fout:
        pickle.dump(compressed, fout)            


def compress_feat(filename, is_dir=False):               

    def f(logger):
        filepath = os.path.join(config.FEAT_PATH, filename)
        logger.info("reading {}".format(filepath))
        df = dd.read_parquet(filepath + "/*.parq") if is_dir else dd.read_parquet(filepath)
        df = df.compute()
        cols = list(set(df.columns).difference(longstuf.get_useless_columns()))
        
        for col in tqdm(cols):
            process_one_4_compress_feat2(filename, df, is_dir, col)

    task = Task('compress_feat-{}'.format(filename), lambda u: f(u))
    task()    

                                          
def run():
    filenames1 = ['traintest_moliso_feat.parq', 'traintest_bond.parq', 'traintest_atom.parq'] + \
        ['traintest_atom_mullikencharge.parq', 'traintest_autoencoder_shortseq_feat-model_shortseq_epoch_020_loss_5.4097.hdf5.parq', 'traintest_autoencoder_feat-model_epoch_024_loss_1.1168.hdf5.parq'] +\
        ['traintest_autoencoder_mol2_feat_molno1-model_mol2_epoch_020_loss_8.1866.hdf5.parq', 'traintest_autoencoder_mol2_feat_molno0-model_mol2_epoch_020_loss_8.1866.hdf5.parq']
    filenames2 = ['traintest_1j_quadruples_feat.parq', 'traintest_1j_triplets_feat.parq', 'traintest_2j_quadruples_feat.parq', 'traintest_2j_triplets_feat.parq', 'traintest_3j_quadruples_feat.parq', 'traintest_3j_triplets_feat.parq']
    filenames = [(u, False) for u in filenames1] + [(u, True) for u in filenames2]

    if 1:    
        for fname in filenames:
                compress_feat(*fname)
    else:
        Parallel(n_jobs=myutils.get_num_thread())(delayed(compress_feat)(*vv) for  vv in filenames)


if __name__ == '__main__':
    run()

