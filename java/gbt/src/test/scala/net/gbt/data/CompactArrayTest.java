package net.gbt.data;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CompactArrayTest {

    @Test
    public void getValues() throws IOException, ClassNotFoundException {
        ArrayList<Integer> data = new ArrayList<>();
        for (int i=0;i<10000;i++){
            if (i%39==0){
                data.add(i);
            } else {
                data.add(213);
            }
        }

        CompactArray arr = new CompactArray(data);
        System.out.println("List length: " + data.size());
        System.out.println("uncompressed bytes length: " + arr.getDecompressedLength());
        System.out.println("compressed bytes length: " + arr.getCompressedLength());
        System.out.println("compressed bytes length2: " + arr.getCompressedBytes().length);
        System.out.println("compressed ratio: " + arr.getCompressRatio());

        List<Integer> newdata = arr.getValues();
        Assert.assertEquals(data, newdata);


    }
}