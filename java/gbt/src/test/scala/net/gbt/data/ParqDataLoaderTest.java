package net.gbt.data;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class ParqDataLoaderTest {

    @Test
    public void loadParq() throws Exception {
        Set<String> categoricalColumns = new HashSet<>();
        categoricalColumns.add("ZN");
        ParqDataLoader dataLoader = new ParqDataLoader("target", null, categoricalColumns, 5, 255);
        if (true) {
            String fpath1 = "data/boston.train.parq";
            DataFrame df = dataLoader.load(fpath1);
            df.save("/tmp/boston.train.parq.bin");
            System.out.println(df);
            System.out.println(df.getColumnNames());
            assertEquals(13, df.shape1());
            assertEquals(400, df.shape0());
            df.showBinners();
            df.showData();
        }
        if (true) {
            String fpath1 = "data/boston.test.parq";
            DataFrame df = dataLoader.load(fpath1, false);
            System.out.println(df);
            System.out.println(df.getColumnNames());
            assertEquals(13, df.shape1());
            assertEquals(106, df.shape0());
            df.showBinners();
            df.showData();

        }
    }
}