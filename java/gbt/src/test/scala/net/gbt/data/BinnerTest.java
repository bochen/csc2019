package net.gbt.data;

import org.junit.Test;
import sourcecode.Impls;

import java.util.ArrayList;
import java.util.List;

public class BinnerTest {

    @Test
    public void trainCategory() throws Exception {
        List<Object> data = new ArrayList<>();
        for (int i=0;i<10000;i++){
            data.add("S"+String.valueOf(i%5));

        }

        Binner binner = new Binner("test", true);

        binner.train(data);
        System.out.println(binner);

        CompactArray arr = binner.transform(data);

        System.out.println("nBytes: " + arr.nBytes());
        System.out.println("compressed ratio: " + arr.getCompressRatio());

        List<Integer> newdata = arr.getValues();
        for (int i=0;i<10;i++){
            System.out.println("" + data.get(i) +"\t" + newdata.get(i));
        }

    }

    @Test
    public void trainNumbers() throws Exception {
        List<Object> data = new ArrayList<>();
        for (int i=0;i<10000;i++){
            data.add(Math.round(Math.random()*10)/10.0);

        }

        Binner binner = new Binner("test", false);

        binner.train(data);
        System.out.println(binner);

        CompactArray arr = binner.transform(data);

        System.out.println("nBytes: " + arr.nBytes());
        System.out.println("compressed ratio: " + arr.getCompressRatio());

        List<Integer> newdata = arr.getValues();
        for (int i=0;i<10;i++){
            System.out.println("" + data.get(i) +"\t" + newdata.get(i));
        }

    }

    @Test
    public void trainNumbers2() throws Exception {
        List<Object> data = new ArrayList<>();
        for (int i=0;i<10000;i++){
            data.add(Math.round(Math.random()*10-5)/10.0);

        }

        Binner binner = new Binner("test", false);

        binner.train(data);
        System.out.println(binner);

        CompactArray arr = binner.transform(data);

        System.out.println("nBytes: " + arr.nBytes());
        System.out.println("compressed ratio: " + arr.getCompressRatio());

        List<Integer> newdata = arr.getValues();
        for (int i=0;i<10;i++){
            System.out.println("" + data.get(i) +"\t" + newdata.get(i));
        }

    }
}