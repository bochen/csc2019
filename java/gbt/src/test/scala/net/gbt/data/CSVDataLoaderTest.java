package net.gbt.data;

import org.junit.Test;

import static org.junit.Assert.*;

public class CSVDataLoaderTest {

    @Test
    public void loadCSV() throws Exception {
        CSVDataLoader dataLoader = new CSVDataLoader("target", null, null, 5, 255);
        if (true) {
            String fpath1 = "data/boston.train.csv";
            DataFrame df = dataLoader.load(fpath1);
            System.out.println(df);
            System.out.println(df.getColumnNames());
            assertEquals(13, df.shape1());
            assertEquals(400, df.shape0());
            df.showBinners();
            df.showData();
        }
        if (true) {
            String fpath1 = "data/boston.test.csv";
            DataFrame df = dataLoader.load(fpath1, false);
            System.out.println(df);
            System.out.println(df.getColumnNames());
            assertEquals(13, df.shape1());
            assertEquals(106, df.shape0());
            df.showBinners();
            df.showData();

        }
    }
}