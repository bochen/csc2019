package net.gbt.data;

import org.junit.Test;

import static org.junit.Assert.*;

public class IndexedDataTest {
    @Test
    public void testCopy(){
        IndexedData data = new IndexedData();
        data.add(2,111);
        data.add(123,2223);
        data.add(432,934);
        IndexedData data2 = data.copy();
        data2.remove(432);
        assertEquals(3, data.size());
        assertEquals(2, data2.size());
    }

}