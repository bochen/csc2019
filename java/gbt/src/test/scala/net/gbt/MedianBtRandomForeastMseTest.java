package net.gbt;

import net.gbt.data.CSVDataLoader;
import net.gbt.data.DataFrame;
import org.junit.Before;
import org.junit.Test;

import java.io.PrintStream;
import java.util.List;

public class MedianBtRandomForeastMseTest {
    private DataFrame traindf;
    private DataFrame testdf;
    private DataFrame traindf2;
    private DataFrame testdf2;
    private PrintStream out;

    @Before
    public void setup() throws Exception {
        if (true) {
            CSVDataLoader dataLoader = new CSVDataLoader("target", null, null, 5, 32);
            String fpath1 = "data/boston.train.csv";
            traindf = dataLoader.load(fpath1);
            fpath1 = "data/boston.test.csv";
            testdf = dataLoader.load(fpath1, false);
            out = System.out;
        }
        if (true) {
            CSVDataLoader dataLoader = new CSVDataLoader("target", null, null, 5, 32);
            String fpath1 = "data/diabetes.train.csv";
            traindf2 = dataLoader.load(fpath1);
            fpath1 = "data/diabetes.test.csv";
            testdf2 = dataLoader.load(fpath1, false);
            out = System.out;
        }
    }


    @Test
    public void train_diabetes_parallel() throws Exception {
        MulticoreExecutor executor = new MulticoreExecutor(6);
        try {
            MedianRandomForeast tree = new MedianRandomForeast("MSE", 0)
                    .setColSample(1).setBootstrap(true).setMultiCoreExecutor(executor);
            tree.train(traindf2, testdf2, 100, 0.1,
                    100, 1);
            evaluate(tree, traindf2,testdf2);
        } finally {
            executor.shutdown();
        }

    }

    @Test
    public void train2_parallel() throws Exception {
        MulticoreExecutor executor = new MulticoreExecutor(6);
        try {
            MedianRandomForeast tree = new MedianRandomForeast("MSE", 0)
                    .setColSample(1).setBootstrap(true).setMultiCoreExecutor(executor);
            tree.train(traindf, testdf, 100, 0.1,
                    100, 1);
            evaluate(tree, traindf,testdf);
        } finally {
            executor.shutdown();
        }

    }


    private void evaluate(MedianRandomForeast tree, DataFrame traindf, DataFrame testdf) throws Exception {
        List<Double> y = traindf.getTarget();
        System.out.println("Train ymean=" + ArrayUtils.mean(y));
        y = testdf.getTarget();
        System.out.println("Test ymean=" + ArrayUtils.mean(y));
        System.out.println("Train RMES Loss=" + evaluate(tree, traindf));
        System.out.println("Test RMES Loss=" + evaluate(tree, testdf));
        System.out.println("Train MAE Loss=" + evaluateMAE(tree, traindf));
        System.out.println("Test MAE Loss=" + evaluateMAE(tree, testdf));
        System.out.println("Train LogAbs Loss=" + evaluateLogAbs(tree, traindf));
        System.out.println("Test LogAbs Loss=" + evaluateLogAbs(tree, testdf));
    }

    private double evaluateMAE(MedianRandomForeast tree, DataFrame df) throws Exception {
        List<Double> pred = tree.predict(df);
        return MAELoss.calMAE(df.getTarget(), pred);
    }



    private double evaluate(MedianRandomForeast tree, DataFrame df) throws Exception {
        List<Double> pred = tree.predict(df);
        return MSELoss.calRMSE(df.getTarget(), pred);

    }

    private double evaluateLogAbs(MedianRandomForeast tree, DataFrame df) throws Exception {
        List<Double> pred = tree.predict(df);
        return LogAbsLoss.calLogAbsError(df.getTarget(), pred, df.getWeights());

    }
}