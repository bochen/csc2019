package net.gbt;

import net.gbt.data.CSVDataLoader;
import net.gbt.data.DataFrame;
import org.junit.Before;
import org.junit.Test;

import java.io.PrintStream;
import java.util.List;

import static org.junit.Assert.*;

public class RegressionTreeMSETest {
    private DataFrame traindf;
    private DataFrame testdf;
    private PrintStream out;

    @Before
    public void setup() throws Exception {
        CSVDataLoader dataLoader = new CSVDataLoader("target", null, null, 5, 32);
        String fpath1 = "data/boston.train.csv";
        traindf = dataLoader.load(fpath1);
        fpath1 = "data/boston.test.csv";
        testdf = dataLoader.load(fpath1, false);
        out = System.out;
    }

    @Test
    public void train1() throws Exception {
        RegressionTree tree = new RegressionTree().setMinDataInLeaf(1).setMaxDepth(20);
        tree.train(traindf);
        out.println(tree);
        out.println("#of nodes: " + tree.getNumberNode());
        out.println("#of leave: " + tree.getNumberLeave());
        out.println("#level: " + tree.getNumLevel());
        //tree.showNodes();
        assertEquals(tree.getNumberLeave(), tree.getNumberValidFlag());
        assertEquals(tree.getNumberNode(), tree.getNumberSplitInfo());
        tree.showLeaveNodeValue();
        tree.clearValidFlag();
        assertEquals(0, tree.getNumberValidFlag());
        List<Double> a = tree.predict(testdf);
        List<Double> b = tree.predict(testdf);
        ArrayUtils.subtract(a,b).forEach(o-> assertEquals(o,0.0,1e-20));
        evaluate(tree, traindf, testdf);

    }

    @Test
    public void train2() throws Exception {
        RegressionTree tree = new RegressionTree().setMaxDepth(10).setMinDataInLeaf(5);
        tree.train(traindf);
        out.println(tree);
        out.println("#of nodes: " + tree.getNumberNode());
        out.println("#of leave: " + tree.getNumberLeave());
        out.println("#level: " + tree.getNumLevel());
        //tree.showNodes();
        assertEquals(tree.getNumberLeave(), tree.getNumberValidFlag());
        assertEquals(tree.getNumberNode(), tree.getNumberSplitInfo());

        tree.clearValidFlag();
        assertEquals(0, tree.getNumberValidFlag());

        evaluate(tree, traindf, testdf);

    }

    @Test
    public void train2_parallel() throws Exception {
        MulticoreExecutor executor = new MulticoreExecutor(6);

        try {
            RegressionTree tree = new RegressionTree().setMaxDepth(10).setMinDataInLeaf(5)
                    .setMultiCoreExecutor(executor);
            tree.train(traindf);
            out.println(tree);
            out.println("#of nodes: " + tree.getNumberNode());
            out.println("#of leave: " + tree.getNumberLeave());
            out.println("#level: " + tree.getNumLevel());
            out.println("#thread: " + tree.getNumThread());
            //tree.showNodes();
            assertEquals(tree.getNumberLeave(), tree.getNumberValidFlag());
            assertEquals(tree.getNumberNode(), tree.getNumberSplitInfo());

            tree.clearValidFlag();
            assertEquals(0, tree.getNumberValidFlag());

            evaluate(tree, traindf, testdf);
        } finally {
            executor.shutdown();
        }

    }

    private void evaluate(RegressionTree tree, DataFrame traindf, DataFrame testdf) throws Exception {
        List<Double> y = traindf.getTarget();
        System.out.println("Train ymean=" + ArrayUtils.mean(y));
        y = testdf.getTarget();
        System.out.println("Test ymean=" + ArrayUtils.mean(y));
        System.out.println("Train RMES Loss=" + evaluate(tree, traindf));
        System.out.println("Test RMES Loss=" + evaluate(tree, testdf));
        System.out.println("Train MAE Loss=" + evaluateMAE(tree, traindf));
        System.out.println("Test MAE Loss=" + evaluateMAE(tree, testdf));
        System.out.println("Train LogAbs Loss=" + evaluateLogAbs(tree, traindf));
        System.out.println("Test LogAbs Loss=" + evaluateLogAbs(tree, testdf));
    }

    private double evaluateMAE(RegressionTree tree, DataFrame df) throws Exception {
        List<Double> pred = tree.predict(df);
        return MAELoss.calMAE(df.getTarget(), pred);
    }


    private double evaluate(RegressionTree tree, DataFrame df) throws Exception {
        List<Double> pred = tree.predict(df);
        return MSELoss.calRMSE(df.getTarget(), pred);

    }

    private double evaluateLogAbs(RegressionTree tree, DataFrame df) throws Exception {
        List<Double> pred = tree.predict(df);
        return LogAbsLoss.calLogAbsError(df.getTarget(), pred, df.getWeights());

    }
}