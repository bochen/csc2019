package net.gbt;

import org.javatuples.Pair;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class LogMAELossTest {

    @Test
    public void nodeEvaluate_unweighted() throws Exception {
        if (true) {
            List<Double> list = Arrays.asList(-0.1, 0.3, 0.5, 0.7);
            Pair ret = new LogAbsLoss().nodeEvaluateLogAbs(list, 0.0);
            System.out.println(ret);

        }
        if (true) {
            List<Double> list = Arrays.asList(0.3, 0.5, 0.7);
            Pair ret = new LogAbsLoss().nodeEvaluateLogAbs(list, 0.0);
            System.out.println(ret);
        }

        if (true) {
            List<Double> list = Arrays.asList(-0.3, -0.5, -0.7);
            Pair ret = new LogAbsLoss().nodeEvaluateLogAbs(list, 0.0);
            System.out.println(ret);
        }

        if (true) {
            List<Double> list = Arrays.asList(-0.1, -1e-10, 1e-10, 0.1, 0.3, 0.5, 0.7);
            Pair ret = new LogAbsLoss().nodeEvaluateLogAbs(list, 0.0);
            System.out.println(ret);
        }
    }

    @Test
    public void nodeEvaluate_weighted() {
        if (true) {
            List<Double> list = Arrays.asList(-0.1, -0.05, 0.7);
            List<Double> weights = Arrays.asList(10.0, 1.0, 1.0);
            Pair ret = new LogAbsLoss().nodeEvaluateLogAbs(list, weights, 0.0);
            System.out.println(ret);
        }

        if (true) {
            List<Double> list = Arrays.asList(-0.1, 0.05, 0.7, 0.2);
            List<Double> weights = Arrays.asList(1.0, 1.0, 10.0, 1.0);
            Pair ret = new LogAbsLoss().nodeEvaluateLogAbs(list, weights, 0.0);
            System.out.println(ret);
        }

        if (true) {
            List<Double> list = Arrays.asList(1.0, 2.0, 3.0);
            List<Double> weights = Arrays.asList(1.0, 2.0, 1.0);
            Pair ret = new LogAbsLoss().nodeEvaluateLogAbs(list, weights, 0.0);
            System.out.println(ret);
        }
        if (true) {
            List<Double> list = Arrays.asList(1.0, 2.0, 3.0);
            List<Double> weights = Arrays.asList(2.0, 2.0, 1.0);
            Pair ret = new LogAbsLoss().nodeEvaluateLogAbs(list, weights, 0.0);
            System.out.println(ret);
        }

        if (true) {
            List<Double> list = Arrays.asList(1.0, 2.0, 3.0);
            List<Double> weights = Arrays.asList(2.0, 1.0, 2.0);
            Pair ret = new LogAbsLoss().nodeEvaluateLogAbs(list, weights, 0.0);
            System.out.println(ret);
        }

        if (true) {
            List<Double> list = Arrays.asList(-2., 1.0, 2.0, 3.0, 4.0);
            List<Double> weights = Arrays.asList(2.0, 2.0, 1.0, 1.0, 2.0);
            Pair ret = new LogAbsLoss().nodeEvaluateLogAbs(list, weights, 0.0);
            System.out.println(ret);
        }

        if (true) {//1 1 2 3 3 4
            List<Double> list = Arrays.asList(1.0, 2.0, 3.0, 4.0);
            List<Double> weights = Arrays.asList(2.0, 1.0, 2.0, 10.0);
            Pair ret = new LogAbsLoss().nodeEvaluateLogAbs(list, weights, 0.0);
            System.out.println(ret);
        }
    }

    @Test
    public void makeCandidatePoints_unweighted() {
        if (true) {
            List<Double> list = Arrays.asList(-0.1, 0.3, 0.5, 0.7);
            List<Double> points = LogAbsLoss.makeUnweightedCandidatePoints(list);
            System.out.println(points);
            assertEquals(3, points.size());
            assertTrue(listContains(points, 0.3));
            assertTrue(listContains(points, -0.1));
        }
        if (true) {
            List<Double> list = Arrays.asList(0.3, 0.5, 0.7);
            List<Double> points = LogAbsLoss.makeUnweightedCandidatePoints(list);
            System.out.println(points);
            assertEquals(3, points.size());
            assertTrue(listContains(points, 0.5));
            assertTrue(listContains(points, -1e-9));
        }

        if (true) {
            List<Double> list = Arrays.asList(-0.3, -0.5, -0.7);
            List<Double> points = LogAbsLoss.makeUnweightedCandidatePoints(list);
            System.out.println(points);
            assertEquals(3, points.size());
            assertTrue(listContains(points, 1e-9));
            assertTrue(listContains(points, -0.5));
            assertTrue(listContains(points, -0.3));
        }

        if (true) {
            List<Double> list = Arrays.asList(-0.1, -1e-10, 1e-10, 0.1, 0.3, 0.5, 0.7);
            List<Double> points = LogAbsLoss.makeUnweightedCandidatePoints(list);
            System.out.println(points);
            assertEquals(3, points.size());
            assertTrue(listContains(points, -1e-9));
            assertTrue(listContains(points, 1e-9));
            assertTrue(listContains(points, 0.1));
        }
    }

    @Test
    public void makeCandidatePoints_weighted() {
        if (true) {
            List<Double> list = Arrays.asList(-0.1, -0.05, 0.7);
            List<Double> weights = Arrays.asList(10.0, 1.0, 1.0);
            List<Double> points = LogAbsLoss.makeWeightedCandidatePoints(list, weights);
            System.out.println(points);
            assertEquals(3, points.size());
            assertTrue(listContains(points, 0.7));
            assertTrue(listContains(points, -0.05));
            assertTrue(listContains(points, -0.1));
        }

        if (true) {
            List<Double> list = Arrays.asList(-0.1, 0.05, 0.7, 0.2);
            List<Double> weights = Arrays.asList(1.0, 1.0, 10.0, 1.0);
            List<Double> points = LogAbsLoss.makeWeightedCandidatePoints(list, weights);
            System.out.println(points);
            assertEquals(3, points.size());
            assertTrue(listContains(points, 0.7));
            assertTrue(listContains(points, 0.05));
            assertTrue(listContains(points, -0.1));
        }

        if (true) {
            List<Double> list = Arrays.asList(1.0, 2.0, 3.0);
            List<Double> weights = Arrays.asList(1.0, 2.0, 1.0);
            List<Double> points = LogAbsLoss.makeWeightedCandidatePoints(list, weights);
            System.out.println(points);
            assertEquals(3, points.size());
            assertTrue(listContains(points, 2.0));
        }
        if (true) {
            List<Double> list = Arrays.asList(1.0, 2.0, 3.0);
            List<Double> weights = Arrays.asList(2.0, 2.0, 1.0);
            List<Double> points = LogAbsLoss.makeWeightedCandidatePoints(list, weights);
            System.out.println(points);
            assertEquals(3, points.size());
            assertTrue(listContains(points, 2.0));
        }

        if (true) {
            List<Double> list = Arrays.asList(1.0, 2.0, 3.0);
            List<Double> weights = Arrays.asList(2.0, 1.0, 2.0);
            List<Double> points = LogAbsLoss.makeWeightedCandidatePoints(list, weights);
            System.out.println(points);
            assertEquals(3, points.size());
            assertTrue(listContains(points, 2.0));
        }

        if (true) {
            List<Double> list = Arrays.asList(1.0, 2.0, 3.0, 4.0);
            List<Double> weights = Arrays.asList(2.0, 1.0, 1.0, 2.0);
            List<Double> points = LogAbsLoss.makeWeightedCandidatePoints(list, weights);
            System.out.println(points);
            assertEquals(3, points.size());
            assertTrue(listContains(points, 3) || listContains(points, 2));
        }

        if (true) {//1 1 2 3 3 4
            List<Double> list = Arrays.asList(1.0, 2.0, 3.0, 4.0);
            List<Double> weights = Arrays.asList(2.0, 1.0, 2.0, 1.0);
            List<Double> points = LogAbsLoss.makeWeightedCandidatePoints(list, weights);
            System.out.println(points);
            assertEquals(3, points.size());
            assertTrue(listContains(points, 3) || listContains(points, 2));
        }
    }


    private boolean listContains(List<Double> list, double v) {
        for (double x : list) {
            if (Math.abs(x - v) < 1e-10) {
                return true;
            }
        }
        return false;
    }
}