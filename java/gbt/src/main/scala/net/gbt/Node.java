package net.gbt;

import net.gbt.data.Column;
import net.gbt.data.DataFrame;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Regression tree node.
 */
class Node implements Serializable {

    /**
     * Predicted real value for this node.
     */
    double output = 0.0;
    /**
     * The split feature results to  this node.
     */
    String splitFeature = null;
    /**
     * The split value.
     */
    Integer splitValue = Integer.MAX_VALUE;
    /**
     * The split direction. default values always to this node (aka root)
     */
    boolean isTrueSplit = true;

    /**
     * Split criterion. #the high the better.
     */
    double splitScore = -Integer.MAX_VALUE;
    /**
     * Children node.
     */
    Node trueChild;
    /**
     * Children node.
     */
    Node falseChild;

    /**
     * parent node
     */
    Node parent;

    /**
     * an unique id
     */
    private String name;

    /**
     * Constructor.
     */
    public Node(double output) {
        this.output = output;
        name = UUID.randomUUID().toString();
    }

    public void setParent(Node n) {
        parent = n;
    }

    public boolean isRoot() {
        return parent == null;
    }

    public int getLevel() {
        if (isRoot()) {
            return 1;

        } else {
            return 1 + parent.getLevel();
        }
    }

    public List<Double> predict(DataFrame df) throws Exception {
        assert this.isRoot();
        ArrayList<Boolean> rowFlags = ArrayUtils.newBooleanList(df.size(), true);
        return predict(df, rowFlags);
    }

    /**
     * rowFlags from parent, it has to been updated for this node to receive the output.
     */
    public List<Double> predict(DataFrame df, List<Boolean> rowFlags) throws Exception {
        assert (rowFlags != null && rowFlags.size() == df.size());

        if (!isRoot()) {
            Column col = df.getColumn(splitFeature);
            List<Integer> featureValues = col.getValuesAsList();
            List<Boolean> thisFlags = calcValidFlags(featureValues, col.isCategory());
            rowFlags = ArrayUtils.and(rowFlags, thisFlags);
        }

        if (isLeave()) {
            List<Double> ret = ArrayUtils.newDoubleList(df.size(), output);
            return ArrayUtils.multiplyBool(ret, rowFlags);
        } else {
            List<Double> a = trueChild.predict(df, rowFlags);
            List<Double> b = falseChild.predict(df, rowFlags);
            return ArrayUtils.add(a, b);
        }
    }

    public int getNumLeaves() {
        if (isLeave()) {
            return 1;
        } else {
            return trueChild.getNumLeaves() + falseChild.getNumLeaves();
        }

    }

    public boolean isLeave() {
        return trueChild == null;
    }

    public String getName() {
        return name;
    }

    public void setSplitFeature(String splitColName) {
        this.splitFeature = splitColName;
    }

    public void setSplitValue(int splitValue) {
        this.splitValue = splitValue;
    }

    public void setChildren(Node trueNode, Node falseNode) {
        assert trueNode != null && falseNode != null;
        this.trueChild = trueNode;
        this.falseChild = falseNode;
    }

    public void setIsTrueSplit(boolean b) {
        isTrueSplit = b;
    }

    public Node getTrueChild() {
        return trueChild;
    }

    public Node getFalseChild() {
        return falseChild;
    }

    public Node getParent() {
        return parent;
    }

    public String getSplitFeature() {
        return splitFeature;
    }

    public List<Boolean> calcValidFlags(List<Integer> featureValues, boolean isCategorical) {
        List<Boolean> thisFlags;
        if (isCategorical) {
            thisFlags = ArrayUtils.equals(featureValues, this.splitValue);

        } else {
            thisFlags = ArrayUtils.lessThanOrEquals(featureValues, this.splitValue);
        }
        if (!this.isTrueSplit) {
            thisFlags = ArrayUtils.not(thisFlags);
        }
        return thisFlags;

    }

    @Override
    public String toString() {
        return "Node{" +
                "output=" + output +
                ", splitFeature='" + splitFeature + '\'' +
                ", splitValue=" + splitValue +
                ", isTrueSplit=" + isTrueSplit +
                ", splitScore=" + splitScore +
                ", trueChild=" + (trueChild == null ? null : trueChild.getName()) +
                ", falseChild=" + (falseChild == null ? null : falseChild.getName()) +
                ", parent=" + (parent == null ? null : parent.getName()) +
                ", name='" + name + '\'' +
                '}';
    }

    public double getOutput() {
        return output;
    }

    public int getSplitValue() {
        return splitValue;
    }
}
