package net.gbt;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class ArrayUtils {

    protected static int[] reindex(int[] v, Integer[] idx) {
        int[] v2 = new int[v.length];
        for (int i = 0; i < v.length; i++) {
            v2[i] = v[idx[i]];
        }
        return v2;
    }

    protected static float[] reindex(float[] v, Integer[] idx) {
        float[] v2 = new float[v.length];
        for (int i = 0; i < v.length; i++) {
            v2[i] = v[idx[i]];
        }
        return v2;
    }

    public static Tuple<Integer[], int[]> argsort(final int[] a) {
        return argsort(a, true);
    }

    public static Tuple<Integer[], int[]> argsort(final int[] a, final boolean ascending) {
        Integer[] indexes = new Integer[a.length];
        int[] sorted_values = new int[a.length];
        for (int i = 0; i < indexes.length; i++) {
            indexes[i] = i;
        }

        Arrays.sort(indexes, new Comparator<Integer>() {
            @Override
            public int compare(final Integer i1, final Integer i2) {
                return (ascending ? 1 : -1) * Integer.compare(a[i1], a[i2]);
            }
        });

        for (int i = 0; i < indexes.length; i++) {
            sorted_values[i] = a[indexes[i]];
        }
        return new Tuple(indexes, sorted_values);
        //return new Tuple(Arrays.stream(indexes).mapToInt(Integer::intValue).toArray(), sorted_values);
    }

    public static String[] toArray(List<String> list) {
        return list.toArray(new String[0]);
    }

    public static String[] toArray(Set<String> set) {
        return set.toArray(new String[0]);
    }

    public static List<Double> newDoubleList(int size, double fill) {
        return new ArrayList<Double>(Collections.nCopies(size, fill));
    }

    public static List<Double> multiplyBool(List<Double> list, List<Boolean> validFlags) {
        assert (list.size() == validFlags.size());
        List<Double> ret = new ArrayList<>(list.size());
        for (int i = 0; i < list.size(); i++) {
            if (validFlags.get(i)) {
                ret.add(list.get(i));
            } else {
                ret.add(0.0);
            }
        }
        return ret;
    }

    public static List<Double> multiply(List<Double> a, List<Double> b) {
        assert (a.size() == b.size());
        List<Double> ret = new ArrayList<>(a.size());
        for (int i = 0; i < a.size(); i++) {
            ret.add(a.get(i) * b.get(i));
        }
        return ret;
    }

    public static List<Double> multiply(List<Double> a, double b) {
        List<Double> ret = new ArrayList<>(a.size());
        for (int i = 0; i < a.size(); i++) {
            ret.add(a.get(i) * b);
        }
        return ret;
    }

    public static ArrayList<Boolean> newBooleanList(int size, boolean b) {
        return new ArrayList<Boolean>(Collections.nCopies(size, b));
    }

    public static List<Boolean> and(List<Boolean> a, List<Boolean> b) {
        assert (a.size() == b.size());
        List<Boolean> ret = new ArrayList<>(a.size());
        for (int i = 0; i < a.size(); i++) {
            ret.add(a.get(i) && b.get(i));

        }
        return ret;
    }

    public static List<Double> add(List<Double> a, double b) {
        List<Double> ret = new ArrayList<>(a.size());
        for (int i = 0; i < a.size(); i++) {
            ret.add(a.get(i) + b);

        }
        return ret;
    }

    public static List<Double> add(List<Double> a, List<Double> b) {
        assert (a.size() == b.size());
        List<Double> ret = new ArrayList<>(a.size());
        for (int i = 0; i < a.size(); i++) {
            ret.add(a.get(i) + b.get(i));

        }
        return ret;
    }

    public static double mean(List<Double> list) {
        return sum(list) / list.size();
    }

    public static double sum(List<Double> list) {
        double sum = 0.0;
        for (Double d : list) {
            sum += d;
        }
        return sum;
    }

    public static List<Double> minus(List<Double> a, List<Double> b) {
        assert (a.size() == b.size());
        List<Double> ret = new ArrayList<>(a.size());
        for (int i = 0; i < a.size(); i++) {
            ret.add(a.get(i) - b.get(i));

        }
        return ret;
    }

    public static List<Double> minus(List<Double> a, Double b) {
        List<Double> ret = new ArrayList<>(a.size());
        for (int i = 0; i < a.size(); i++) {
            ret.add(a.get(i) - b);

        }
        return ret;
    }

    public static List<Integer> filterInteger(List<Integer> list, List<Boolean> flags) {
        assert list.size() == flags.size();
        List<Integer> ret = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (flags.get(i)) {
                ret.add(list.get(i));
            }
        }
        return ret;
    }

    public static List<Double> filterDouble(List<Double> list, List<Boolean> flags) {
        assert list.size() == flags.size();
        List<Double> ret = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (flags.get(i)) {
                ret.add(list.get(i));
            }
        }
        return ret;
    }

    public static List<Boolean> equals(List<Integer> x, int a) {
        return x.stream().map(o -> o == a).collect(Collectors.toList());
    }

    public static List<Boolean> not(List<Boolean> x) {
        return x.stream().map(o -> !o).collect(Collectors.toList());
    }

    public static List<Boolean> lessThanOrEquals(List<Integer> x, int a) {
        return x.stream().map(o -> o <= a).collect(Collectors.toList());
    }

    public static int countBool(List<Boolean> x) {
        int s = 0;
        for (Boolean b : x) {
            if (b) s++;
        }
        return s;
    }

    public static List<Double> subtract(List<Double> a, List<Double> b) {
        b = ArrayUtils.negative(b);
        return add(a, b);
    }

    private static List<Double> negative(List<Double> b) {
        return b.stream().map(o -> -o).collect(Collectors.toList());
    }


    private static Double median2Single(List<LogAbsLoss.ValWeight> data) {
        Collections.sort(data, Comparator.comparing(LogAbsLoss.ValWeight::getValue));
        double sumWeight = data.stream().map(o -> o.weight).reduce((o1, o2) -> o1 + o2).get();
        double cumWeight = 0;
        double median = 0;
        boolean median_found = false;
        for (int i = 0; i < data.size(); i++) {
            LogAbsLoss.ValWeight vw = data.get(i);
            if (!median_found) {
                cumWeight += vw.weight;
                if (i == 0) {
                    median = vw.value;
                } else if (cumWeight > sumWeight / 2.0) {
                    LogAbsLoss.ValWeight vw0 = data.get(i - 1);
                    double cumWeight0 = cumWeight - vw.weight - vw0.weight / 2.0;
                    double cumWeight1 = cumWeight - vw.weight / 2.0;
                    if (sumWeight / 2.0 - cumWeight0 < cumWeight1 - sumWeight / 2.0) {
                        median = vw0.value;
                    } else {
                        median = vw.value;
                    }
                    median_found = true;
                }
            } else {
                break;
            }
        }
        return median;
    }

    public static List<Double> median2(List<List<Double>> data) {
        return median2(data, newDoubleList(data.get(0).size(), 1.0));
    }

    public static List<Double> median2(List<List<Double>> data, MulticoreExecutor executor) throws Exception {
        return median2(data, newDoubleList(data.get(0).size(), 1.0), executor);
    }

    private static List<Double> median2(List<List<Double>> data, List<Double> weights, MulticoreExecutor executor) throws Exception {
        int n = data.size();
        int m = data.get(0).size();
        assert m == weights.size();

        List<MedianTask> tasks = new ArrayList<>(m);

        for (int i = 0; i < m; i++) {
            List<LogAbsLoss.ValWeight> tmp = new ArrayList<>(n);
            for (List<Double> lst : data) {
                tmp.add(new LogAbsLoss.ValWeight(lst.get(i), weights.get(i)));
            }
            tasks.add(new MedianTask(tmp));
        }

        List<Double> a = executor.run(tasks);
        return a;
    }

    public static List<Double> median2(List<List<Double>> data, List<Double> weights) {

        int n = data.size();
        int m = data.get(0).size();
        assert m == weights.size();
        List<Double> ret = new ArrayList<>(m);
        for (int i = 0; i < m; i++) {
            List<LogAbsLoss.ValWeight> tmp = new ArrayList<>(n);
            for (List<Double> lst : data) {
                tmp.add(new LogAbsLoss.ValWeight(lst.get(i), weights.get(i)));
            }
            ret.add(median2Single(tmp));
        }
        return ret;
    }

    public static List<Integer> unique(List<Integer> x) {
        return x.stream().distinct().collect(Collectors.toList());
    }


    public static class Tuple<X, Y> {
        public final X x;
        public final Y y;

        public Tuple(X x, Y y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Tuple<?, ?> tuple = (Tuple<?, ?>) o;
            return Objects.equals(x, tuple.x) &&
                    Objects.equals(y, tuple.y);
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }
    }

    protected static class Triplet<X, Y, Z> {
        protected final X x;
        protected final Y y;
        protected final Z z;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Triplet<?, ?, ?> triplet = (Triplet<?, ?, ?>) o;
            return Objects.equals(x, triplet.x) &&
                    Objects.equals(y, triplet.y) &&
                    Objects.equals(z, triplet.z);
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y, z);
        }

        protected Triplet(X x, Y y, Z z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

    // Implementing Fisher–Yates shuffle
    public static void shuffleArray(int[] ar) {
        // If running on Java 6 or older, use `new Random()` on RHS here
        Random rnd = ThreadLocalRandom.current();
        for (int i = ar.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            int a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }

    // Implementing Fisher–Yates shuffle
    public static void shuffleArray(String[] ar) {
        // If running on Java 6 or older, use `new Random()` on RHS here
        Random rnd = ThreadLocalRandom.current();
        for (int i = ar.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            String a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }


    static class MedianTask implements Callable<Double> {


        private final List<LogAbsLoss.ValWeight> x;

        MedianTask(List<LogAbsLoss.ValWeight> x) {
            this.x = x;
        }

        @Override
        public Double call() {
            return ArrayUtils.median2Single(x);
        }
    }
}


