package net.gbt;

import net.gbt.data.DataFrame;
import org.javatuples.Pair;

import java.util.List;

public abstract class Loss {
    /**
     * @param y
     * @param reg_lambda
     * @return <criterion, node output>
     */
    public abstract Pair nodeEvaluateMSE(List<Double> y, double reg_lambda) throws Exception;

    public abstract Pair nodeEvaluateLogAbs(List<Double> y, List<Double> weights, double defaultPoint) throws Exception;

    public Pair nodeEvaluateMAE(List<Double> y) throws Exception {
        throw new Exception("NA");
    }

    public Pair nodeEvaluateLogAbs(List<Double> y, double defaultPoint) throws Exception {
        return nodeEvaluateLogAbs(y, ArrayUtils.newDoubleList(y.size(), 1.0), defaultPoint);
    }


}
