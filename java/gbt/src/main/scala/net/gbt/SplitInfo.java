package net.gbt;

import org.javatuples.Pair;

/**
 * Split info holds information for a potential split for the training data on a node.
 * Only used during training
 */
public class SplitInfo {
    /**
     * splt by this column
     */
    private final String colName;

    /**
     * split value. data going to either true or false direction
     * depends on numerical or categorical column
     */
    private Integer splitValue;

    /**
     * If goes to true, the criterion got.
     */
    private double trueChildCriterion; //the larger the better;
    /**
     * If goes to false, the criterion got.
     */
    private double falseChildCriterion; //the larger the better;


    /**
     * If goes to true, the value to be predicted
     */
    private double trueChildOutput;
    /**
     * If goes to false, the value to be predicted
     */
    private double falseChildOutput;
    private double thisCriterion;
    private boolean haveChildren;


    public SplitInfo(String colName, double thisCriterion, Pair childCriterion, Integer splitValue, Pair childOutputs) {
        this.colName = colName;
        this.splitValue = splitValue;
        this.trueChildCriterion = (double) childCriterion.getValue0();
        this.falseChildCriterion = (double) childCriterion.getValue1();
        this.trueChildOutput = (double) childOutputs.getValue0();
        this.falseChildOutput = (double) childOutputs.getValue1();
        this.thisCriterion = thisCriterion;
        this.haveChildren = true;
    }

    public SplitInfo(String colName, double thisCriterion) {
        this.colName = colName;
        this.thisCriterion = thisCriterion;
    }

    public String getColName() {
        return colName;
    }

    public Integer getSplitValue() {
        return splitValue;
    }

    /**
     * when a best split is found, use this function
     * to create children.
     *
     * @param parent
     * @return
     */
    public void makeTrueFalseChildren(Node parent) {
        Node trueNode = makeTrueNode(parent);
        Node falseNode = makeFalseNode(parent);
        parent.setChildren(trueNode, falseNode);
    }

    private Node makeTrueNode(Node parent) {
        Node node = new Node(trueChildOutput);
        node.setParent(parent);
        node.setSplitFeature(colName);
        node.setSplitValue(splitValue);
        node.setIsTrueSplit(true);
        return node;
    }

    private Node makeFalseNode(Node parent) {
        Node node = new Node(falseChildOutput);
        node.setParent(parent);
        node.setSplitFeature(colName);
        node.setSplitValue(splitValue);
        node.setIsTrueSplit(false);
        return node;
    }

    public double getGain() {
        return falseChildCriterion + trueChildCriterion - thisCriterion;
    }

    public double getParentCriterion() {
        return thisCriterion;
    }

    public double getTrueCriterion() {
        return trueChildCriterion;
    }

    public double getFalseCriterion() {
        return falseChildCriterion;
    }

    @Override
    public String toString() {
        return "SplitInfo{" +
                "colName='" + colName + '\'' +
                ", splitValue=" + splitValue +
                ", trueChildCriterion=" + trueChildCriterion +
                ", falseChildCriterion=" + falseChildCriterion +
                ", trueChildOutput=" + trueChildOutput +
                ", falseChildOutput=" + falseChildOutput +
                ", thisCriterion=" + thisCriterion +
                ", gain =" + getGain() +
                ", haveChildren=" + haveChildren +
                '}';
    }
}
