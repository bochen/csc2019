package net.gbt;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.github.luben.zstd.ZstdInputStream;
import com.github.luben.zstd.ZstdOutputStream;
import net.gbt.data.DataFrame;
import net.gbt.data.ParqDataLoader;
import org.javatuples.Pair;
import org.javatuples.Triplet;
import org.javatuples.Quartet;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class MedianRandomForeast {
    private final String lossType;
    private final int verbose;
    private ArrayList<RegressionTree> treeList = new ArrayList<>();
    private double learning_rate = 0.1;
    private int minDataInLeaf = -1;
    private int maxDepth = -1;
    private MulticoreExecutor multicoreExecutor;
    private double col_sample = 1.0;
    private boolean use_bootstrap = false;

    public MedianRandomForeast(String lossType, int verbose) {
        this.lossType = lossType.toLowerCase();
        this.verbose = verbose;
    }

    public void train(DataFrame traindf, int numTree, double learning_rate, int maxDepth, int minDataInLeaf) throws Exception {
        train(traindf, null, numTree, learning_rate, maxDepth, minDataInLeaf);
    }


    public Pair train(DataFrame traindfRaw, DataFrame testdf, int numTree, double learning_rate, int maxDepth, int minDataInLeaf) throws Exception {
        this.learning_rate = learning_rate;
        this.maxDepth = maxDepth;
        this.minDataInLeaf = minDataInLeaf;
        List<Double> trainTarget = traindfRaw.getTarget();
        List<Double> target = trainTarget;

        List<List<Double>> testPreds = null;
        if (testdf != null) {
            testPreds = new ArrayList<>();
        }
        List<List<Double>> trainPreds = new ArrayList<>();

        if (multicoreExecutor==null) {
            for (int i = 0; i < numTree; i++) {
                Triplet a = train_a_tree(i, traindfRaw, target, testdf);
                treeList.add((RegressionTree) a.getValue0());
                List<Double> thisTrainPred = (List<Double>) a.getValue1();
                trainPreds.add(thisTrainPred);
                if (testdf != null) {
                    List<Double> thisTestPred = (List<Double>) a.getValue2();
                    testPreds.add(thisTestPred);
                }

            }
        }else{
            List<TrainTask> tasks = new ArrayList<>(numTree);

            for (int i = 0; i < numTree; i++) {
                tasks.add(new TrainTask(i, this ,traindfRaw,target,testdf));
            }

            List<Triplet> triplets = multicoreExecutor.run(tasks);

            for (Triplet a: triplets){
                treeList.add((RegressionTree) a.getValue0());
                List<Double> thisTrainPred = (List<Double>) a.getValue1();
                trainPreds.add(thisTrainPred);
                if (testdf != null) {
                    List<Double> thisTestPred = (List<Double>) a.getValue2();
                    testPreds.add(thisTestPred);
                }
            }

        }

        return new Pair(trainPreds, testPreds);
    }

    public   Triplet train_a_tree(int i, DataFrame traindfRaw, List<Double> target, DataFrame testdf) throws Exception {
        System.out.println("start making tree " + i);
        RegressionTree tree = use_bootstrap ? trainATree(traindfRaw.bootstrap(), target) : trainATree(traindfRaw, target);
        System.out.println("end  making tree " + i);
        List<Double> thisTrainPred = tree.predict(traindfRaw);
        //List<Double> trainPred = ArrayUtils.median2(trainPreds);
        Double trainLoss = calLoss(target, thisTrainPred);
        List<Double> thisTestPred =null;
        if (testdf != null) {
            thisTestPred = tree.predict(testdf);
            //List<Double> testPred = ArrayUtils.median2(testPreds);
            double testLoss = calLoss(testdf.getTarget(), thisTestPred);
            double testMAELoss = MAELoss.calMAE(testdf.getTarget(), thisTestPred);
            double testLogAbsLoss = LogAbsLoss.calLogAbsError(testdf.getTarget(), thisTestPred);
            System.out.println(String.format(
                    "Iteration %d, Train Loss: %f, Test Loss: %f, Test MAE: %f, Test LMAE: %f",
                    i, trainLoss, testLoss, testMAELoss, testLogAbsLoss));
        } else {
            System.out.println(String.format("Iteration %d, Train Loss: %f", i, trainLoss));
        }

        return new Triplet(tree, thisTrainPred,thisTestPred);
    }

    public List<Double> predict(DataFrame df) throws Exception {
        int n = this.treeList.size();
        return predict(df, n);

    }


    public List<List<Double>> predictRaw(DataFrame df) throws Exception {
        List<List<Double>> pred = new ArrayList<>();
        for (int i = 0; i < treeList.size(); i++) {
            RegressionTree tree = treeList.get(i);
            List<Double> thisPred = tree.predict(df);
            pred.add(thisPred);

        }
        return pred;
    }

    public List<Double> predict(DataFrame df, int n_tree) throws Exception {
        //System.out.println("Use " + n_tree + " trees out of " + treeList.size() + " total");
        List<List<Double>> pred = new ArrayList<>();
        for (int i = 0; i < n_tree; i++) {
            RegressionTree tree = treeList.get(i);
            List<Double> thisPred = tree.predict(df);
            pred.add(thisPred);

        }
        return ArrayUtils.median2(pred, df.getWeights());
    }

    private Double calLoss(List<Double> ytrue, List<Double> ypred) {
        if (this.lossType.equals("mse")) {
            return MSELoss.calRMSE(ytrue, ypred);
        } else if (this.lossType.equals("mae")) {
            return MAELoss.calMAE(ytrue, ypred);
        } else if (this.lossType.equals("logabs")) {
            return LogAbsLoss.calLogAbsError(ytrue, ypred);
        } else {
            return null;
        }
    }

    private RegressionTree trainATree(DataFrame traindf, List<Double> target) throws Exception {
        List<Double> originalTarget = traindf.getTarget();
        traindf.updateTarget(target);

        RegressionTree tree;
        tree = new RegressionTree().setLossType(this.lossType).setColSample(this.col_sample);
        if (false && multicoreExecutor != null) {
            tree = tree.setMultiCoreExecutor(multicoreExecutor);
        }
        if (maxDepth > 0) {
            tree.setMaxDepth(this.maxDepth);
        }
        if (minDataInLeaf > 0) {
            tree.setMinDataInLeaf(minDataInLeaf);
        }

        tree.train(traindf);
        tree.clearValidFlag();
        traindf.updateTarget(originalTarget);
        return tree;
    }

    public static MedianRandomForeast load(String modelpath) throws IOException {
        // Reading the object from a file
        System.out.println("read model from " + modelpath);
        FileInputStream file = new FileInputStream(modelpath);
        ZstdInputStream zis = new ZstdInputStream(file);

        Input input = new Input(zis);
        Kryo kryo = new Kryo();
        MedianRandomForeast tree = kryo.readObject(input, MedianRandomForeast.class);
        input.close();
        zis.close();
        file.close();
        System.out.println("finish reading model from  " + modelpath);
        return tree;
    }

    public void saveModel(String output_file) throws Exception {
        System.out.println("writing model to " + output_file);
        //Saving of object in a file
        FileOutputStream file = new FileOutputStream(output_file);
        ZstdOutputStream zos = new ZstdOutputStream(file);
        Output okyro = new Output(zos);

        // Method for serialization of object
        Kryo kryo = new Kryo();
        kryo.writeObject(okyro, this);

        okyro.close();
        zos.close();
        file.close();
        System.out.println("finish writing " + output_file);


    }


    public MedianRandomForeast setMultiCoreExecutor(MulticoreExecutor executor) {
        this.multicoreExecutor = executor;
        return this;
    }

    public MedianRandomForeast setColSample(double v) {
        this.col_sample = v;
        return this;
    }

    public MedianRandomForeast setBootstrap(boolean b) {
        this.use_bootstrap = b;
        return this;
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 7) {
            System.out.println("cmd <train_parq> <test_parq> <loss> <bootstrap> <thread> <ntree> <colsamp>");
            System.exit(-1);
        }

        DataFrame traindf;
        DataFrame testdf;

        int nthread = Integer.valueOf(args[4]);
        int ntree = Integer.valueOf(args[5]);
        double colsamp = Double.valueOf(args[6]);
        String loss = args[2];
        boolean bootstrap = Integer.valueOf(args[3]) > 0;
        if (true) {
            if (true) {
                String fpath1 = args[0];
                if (Utils.path_exists(fpath1 + ".bin", false)) {
                    traindf = DataFrame.load(fpath1 + ".bin");
                    System.err.println("loaded " + fpath1 + ".bin");

                } else {
                    ParqDataLoader dataLoader = new ParqDataLoader("target");
                    traindf = dataLoader.load(fpath1);
                    System.err.println("loaded " + fpath1);
                    traindf.save(fpath1 + ".bin");
                }
            }
            if (true) {
                String fpath1 = args[1];
                if (Utils.path_exists(fpath1 + ".bin", false)) {
                    testdf = DataFrame.load(fpath1 + ".bin");
                    System.err.println("loaded " + fpath1 + ".bin");

                } else {
                    ParqDataLoader dataLoader = new ParqDataLoader("target");
                    testdf = dataLoader.load(fpath1);
                    System.err.println("loaded " + fpath1);
                    testdf.save(fpath1 + ".bin");
                }
            }

        }


        System.err.println("traindf length " + traindf.size() + " #column: " + traindf.shape1());
        System.err.println("testdf  length " + testdf.size() + " #column: " + testdf.shape1());
        MulticoreExecutor executor = new MulticoreExecutor(nthread);
        try {
            System.err.println("start training model");

            MedianRandomForeast tree = new MedianRandomForeast(loss, 0)
                    .setColSample(colsamp).setBootstrap(bootstrap).setMultiCoreExecutor(executor);
            Pair p = tree.train(traindf, testdf, ntree, 0.1,
                    100, 1);
            System.err.println("end training model");
            List<List<Double>> trainPreds = (List<List<Double>>) p.getValue0();
            List<List<Double>> testPreds = (List<List<Double>>) p.getValue1();

            List<Double> trainPred = ArrayUtils.median2(trainPreds, executor);
            List<Double> testPred = ArrayUtils.median2(testPreds, executor);

            evaluate(trainPred, traindf.getTarget(), traindf.getWeights(),
                    testPred, testdf.getTarget(), testdf.getWeights());

        } finally {
            executor.shutdown();
        }


    }

    private static void evaluate(
            List<Double> trainPred, List<Double> trainy, List<Double> trainWeights,
            List<Double> testPred, List<Double> testy, List<Double> testWeights) {
        System.out.println("Train ymean=" + ArrayUtils.mean(trainy));
        System.out.println("Test ymean=" + ArrayUtils.mean(testy));
        System.out.println("Train RMES Loss=" + evaluate(trainPred, trainy));
        System.out.println("Test RMES Loss=" + evaluate(testPred, testy));
        System.out.println("Train MAE Loss=" + evaluateMAE(trainPred, trainy));
        System.out.println("Test MAE Loss=" + evaluateMAE(testPred, testy));
        System.out.println("Train LogAbs Loss=" + evaluateLogAbs(trainPred, trainy, trainWeights));
        System.out.println("Test LogAbs Loss=" + evaluateLogAbs(testPred, testy, testWeights));
    }

    private static double evaluate(List<Double> trainPred, List<Double> trainy) {
        return MSELoss.calRMSE(trainy, trainPred);

    }


    private static double evaluateMAE(List<Double> trainPred, List<Double> trainy) {
        return MAELoss.calMAE(trainy, trainPred);
    }


    private static double evaluateLogAbs(List<Double> trainPred, List<Double> trainy,
                                         List<Double> trainWeights) {
        return LogAbsLoss.calLogAbsError(trainy, trainPred, trainWeights);
    }

    static class TrainTask implements Callable<Triplet> {


        private final int i;
        private final DataFrame traindfRaw;
        private final List<Double> target;
        private final DataFrame testdf;
        private final MedianRandomForeast rf;

        public TrainTask(int i, MedianRandomForeast rf ,  DataFrame traindfRaw, List<Double> target, DataFrame testdf) {
            this.i=i;
            this.rf=rf;
            this.traindfRaw=traindfRaw;
            this.target=target;
            this.testdf=testdf;
        }

        @Override
        public Triplet call() throws Exception {
            return rf.train_a_tree(i, traindfRaw,target,testdf);
        }
    }
}
