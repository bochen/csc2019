package net.gbt;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.github.luben.zstd.ZstdInputStream;
import com.github.luben.zstd.ZstdOutputStream;
import net.gbt.data.DataFrame;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BoostedRegressionTree {
    private final String lossType;
    private final int verbose;
    private ArrayList<RegressionTree> treeList = new ArrayList<>();
    private double learning_rate = 0.1;
    int bestIteration = -1;
    private int minDataInLeaf = -1;
    private int maxDepth = -1;
    private MulticoreExecutor multicoreExecutor;

    public BoostedRegressionTree(String lossType, int verbose) {
        this.lossType = lossType.toLowerCase();
        this.verbose = verbose;
    }

    public void train(DataFrame traindf, int numTree, double learning_rate, int maxDepth, int minDataInLeaf) throws Exception {
        train(traindf, null, numTree, learning_rate, -1, maxDepth, minDataInLeaf);
    }


    public void train(DataFrame traindf, DataFrame testdf, int numTree, double learning_rate, int earlyStopping, int maxDepth, int minDataInLeaf) throws Exception {
        this.learning_rate = learning_rate;
        this.maxDepth = maxDepth;
        this.minDataInLeaf = minDataInLeaf;
        if (earlyStopping > 0) {
            assert (testdf != null);
        }
        List<Double> trainTarget = traindf.getTarget();
        List<Double> target = trainTarget;
        List<Double> trainPred = ArrayUtils.newDoubleList(target.size(), 0.0);
        List<Double> testPred = null;
        List<Double> testLosses = new ArrayList<>();
        double bestTestLoss = Double.MAX_VALUE;


        if (testdf != null) {
            testPred = ArrayUtils.newDoubleList(testdf.getTarget().size(), 0.0);
        }
        for (int i = 0; i < numTree; i++) {
            RegressionTree tree = trainATree(traindf, target);
            treeList.add(tree);
            List<Double> thisTrainPred = tree.predict(traindf);
            trainPred = ArrayUtils.add(trainPred, ArrayUtils.multiply(thisTrainPred, learning_rate));

            double trainLoss = calLoss(trainTarget, trainPred);
            target = ArrayUtils.subtract(trainTarget, trainPred);
            if (testdf != null) {
                List<Double> thisTestPred = tree.predict(testdf);
                testPred = ArrayUtils.add(testPred, ArrayUtils.multiply(thisTestPred, learning_rate));
                List<Double> tmpTestPred= testPred;
                //List<Double> tmpTestPred = ArrayUtils.add(testPred, thisTestPred);
                double testLoss = calLoss(testdf.getTarget(), tmpTestPred);
                double testMAELoss = MAELoss.calMAE(testdf.getTarget(), tmpTestPred);
                double testLogAbsLoss = LogAbsLoss.calLogAbsError(testdf.getTarget(), tmpTestPred);
                if (bestTestLoss > testLoss) {
                    bestTestLoss = testLoss;
                    bestIteration = i;
                }
                testLosses.add(testLoss);
                System.out.println(String.format("Iteration %d, Train Loss: %f, Test Loss: %f, Test MAE: %f, Test LMAE: %f", i, trainLoss, testLoss, testMAELoss, testLogAbsLoss));
                if (earlyStopping > 1 && i - bestIteration >= earlyStopping) {
                    System.out.println(String.format("Early stopping at iteration %d, best iteration is %d, best loss is %f", i,bestIteration, bestTestLoss));
                    break;
                }
            } else {
                System.out.println(String.format("Iteration %d, Train Loss: %f", i, trainLoss));
            }

        }
    }

    public List<Double> predict(DataFrame df) throws Exception {
        int n = this.treeList.size();
        if (this.bestIteration >= 0) {
            n = this.bestIteration + 1;
        }
        return predict(df, n);

    }

    public List<Double> predict(DataFrame df, int n_tree) throws Exception {
        List<Double> pred = ArrayUtils.newDoubleList(df.size(), 0.0);
        for (int i = 0; i < n_tree; i++) {
            RegressionTree tree = treeList.get(i);
            List<Double> thisPred = tree.predict(df);
            if(false && i+1==n_tree){
                pred = ArrayUtils.add(pred, thisPred);
            } else {
                pred = ArrayUtils.add(pred, ArrayUtils.multiply(thisPred, this.learning_rate));
            }
        }
        return pred;
    }

    private Double calLoss(List<Double> ytrue, List<Double> ypred) {
        if (this.lossType.equals("mse")) {
            return MSELoss.calRMSE(ytrue, ypred);
        }else if (this.lossType.equals("mae")) {
            return MAELoss.calMAE(ytrue, ypred);
        }else if (this.lossType.equals("logabs")) {
            return LogAbsLoss.calLogAbsError(ytrue, ypred);
        } else {
            return null;
        }
    }

    private RegressionTree trainATree(DataFrame traindf, List<Double> target) throws Exception {
        List<Double> originalTarget = traindf.getTarget();
        traindf.updateTarget(target);

        RegressionTree tree;
        tree = new RegressionTree().setLossType(this.lossType);
        if (multicoreExecutor != null) {
            tree = tree.setMultiCoreExecutor(multicoreExecutor);
        }
        if (maxDepth > 0) {
            tree.setMaxDepth(this.maxDepth);
        }
        if (minDataInLeaf > 0) {
            tree.setMinDataInLeaf(minDataInLeaf);
        }
        tree.train(traindf);
        tree.clearValidFlag();
        traindf.updateTarget(originalTarget);
        return tree;
    }

    public static BoostedRegressionTree load(String modelpath) throws IOException {
        // Reading the object from a file
        System.out.println("read model from " + modelpath);
        FileInputStream file = new FileInputStream(modelpath);
        ZstdInputStream zis = new ZstdInputStream(file);

        Input input = new Input(zis);
        Kryo kryo = new Kryo();
        BoostedRegressionTree tree = kryo.readObject(input, BoostedRegressionTree.class);
        input.close();
        zis.close();
        file.close();
        System.out.println("finish reading model from  " + modelpath);
        return tree;
    }

    public void saveModel(String output_file) throws Exception {
        System.out.println("writing model to " + output_file);
        //Saving of object in a file
        FileOutputStream file = new FileOutputStream(output_file);
        ZstdOutputStream zos = new ZstdOutputStream(file);
        Output okyro = new Output(zos);

        // Method for serialization of object
        Kryo kryo = new Kryo();
        kryo.writeObject(okyro, this);

        okyro.close();
        zos.close();
        file.close();
        System.out.println("finish writing " + output_file);


    }


    public BoostedRegressionTree setMultiCoreExecutor(MulticoreExecutor executor) {
        this.multicoreExecutor = executor;
        return this;
    }
}
