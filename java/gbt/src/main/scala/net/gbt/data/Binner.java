package net.gbt.data;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.github.luben.zstd.ZstdInputStream;
import com.github.luben.zstd.ZstdOutputStream;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class Binner implements Serializable {
    private static final long serialVersionUID = 723211L;
    private int minDataInLeaf;
    private boolean isCategory;
    private int maxBin;
    private static org.slf4j.Logger logger = LoggerFactory.getLogger(Binner.class);
    private String name;

    private HashMap<Object, Integer> categoryToInteger = new HashMap<>();

    Vector<Double> bin_upper_bound_ = new Vector<>();
    private boolean is_trivial_;
    private int default_bin_;
    private Double min_val_;
    private Double max_val_;
    private float sparse_rate_;
    private static Double kZeroThreshold = 1e-9;

    private Binner(){}
    public Binner(String name, boolean isCategory, int maxBin, int minDataInLeaf) {
        this.name = name;
        this.isCategory = isCategory;
        this.maxBin = maxBin;
        this.minDataInLeaf = minDataInLeaf;
    }

    public Binner(String name, boolean isCategory) {
        this(name, isCategory, 255, 1);
    }


    public Binner train(List<Object> values) throws Exception {
        if (isCategory) {
            return trainCategory(values);
        } else {
            return trainNumerical(values);
        }
    }

    private Binner trainNumerical(List<Object> values) throws Exception {
        Double[] newvalues = values.toArray(new Double[values.size()]);
        findBin(newvalues, maxBin, minDataInLeaf, minDataInLeaf);
        return this;
    }

    private Binner trainCategory(List<Object> values) {
        List<Object> valuesDistinct = values.stream().distinct().collect(Collectors.toList());
        if (valuesDistinct.size() > 255) {
            logger.warn(String.format("Distinct values of %s is too big which is %d", name, valuesDistinct.size()));
        }

        for (int i = 0; i < valuesDistinct.size(); i++) {
            categoryToInteger.put(valuesDistinct.get(i), i);
        }
        return this;
    }

    public CompactArray transform(List<Object> values) throws Exception {
        if (isCategory) {
            return transformCategory(values);
        } else {
            return transformNumerical(values);
        }
    }

    private CompactArray transformNumerical(List<Object> values) throws Exception {
        List<Integer> newValues = values.stream()
                .map(o -> valueToBin((Double) o))
                .collect(Collectors.toList());

        return new CompactArray(newValues);
    }

    private CompactArray transformCategory(List<Object> values) throws IOException {
        List<Integer> newValues = values.stream()
                .map(o -> categoryToInteger.get(o))
                .collect(Collectors.toList());

        return new CompactArray(newValues);

    }

    @Override
    public String toString() {
        return "Binner{" +
                "minDataInLeaf=" + minDataInLeaf +
                ", isCategory=" + isCategory +
                ", maxBin=" + maxBin +
                ", name='" + name + '\'' +
                ", #category=" + categoryToInteger.size() +
                ", #bin_upper_bound_=" + bin_upper_bound_.size() +
                ", is_trivial_=" + is_trivial_ +
                ", default_bin_=" + default_bin_ +
                ", min_val_=" + min_val_ +
                ", max_val_=" + max_val_ +
                ", sparse_rate_=" + sparse_rate_ +
                '}';
    }

    private static List<Map.Entry<Double, Integer>> sortByComparator(Map<Double, Integer> unsortMap, final boolean order) {

        List<Map.Entry<Double, Integer>> list = new LinkedList<Map.Entry<Double, Integer>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<Double, Integer>>() {
            public int compare(Map.Entry<Double, Integer> o1,
                               Map.Entry<Double, Integer> o2) {
                if (order) {
                    return o1.getKey().compareTo(o2.getKey());
                } else {
                    return o2.getKey().compareTo(o1.getKey());

                }
            }
        });
        return list;


    }


    private void findBin(Double[] values,
                         int max_bin, int min_data_in_bin, int min_split_data) throws Exception {
        int num_sample_values = values.length;
        int total_sample_cnt = values.length;
        int tmp_num_sample_values = values.length;

        num_sample_values = tmp_num_sample_values;

        default_bin_ = 0;
        // find distinct_values first
        //Vector<Double> distinct_values = new Vector<>();
        //Vector<Integer> counts = new Vector<>();
        HashMap<Double, Integer> distinct_values_dict = new HashMap<>();


        for (int i = 0; i < num_sample_values; ++i) {
            double v = values[i];
            if (!distinct_values_dict.containsKey(v)) {
                distinct_values_dict.put(v, 0);
            }
            distinct_values_dict.put(v, distinct_values_dict.get(v) + 1);
        }


        List<Map.Entry<Double, Integer>> tmpvalues = sortByComparator(distinct_values_dict, true);

        ArrayList<Double> distinct_values = new ArrayList<>();
        ArrayList<Integer> counts = new ArrayList<>();
        for (int i = 0; i < tmpvalues.size(); i++) {
            distinct_values.add(tmpvalues.get(i).getKey());
            counts.add(tmpvalues.get(i).getValue());
        }
        Collections.sort(distinct_values);


        min_val_ = distinct_values.get(0);
        max_val_ = distinct_values.get(distinct_values.size() - 1);
        Vector<Integer> cnt_in_bin;
        int num_distinct_values = distinct_values.size();
        if (true) {

            bin_upper_bound_ = FindBinWithZeroAsOneBin(distinct_values, counts,
                    num_distinct_values, max_bin, total_sample_cnt, min_data_in_bin);

            int num_bin_ = bin_upper_bound_.size();
            {
                cnt_in_bin = new Vector<>(Collections.nCopies(max_bin, 0));
                int i_bin = 0;
                for (int i = 0; i < num_distinct_values; ++i) {
                    if (distinct_values.get(i) > bin_upper_bound_.get(i_bin)) {
                        ++i_bin;
                    }
                    cnt_in_bin.set(i_bin, cnt_in_bin.get(i_bin) + counts.get(i));
                }
            }
            if (!(num_bin_ <= max_bin)) {
                throw new Exception("num_bin < max_bin " + num_bin_ + " < " + max_bin);
            }
        }
        int num_bin_ = bin_upper_bound_.size();

        // check trivial(num_bin_ == 1) feature
        if (num_bin_ <= 1) {
            is_trivial_ = true;
        } else {
            is_trivial_ = false;
        }
        // check useless bin
        if (!is_trivial_ && NeedFilter(cnt_in_bin, total_sample_cnt, min_split_data)) {
            is_trivial_ = true;
        }

        if (!is_trivial_) {
            default_bin_ = valueToBin(0.0);

        }
        if (!is_trivial_) {
            // calculate sparse rate
            sparse_rate_ = 1.0f * cnt_in_bin.get(default_bin_) / total_sample_cnt;
        } else {
            sparse_rate_ = 1.0f;
        }
    }

    private Vector<Double> FindBinWithZeroAsOneBin(ArrayList<Double> distinct_values, ArrayList<Integer> counts, int num_distinct_values, int max_bin, int total_sample_cnt, int min_data_in_bin) throws Exception {
        Vector<Double> bin_upper_bound = new Vector<>();
        int left_cnt_data = 0;
        int cnt_zero = 0;
        int right_cnt_data = 0;
        for (int i = 0; i < num_distinct_values; ++i) {
            if (distinct_values.get(i) <= -kZeroThreshold) {
                left_cnt_data += counts.get(i);
            } else if (distinct_values.get(i) > kZeroThreshold) {
                right_cnt_data += counts.get(i);
            } else {
                cnt_zero += counts.get(i);
            }
        }

        int left_cnt = -1;
        for (int i = 0; i < num_distinct_values; ++i) {
            if (distinct_values.get(i) > -kZeroThreshold) {
                left_cnt = i;
                break;
            }
        }

        if (left_cnt < 0) {
            left_cnt = num_distinct_values;
        }

        if (left_cnt > 0) {
            int left_max_bin = (int) (1.0 * (left_cnt_data) / (total_sample_cnt - cnt_zero) * (max_bin - 1));
            left_max_bin = Math.max(1, left_max_bin);
            bin_upper_bound = GreedyFindBin(distinct_values, counts, left_cnt, left_max_bin, left_cnt_data, min_data_in_bin);
            bin_upper_bound.set(bin_upper_bound.size() - 1, -kZeroThreshold);
        }

        int right_start = -1;
        for (int i = left_cnt; i < num_distinct_values; ++i) {
            if (distinct_values.get(i) > kZeroThreshold) {
                right_start = i;
                break;
            }
        }

        if (right_start >= 0) {
            int right_max_bin = max_bin - 1 - bin_upper_bound.size();
            if (!(right_max_bin > 0)) {
                throw new Exception("right_max_bin <=0: " + right_max_bin);
            }

            Vector<Double> right_bounds =
                    GreedyFindBin(distinct_values.subList(right_start, distinct_values.size())
                            , counts.subList(right_start, counts.size()),
                            num_distinct_values - right_start, right_max_bin, right_cnt_data, min_data_in_bin);
            bin_upper_bound.add(1e-18);
            bin_upper_bound.addAll(right_bounds);
        } else {
            bin_upper_bound.add(Double.MAX_VALUE);
        }
        return bin_upper_bound;
    }

    private static double GetDoubleUpperBound(double a) {
        return Math.nextAfter(a, Double.MAX_VALUE);
    }

    private static boolean CheckDoubleEqualOrdered(double a, double b) {
        double upper = Math.nextAfter(a, Double.MAX_VALUE);
        return b <= upper;
    }

    private Vector<Double> GreedyFindBin(List<Double> distinct_values, List<Integer>
            counts, int num_distinct_values, int max_bin, int total_cnt, int min_data_in_bin) throws Exception {
        Vector<Double> bin_upper_bound = new Vector<>();
        if (!(max_bin > 0)) {
            throw new Exception("max_bin<=0: " + max_bin);
        }
        if (num_distinct_values <= max_bin) {
            bin_upper_bound.clear();
            int cur_cnt_inbin = 0;
            for (int i = 0; i < num_distinct_values - 1; ++i) {
                cur_cnt_inbin += counts.get(i);
                if (cur_cnt_inbin >= min_data_in_bin) {
                    double val = GetDoubleUpperBound((distinct_values.get(i) + distinct_values.get(i + 1)) / 2.0);
                    if (bin_upper_bound.isEmpty() || !CheckDoubleEqualOrdered(bin_upper_bound.get(bin_upper_bound.size() - 1)
                            , val)) {
                        bin_upper_bound.add(val);
                        cur_cnt_inbin = 0;
                    }
                }
            }
            cur_cnt_inbin += counts.get(num_distinct_values - 1);
            bin_upper_bound.add(Double.MAX_VALUE);
        } else {
            if (min_data_in_bin > 0) {
                max_bin = Math.min(max_bin, (int) (total_cnt / min_data_in_bin));
                max_bin = Math.max(max_bin, 1);
            }
            double mean_bin_size = 1.0 * (total_cnt) / max_bin;

            // mean size for one bin
            int rest_bin_cnt = max_bin;
            int rest_sample_cnt = (total_cnt);
            Vector<Boolean> is_big_count_value = new Vector<>(Collections.nCopies(num_distinct_values, false));
            for (int i = 0; i < num_distinct_values; ++i) {
                if (counts.get(i) >= mean_bin_size) {
                    is_big_count_value.set(i, true);
                    --rest_bin_cnt;
                    rest_sample_cnt -= counts.get(i);
                }
            }
            mean_bin_size = 1.0 * (rest_sample_cnt) / rest_bin_cnt;
            Vector<Double> upper_bounds = new Vector<>(Collections.nCopies(max_bin, Double.MAX_VALUE));
            Vector<Double> lower_bounds = new Vector<>(Collections.nCopies(max_bin, Double.MAX_VALUE));

            int bin_cnt = 0;
            lower_bounds.set(bin_cnt, distinct_values.get(0));
            int cur_cnt_inbin = 0;
            for (int i = 0; i < num_distinct_values - 1; ++i) {
                if (!is_big_count_value.get(i)) {
                    rest_sample_cnt -= counts.get(i);
                }
                cur_cnt_inbin += counts.get(i);
                // need a new bin
                if (is_big_count_value.get(i) || cur_cnt_inbin >= mean_bin_size ||
                        (is_big_count_value.get(i + 1)
                                && cur_cnt_inbin >= Math.max(1.0, mean_bin_size * 0.5f))) {
                    upper_bounds.set(bin_cnt, distinct_values.get(i));
                    ++bin_cnt;
                    lower_bounds.set(bin_cnt, distinct_values.get(i + 1));
                    if (bin_cnt >= max_bin - 1) {
                        break;
                    }
                    cur_cnt_inbin = 0;
                    if (!is_big_count_value.get(i)) {
                        --rest_bin_cnt;
                        mean_bin_size = rest_sample_cnt / (double) (rest_bin_cnt);
                    }
                }
            }
            ++bin_cnt;
            // update bin upper bound
            bin_upper_bound.clear();
            for (int i = 0; i < bin_cnt - 1; ++i) {
                double val = GetDoubleUpperBound((upper_bounds.get(i) + lower_bounds.get(i + 1)) / 2.0);
                if (bin_upper_bound.isEmpty() || !CheckDoubleEqualOrdered(bin_upper_bound.get(bin_upper_bound.size() - 1), val)) {
                    bin_upper_bound.add(val);
                }
            }
            // last bin upper bound
            bin_upper_bound.add(Double.MAX_VALUE);
        }
        return bin_upper_bound;
    }

    private boolean NeedFilter(Vector<Integer> cnt_in_bin, int total_cnt, int filter_cnt) {
        if (true) {
            int sum_left = 0;
            for (int i = 0; i < cnt_in_bin.size() - 1; ++i) {
                sum_left += cnt_in_bin.get(i);
                if (sum_left >= filter_cnt && total_cnt - sum_left >= filter_cnt) {
                    return false;
                }
            }
        }
        return true;
    }

    private int valueToBin(Double value) {
        // binary search to find bin
        int l = 0;
        int num_bin_ = bin_upper_bound_.size();
        int r = num_bin_ - 1;

        while (l < r) {
            int m = (r + l - 1) / 2;
            if (value <= bin_upper_bound_.get(m)) {
                r = m;
            } else {
                l = m + 1;
            }
        }
        return l;

    }

    public boolean isTrivial() {
        return is_trivial_;
    }


    public static Binner load(String modelpath) throws IOException {
        // Reading the object from a file
        System.out.println("read binner from " + modelpath);
        FileInputStream file = new FileInputStream(modelpath);
        ZstdInputStream zis = new ZstdInputStream(file);

        Input input = new Input(zis);
        Kryo kryo = new Kryo();
        Binner binner  = kryo.readObject(input, Binner.class);
        input.close();
        zis.close();
        file.close();
        System.out.println("finish reading binner from  " + modelpath);
        return binner;
    }

    public void saveModel(String output_file) throws Exception {
        System.out.println("writing model to " + output_file);
        //Saving of object in a file
        FileOutputStream file = new FileOutputStream(output_file);
        ZstdOutputStream zos = new ZstdOutputStream(file);
        Output okyro = new Output(zos);

        // Method for serialization of object
        Kryo kryo = new Kryo();
        kryo.writeObject(okyro, this);

        okyro.close();
        zos.close();
        file.close();
        System.out.println("finish writing " + output_file);


    }

    public boolean isCategorical() {
        return this.isCategory;
    }
}
