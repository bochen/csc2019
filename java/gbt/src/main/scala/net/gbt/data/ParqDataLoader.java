package net.gbt.data;

import org.apache.hadoop.conf.Configuration;
import org.apache.parquet.column.page.PageReadStore;
import org.apache.parquet.example.data.Group;
import org.apache.parquet.example.data.simple.convert.GroupRecordConverter;
import org.apache.parquet.format.converter.ParquetMetadataConverter;
import org.apache.parquet.hadoop.ParquetFileReader;
import org.apache.parquet.hadoop.metadata.ParquetMetadata;
import org.apache.parquet.io.ColumnIOFactory;
import org.apache.parquet.io.MessageColumnIO;
import org.apache.parquet.io.RecordReader;
import org.apache.parquet.schema.MessageType;
import org.apache.hadoop.fs.Path;
import org.apache.parquet.schema.Type;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ParqDataLoader {
    private final String targetName;
    private final Set<String> useColumns;
    private final Set<String> categoricalColumns;
    private final int minDataInLeaf;
    private final int maxBin;
    private HashMap<String, Binner> binners;

    public ParqDataLoader(String targetName, Set<String> useColumns, Set<String> categoricalColumns,
                          int minDataInLeaf, int maxBin) {
        if (categoricalColumns == null) {
            categoricalColumns = new HashSet<>();
        }
        this.targetName = targetName;
        this.useColumns = useColumns;
        this.categoricalColumns = categoricalColumns;
        this.minDataInLeaf = minDataInLeaf;
        this.maxBin = maxBin;
    }

    public ParqDataLoader(String targetName, Set<String> useColumns,
                          Set<String> categoricalColumns) {
        this(targetName, useColumns, categoricalColumns, 1, 255);
    }

    public ParqDataLoader(String targetName) {
        this(targetName, null, new HashSet<>());
    }

    public DataFrame load(String parqFile) throws Exception {
        return load(parqFile, true);

    }

    private List<Group> readParq(String parqFile) throws IOException {
        Configuration conf = new Configuration();
        Configuration hadoopConfig = conf ;
	//hadoopConfig.set("fs.hdfs.impl", org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
    	//hadoopConfig.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());
        List<Group> datalist = new ArrayList<>();

        try {
            Path path = new Path(parqFile);
            ParquetMetadata readFooter = ParquetFileReader.readFooter(conf, path, ParquetMetadataConverter.NO_FILTER);
            MessageType schema = readFooter.getFileMetaData().getSchema();
            ParquetFileReader r = new ParquetFileReader(conf, path, readFooter);

            PageReadStore pages = null;
            try {
                while (null != (pages = r.readNextRowGroup())) {
                    final long rows = pages.getRowCount();

                    final MessageColumnIO columnIO = new ColumnIOFactory().getColumnIO(schema);
                    final RecordReader recordReader = columnIO.getRecordReader(pages, new GroupRecordConverter(schema));
                    for (int i = 0; i < rows; i++) {
                        final Group g = (Group) recordReader.read();
                        datalist.add(g);
                        //printGroup(g);

                        // TODO Compare to System.out.println(g);
                    }
                }
            } finally {
                r.close();
            }
        } catch (IOException e) {
            System.out.println("Error reading parquet file.");
            e.printStackTrace();
            throw e;
        }
        return datalist;

    }

    public DataFrame load(String parqFile, boolean histGen) throws Exception {

        List<Group> listRecords = readParq(parqFile);
        System.out.println("#records: "+listRecords.size());

        if (listRecords.isEmpty()) {
            throw new Exception("empty parq file");
        }

        List<String> columns = getColumns(listRecords.get(0)).stream().filter(o -> !o.equals(targetName))
                .filter(o -> useColumns == null || useColumns.contains(o)).collect(Collectors.toList());


        if (this.binners == null && !histGen) {
            throw new Exception("binners are missing while not generating binners");
        } else if (this.binners != null && histGen) {
            throw new Exception("binners exist while generating binners");
        }

        DataFrame df = new DataFrame(listRecords.size(), minDataInLeaf, maxBin);
        if (this.binners == null) {
            df.setNeedCreateBinner(true);
        } else {
            df.setBinner(binners);
        }

        for (String colname : columns) {
            boolean isCategorical = categoricalColumns.contains(colname);
            ArrayList<Object> v = getValue(colname, listRecords, isCategorical);
            df.addColumn(colname, v, isCategorical, maxBin);
        }
        List<Double> target = getValue(targetName, listRecords, false).stream().map(o -> (Double) o).collect(Collectors.toList());
        df.setTarget(targetName, target);
        this.binners = df.getBinners();
        return df;
    }

    private static ArrayList<String> getColumns(Group g) {
        ArrayList<String> names = new ArrayList<String>();
        int fieldCount = g.getType().getFieldCount();
        for (int field = 1; field < fieldCount; field++) {
            Type fieldType = g.getType().getType(field);
            String fieldName = fieldType.getName();
            names.add(fieldName);
        }
        return names;
    }

    private static ArrayList<Object> getValue(String fieldName, List<Group> glist, boolean isCategorical) {
        ArrayList<Object> list = new ArrayList<Object>();
        for (Group g : glist) {
            list.addAll(getValue(fieldName, g, isCategorical));
        }
        return list;
    }

    private static ArrayList<Object> getValue(String fieldName, Group g, boolean isCategorical) {
        ArrayList<Object> list = new ArrayList<Object>();
        int field = g.getType().getFieldIndex(fieldName);
        if (true) {
            int valueCount = g.getFieldRepetitionCount(field);
            Type fieldType = g.getType().getType(field);

            for (int index = 0; index < valueCount; index++) {
                if (isCategorical) {
                    String a = g.getValueToString(field, index);
                    list.add(a);
                } else {
                    Double a = g.getDouble(field, index);
                    list.add(a);
                }
            }
        }
        return list;
    }

    private static void printGroup(Group g) {
        int fieldCount = g.getType().getFieldCount();
        for (int field = 0; field < fieldCount; field++) {
            int valueCount = g.getFieldRepetitionCount(field);

            Type fieldType = g.getType().getType(field);
            String fieldName = fieldType.getName();

            for (int index = 0; index < valueCount; index++) {
                if (fieldType.isPrimitive()) {
                    System.out.println(fieldName + " " + g.getValueToString(field, index));
                }
            }
        }
        System.out.println("");
    }

}
