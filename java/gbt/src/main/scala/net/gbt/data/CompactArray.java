package net.gbt.data;

import net.jpountz.lz4.LZ4Compressor;
import net.jpountz.lz4.LZ4Factory;
import net.jpountz.lz4.LZ4FastDecompressor;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class CompactArray {
    private int compressedLength;
    private byte[] compressed;

    public byte[] getCompressedBytes() {
        return compressed;
    }

    public int getCompressedLength() {
        return compressedLength;
    }

    public int getDecompressedLength() {
        return decompressedLength;
    }

    private int decompressedLength;
    transient LZ4Factory factory = LZ4Factory.fastestInstance();
    transient LZ4FastDecompressor decompressor = factory.fastDecompressor();

    private  CompactArray(){}
    public CompactArray(List<Integer> values) throws IOException {
        byte[] uncompressBytes = toBytes(values);
        this.decompressedLength = uncompressBytes.length;
        this.compress(uncompressBytes);
    }

    public List<Integer> getValues() throws IOException, ClassNotFoundException {
        byte[] uncompressedBytes = uncompress();
        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(uncompressedBytes));
        List<Integer> list = (List<Integer>) ois.readObject();
        return list;
    }

    private byte[] uncompress() {
        byte[] restored = new byte[decompressedLength];
        int compressedLength2 = decompressor.decompress(compressed, 0, restored, 0, decompressedLength);
        return restored;
    }

    private byte[] toBytes(List<Integer> values) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(values);
        byte[] bytes = bos.toByteArray();
        return bytes;
    }


    private int compress(byte[] values) {

        final int decompressedLength = values.length;
        LZ4Compressor compressor = factory.fastCompressor();
        int maxCompressedLength = compressor.maxCompressedLength(decompressedLength);
        byte[] compressed = new byte[maxCompressedLength];
        int compressedLength = compressor.compress(values, 0, decompressedLength, compressed, 0, maxCompressedLength);
        this.compressed = Arrays.copyOfRange(compressed, 0,compressedLength);
        this.compressedLength = compressedLength;
        return compressedLength;
    }

    public int nBytes(){
        return compressedLength;
    }

    public double getCompressRatio() {
        return 1 - 1.0 * compressedLength / decompressedLength;
    }
}
