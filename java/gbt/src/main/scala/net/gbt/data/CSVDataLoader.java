package net.gbt.data;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.Reader;
import java.util.*;
import java.util.stream.Collectors;

public class CSVDataLoader {
    private final String targetName;
    private final Set<String> useColumns;
    private final Set<String> categoricalColumns;
    private final int minDataInLeaf;
    private final int maxBin;
    private HashMap<String, Binner> binners;

    public CSVDataLoader(String targetName, Set<String> useColumns, Set<String> categoricalColumns,
                         int minDataInLeaf, int maxBin) {
        if (categoricalColumns == null) {
            categoricalColumns = new HashSet<>();
        }
        this.targetName = targetName;
        this.useColumns = useColumns;
        this.categoricalColumns = categoricalColumns;
        this.minDataInLeaf = minDataInLeaf;
        this.maxBin = maxBin;
    }

    public CSVDataLoader(String targetName, Set<String> useColumns,
                         Set<String> categoricalColumns) {
        this(targetName, useColumns, categoricalColumns, 1, 255);
    }

    public CSVDataLoader(String targetName) {
        this(targetName, null, new HashSet<>());
    }

    public DataFrame load(String csvFile) throws Exception {
        return load(csvFile, true);

    }

    public DataFrame load(String csvFile, boolean histGen) throws Exception {

        Reader in = new FileReader(csvFile);
        CSVParser parser = CSVParser.parse(in, CSVFormat.RFC4180.withFirstRecordAsHeader());

        List<String> columns = parser.getHeaderNames().stream().filter(o -> !o.equals(targetName))
                .filter(o -> useColumns == null || useColumns.contains(o)).collect(Collectors.toList());

        List<CSVRecord> listRecords = parser.getRecords();
        System.out.println(listRecords.size());
        in.close();

        if (this.binners == null && !histGen) {
            throw new Exception("binners are missing while not generating binners");
        } else if (this.binners != null && histGen) {
            throw new Exception("binners exist while generating binners");
        }

        DataFrame df = new DataFrame(listRecords.size(), minDataInLeaf, maxBin);
        if (this.binners == null) {
            df.setNeedCreateBinner(true);
        } else {
            df.setBinner(binners);
        }

        for (String colname : columns) {
            boolean isCategorical = categoricalColumns.contains(colname);

            if (isCategorical) {
                List<Object> v = listRecords.stream().map(o -> o.get(colname)).collect(Collectors.toList());
                df.addColumn(colname, v, isCategorical, maxBin);
            } else {
                List<Object> v = listRecords.stream().map(o -> Double.valueOf(o.get(colname))).collect(Collectors.toList());
                df.addColumn(colname, v, isCategorical, maxBin);
            }
        }
        List<Double> target = listRecords.stream().map(o -> Double.valueOf(o.get(targetName))).collect(Collectors.toList());
        df.setTarget(targetName, target);
        this.binners = df.getBinners();
        return df;
    }

}
