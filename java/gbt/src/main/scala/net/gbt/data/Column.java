package net.gbt.data;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Column implements Serializable {
    private static final long serialVersionUID = 23211L;

    private String colname;
    private boolean isCategory;


    private CompactArray values;

    private Column(){}
    public Column(String colname, CompactArray values, boolean isCategory) throws Exception {
        this.colname = colname;
        this.isCategory = isCategory;
        this.values = values;
    }

    private List<Integer> makeSortedDistinct(CompactArray values) throws Exception {
        List<Integer> uncompValues = values.getValues();
        List<Integer> ret = uncompValues.stream().distinct().collect(Collectors.toList());
        Collections.sort(ret);
        return ret;
    }

    public String getColname() {
        return colname;
    }

    public boolean isCategory() {
        return isCategory;
    }

    public CompactArray getValues() {
        return values;
    }

    public List<Integer> getValuesAsList() throws  Exception  {
        return getValues().getValues();
    }
}
