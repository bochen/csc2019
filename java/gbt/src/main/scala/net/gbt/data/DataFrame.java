package net.gbt.data;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.github.luben.zstd.ZstdInputStream;
import com.github.luben.zstd.ZstdOutputStream;
import net.gbt.ArrayUtils;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class DataFrame {
    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(DataFrame.class);

    private int minDataInLeaf;
    private int maxBin;
    private int nRows;
    private boolean needCreateBinner = false;
    private HashMap<String, Column> data = new HashMap<>();
    private HashMap<String, Binner> binners = new HashMap<>();
    private List<Double> target;
    private String targetName;
    private List<Double> weights;

        // Method for serialization of object
        transient static Kryo kryo = new Kryo();
static{
        kryo.register(DataFrame.class);
        kryo.register(ArrayList.class);
        kryo.register(List.class);
        kryo.register(Binner.class);
        kryo.register(Column.class);
        kryo.register(CompactArray.class);
        kryo.register(byte[].class);
        kryo.register(Vector.class);
        kryo.register(HashMap.class);
}

    private  DataFrame(){}
    public DataFrame(int nRows, int minDataInLeaf, int maxBin) {
        this.nRows = nRows;
        this.minDataInLeaf = minDataInLeaf;
        this.maxBin = maxBin;
    }

    public DataFrame(int nRows) {
        this(nRows, 1, 255);
    }

    public DataFrame bootstrap() throws Exception {
        DataFrame newdf = new DataFrame(nRows, minDataInLeaf, maxBin);
        newdf.targetName = targetName;

        Random rand = new Random(System.currentTimeMillis());

        int[] indices = new int[size()];

        int n = size();
        for (int index = 0; index < size(); index++) {
            int newIndex = rand.nextInt() % n;
            indices[index] = newIndex;
        }

        newdf.target = sample(this.target, indices);
        if (weights != null) {
            newdf.weights = sample(this.weights, indices);
        }

        for (String colname : data.keySet()) {
            List<Integer> value = data.get(colname).getValuesAsList();
            CompactArray newValue = new CompactArray(sampleInteger(value, indices));
            Column newcol = new Column(colname, newValue, binners.get(colname).isCategorical());
            newdf.data.put(colname, newcol);
        }
        return newdf;
    }

    private static List<Double> sample(List<Double> list, int[] indices) {
        List<Double> ret = new ArrayList<>(indices.length);
        for (int i = 0; i < indices.length; i++) {
            ret.add(list.get(i));
        }
        return ret;
    }

    private static List<Integer> sampleInteger(List<Integer> list, int[] indices) {
        List<Integer> ret = new ArrayList<>(indices.length);
        for (int i = 0; i < indices.length; i++) {
            ret.add(list.get(i));
        }
        return ret;
    }

    public List<String> getColumns() {
        ArrayList<String> val = new ArrayList<String>(data.keySet());
        return val;
    }

    public List<String> sampleColumns(double ratio) {
        int n = (int) (data.size() * ratio);
        assert (n > 0 && n <= data.size());
        List<String> columns = getColumns();
        Collections.shuffle(columns);
        List<String> ret = columns.subList(0, n);
        assert (ret.size() == n);
        return ret;
    }


    public DataFrame addColumn(String colname, List<Object> values, boolean isCategory) throws Exception {
        return addColumn(colname, values, isCategory, this.maxBin);
    }

    public void setTarget(String targetName, List<Double> target) throws Exception {
        this.targetName = targetName;
        if (target.size() != this.size()) {
            throw new Exception("target size is not right");
        }
        this.target = target;
    }

    public DataFrame setNeedCreateBinner(boolean needCreateBinner) {
        this.needCreateBinner = needCreateBinner;
        return this;
    }

    public DataFrame addColumn(String colname, List<Object> values, boolean isCategory, int maxBin) throws Exception {
        if (values.size() != this.size()) {
            throw new Exception("size is not right");
        }
        if (data.containsKey(colname)) {
            throw new Exception("Column already in data: " + colname);
        }
        Binner binner;
        if (needCreateBinner) {
            binner = new Binner(colname, isCategory, maxBin, minDataInLeaf);
            binner.train(values);
            if (binner.isTrivial()) {
                logger.warn("Column " + colname + " is trivial");
            }
            this.binners.put(colname, binner);
        } else {
            binner = binners.get(colname);
            if (binner == null) {
                throw new Exception("binner missing");
            }
        }

        this.data.put(colname, new Column(colname, binner.transform(values), isCategory));

        return this;
    }

    public int size() {
        return nRows;
    }

    public Column getColumn(String colName) {
        return this.data.get(colName);
    }

    public List<Double> getTarget() {
        return target;
    }

    @Override
    public String toString() {
        return "DataFrame{" +
                "minDataInLeaf=" + minDataInLeaf +
                ", maxBin=" + maxBin +
                ", nRows=" + nRows +
                ", shape1=" + shape1() +
                ", needCreateBinner=" + needCreateBinner +
                ", targetName='" + targetName + '\'' +
                '}';
    }

    public int shape0() {
        return this.size();
    }

    public int shape1() {
        return this.data.size();
    }

    public ArrayList<String> getColumnNames() {
        return new ArrayList<String>(data.keySet());
    }

    public void showBinners() {
        for (String name : binners.keySet()) {
            Binner b = binners.get(name);
            System.out.println(name + "\t" + b);
        }
    }

    public void showData() throws Exception {
        for (String name : data.keySet()) {
            List<Integer> b = data.get(name).getValuesAsList();
            System.out.println(name + "\t" + b);
        }
        System.out.println(targetName + "\t" + target);
    }

    public HashMap<String, Binner> getBinners() {
        return binners;
    }

    public void setBinner(HashMap<String, Binner> binners) {
        this.binners = binners;
    }

    public void updateTarget(List<Double> target) {
        this.target = target;
    }

    public List<Double> getWeights() {
        if (this.weights == null) {
            this.weights = ArrayUtils.newDoubleList(target.size(), 1.0);
        }
        return weights;
    }

    public static DataFrame load(String modelpath) throws IOException {
        // Reading the object from a file
        System.out.println("read model from " + modelpath);
        FileInputStream file = new FileInputStream(modelpath);
        ZstdInputStream zis = new ZstdInputStream(file);

        Input input = new Input(zis);
        DataFrame tree = kryo.readObject(input, DataFrame.class);
        input.close();
        zis.close();
        file.close();
        System.out.println("finish reading model from  " + modelpath);
        return tree;
    }

    public void save(String output_file) throws Exception {
        System.out.println("writing data to " + output_file);
        //Saving of object in a file
        FileOutputStream file = new FileOutputStream(output_file);
        ZstdOutputStream zos = new ZstdOutputStream(file);
        Output okyro = new Output(zos);

        kryo.writeObject(okyro, this);

        okyro.close();
        zos.close();
        file.close();
        System.out.println("finish writing " + output_file);

    }
}
