package net.gbt.data;

import java.util.HashMap;

public class IndexedData {


    private HashMap<Integer, Integer > list = new HashMap<>();

    public IndexedData(){

    }

    public IndexedData copy(){
        IndexedData other = new IndexedData();
        other.list = new HashMap<>(this.list);
        return other;
    }


    public void add(int idx, Integer val) {
        list.put(idx,val) ;
    }

    public void remove(int idx){
        list.remove(idx);
    }

    public int size() {
        return list.size();
    }
}
