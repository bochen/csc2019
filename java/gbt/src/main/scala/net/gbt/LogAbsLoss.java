package net.gbt;

import org.javatuples.Pair;

import java.util.*;
import java.util.stream.Collectors;

public class LogAbsLoss extends Loss {

    private static final double MIN_FLOAT = 1e-9;

    public static double calLogAbsError(List<Double> ytrue, List<Double> ypred, List<Double> weights) {
        double subweights = ArrayUtils.sum(weights);
        List<Double> err = ArrayUtils.subtract(ytrue, ypred).stream().map(o -> Math.abs(o)).map(o -> o < 1e-9 ? 1e-9 : o).map(o -> Math.log(o)).collect(Collectors.toList());
        assert weights.size() == err.size();
        double s = 0;
        for (int i = 0; i < err.size(); i++) {
            s += weights.get(i) * err.get(i);
        }
        return s / subweights;
    }

    public static double calLogAbsError(List<Double> ytrue, List<Double> ypred) {
        return calLogAbsError(ytrue, ypred, ArrayUtils.newDoubleList(ytrue.size(), 1.0));
    }

    static class ValWeight {
        double value;
        double weight;

        public ValWeight(double value, double weight) {
            this.value = value;
            this.weight = weight;
        }

        public double getValue() {
            return value;
        }
    }

    @Override
    public Pair nodeEvaluateMSE(List<Double> y, double reg_lambda) throws Exception {
        throw new Exception("NA");
    }


    @Override
    public Pair nodeEvaluateLogAbs(List<Double> y, List<Double> weights, double defaultPoint) {
        assert y.size() == weights.size();
        List<ValWeight> data = makeSortedData(y, weights);
        List<Double> points = makeCandidatePoints(data);
        points.add(defaultPoint);

        double min_score = Double.MAX_VALUE;
        double min_p = 0;
        for (Double point : points) {
            double s = calcLoss(y, weights, point);
            //System.out.println(" "+ s+ " "+ point);
            if (min_score > s) {
                min_score = s;
                min_p = point;
            }
        }

        return new Pair(-min_score, min_p);
    }

    public static double calcLoss(List<Double> y, Double output) {

        return calcLoss(y, ArrayUtils.newDoubleList(y.size(), 1.0), output);
    }

    public static double calcLoss(List<Double> y, List<Double> weights, Double output) {
        double s = 0;
        for (int i = 0; i < y.size(); i++) {
            double err = Math.abs(y.get(i) - output);
            if (err < MIN_FLOAT) {
                err = MIN_FLOAT;
            }
            s += Math.log(err) * weights.get(i);
        }
        return s;
    }


    public static List<Double> makeUnweightedCandidatePoints(List<Double> y) {
        List<ValWeight> data = makeSortedData(y, ArrayUtils.newDoubleList(y.size(), 1.0));
        return makeCandidatePoints(data);
    }

    public static List<Double> makeWeightedCandidatePoints(List<Double> y, List<Double> weights) {
        List<ValWeight> data = makeSortedData(y, weights);
        return makeCandidatePoints(data);
    }

    public static List<Double> makeCandidatePoints(List<ValWeight> data) {
        List<Double> points = new ArrayList<>();
        double positiveMin = Double.MAX_VALUE;
        double negativeMax = -Double.MAX_VALUE;
        double sumWeight = data.stream().map(o -> o.weight).reduce((o1, o2) -> o1 + o2).get();
        double cumWeight = 0;
        double median = 0;
        boolean median_found = false;

        for (int i = 0; i < data.size(); i++) {
            ValWeight vw = data.get(i);
            if (vw.value < 0 && negativeMax < vw.value) {
                negativeMax = vw.value;
            }
            if (vw.value > 0 && positiveMin > vw.value) {
                positiveMin = vw.value;
            }

            if (false) {
                if (!median_found) {
                    cumWeight += vw.weight;
                    if (Math.abs(cumWeight - sumWeight / 2.0) < MIN_FLOAT) {
                        median = vw.value;
                        median_found = true;
                    } else if (!median_found && cumWeight > sumWeight / 2.0) {
                        if (i == 0 || i == data.size() - 1) {
                            median = vw.value;
                        } else {
                            ValWeight vw0 = data.get(i - 1);
                            double cumWeight0 = cumWeight - vw.weight;
                            double cumWeight1 = sumWeight - cumWeight0;
                            double a = 0.5 - cumWeight0 / sumWeight + Double.MIN_VALUE;
                            double b = 0.5 - cumWeight1 / sumWeight + Double.MIN_VALUE;
                            if (b >= 0) {
                                double p = a / (a + b);
                                median = (vw0.value * p + vw.value * (1 - p));
                            } else {
                                median = vw.value;
                            }
                        }
                        median_found = true;
                    }
                }
            } else {
                if (!median_found) {
                    cumWeight += vw.weight;
                    if (i == 0) {
                        median = vw.value;
                    } else if (cumWeight > sumWeight / 2.0) {
                        ValWeight vw0 = data.get(i - 1);
                        double cumWeight0 = cumWeight - vw.weight - vw0.weight / 2.0;
                        double cumWeight1 = cumWeight - vw.weight / 2.0;
                        if (sumWeight / 2.0 - cumWeight0 < cumWeight1 - sumWeight / 2.0) {
                            median = vw0.value;
                        } else {
                            median = vw.value;
                        }
                        median_found = true;
                    }
                }
            }
        }

        if (false) {
            if (positiveMin < Double.MAX_VALUE - 1 && positiveMin > MIN_FLOAT) {
                points.add(positiveMin);
            } else {
                points.add(MIN_FLOAT);
            }

            if (negativeMax > -Double.MAX_VALUE + 1 && -negativeMax > MIN_FLOAT) {
                points.add(negativeMax);
            } else {
                points.add(-MIN_FLOAT);
            }
        } else {
            if (false) {
                points.add(MIN_FLOAT);
                points.add(-MIN_FLOAT);
            }
        }

        points.add(median);

        return points;
    }

    private double signLog(double x) {
        return Math.signum(x) * Math.log(Math.abs(x));
    }

    private static List<ValWeight> makeSortedData(List<Double> y, List<Double> weights) {
        List<ValWeight> data = new ArrayList<>(y.size());
        for (int i = 0; i < y.size(); i++) {
            data.add(new ValWeight(y.get(i), weights.get(i)));
        }
        Collections.sort(data, Comparator.comparing(ValWeight::getValue));
        return data;
    }
}
