package net.gbt;

import org.javatuples.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MAELoss extends Loss {
    public static double calMAE(List<Double> ytrue, List<Double> ypred) {
        double v = ArrayUtils.subtract(ypred, ytrue).stream().map(o -> Math.abs(o)).mapToDouble(Double::doubleValue).sum();
        return v / ytrue.size();
    }

    @Override
    public Pair nodeEvaluateMAE(List<Double> y) {
        List<Double> numArray = new ArrayList<>(y);
        Collections.sort(numArray);

        double median;
        if (numArray.size() % 2 == 0)
            median = ((double) numArray.get(numArray.size() / 2)
                    + (double) numArray.get(numArray.size() / 2 - 1)) / 2;
        else
            median = (double) numArray.get(numArray.size() / 2);

        double score = y.stream().map(o -> Math.abs(o - median))
                .mapToDouble(Double::doubleValue).sum();
        return new Pair(-score, median);

    }

    @Override
    public Pair nodeEvaluateMSE(List<Double> y, double reg_lambda) throws Exception {
        return null;
    }

    @Override
    public Pair nodeEvaluateLogAbs(List<Double> y, List<Double> weights, double defaultPoint) throws Exception {
        throw new Exception("NA");
    }


}
