package net.gbt;

import net.gbt.data.DataFrame;
import org.javatuples.Pair;

import java.util.List;

public class MSELoss extends Loss {
    public static double calRMSE(List<Double> ytrue, List<Double> ypred) {
        double v = ArrayUtils.subtract(ypred, ytrue).stream().map(o -> o * o).mapToDouble(Double::doubleValue).sum();
        return Math.sqrt(v / ytrue.size());
    }

    @Override
    public Pair nodeEvaluateMSE(List<Double> y, double lambda) {
        List<Double> grad = calGrad(y);
        List<Double> hess = calHess(y);
        double sumg = -ArrayUtils.sum(grad);

        double sumh = ArrayUtils.sum(hess);
        double w = -sumg / (sumh + lambda);
        return new Pair(0.5 * sumg * sumg / (sumh + lambda), w);

    }

    @Override
    public Pair nodeEvaluateLogAbs(List<Double> y, List<Double> weights, double defaultPoint) throws Exception {
        throw new Exception("NA");
    }


    private List<Double> calGrad(List<Double> y) {
        return ArrayUtils.multiply(y, 2);
    }

    private List<Double> calHess(List<Double> y) {
        return ArrayUtils.newDoubleList(y.size(), 2);
    }


}
