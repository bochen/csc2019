package net.gbt;

import net.gbt.data.Column;
import net.gbt.data.DataFrame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import org.javatuples.Pair;
import org.javatuples.Quartet;

public class RegressionTree {
    private String lossType;
    private int maxDepth;
    private int maxNumLeaves;
    private int minDataInLeaf;
    private Node root;
    private Loss lossCalculator;
    //private List<String> columns;
    private HashMap<String, Node> nodeMap = new HashMap<>();
    private HashMap<String, SplitInfo> nodeSplitInfoMap = new HashMap<>();
    private transient HashMap<String, List<Boolean>> nodeValidFlagsMap = new HashMap<>();
    private double reg_lambda;
    private double reg_gamma;
    private SplitCompareInterface categoricalSplitComparator = (a, b) -> ArrayUtils.equals(a, b);
    private SplitCompareInterface numericalSplitComparator = (a, b) -> ArrayUtils.lessThanOrEquals(a, b);
    private transient MulticoreExecutor multicoreExecutor;
    private double col_sample = 1;
    private double row_sample = 1;
    private List<Double> target = null;

    public RegressionTree() throws Exception {
        this("mse", -1, -1, 0.0, 0.0, 1, 0);
    }

    public RegressionTree(String lossType, int maxDepth, int maxNumLeaves, double reg_lambda, double reg_gamma, int minDataInLeaf, int verbose) throws Exception {
        lossType = lossType.toLowerCase();
        assert reg_lambda >= 0;
        assert reg_gamma >= 0;
        this.maxDepth = maxDepth;
        this.maxNumLeaves = maxNumLeaves;
        this.lossType = lossType;
        this.reg_lambda = reg_lambda;
        this.reg_gamma = reg_gamma;
        this.minDataInLeaf = minDataInLeaf;

        setLossType(lossType);
    }

    public RegressionTree setMinDataInLeaf(int num) {
        this.minDataInLeaf = num;
        return this;
    }


    public int getThreadPoolSize() {
        if (multicoreExecutor == null) {
            return 1;
        } else {
            return multicoreExecutor.getThreadPoolSize();
        }
    }

    public void train(DataFrame traindf) throws Exception {
        if (this.multicoreExecutor != null)
            System.out.println("Use " + getThreadPoolSize() + " threads");
        this.col_sample = col_sample;
        //this.columns = sampleColumns(traindf, 1.0);
        //initial row flags

        this.target = traindf.getTarget();

        List<Boolean> rowValidFlags = sampleRows(traindf, row_sample);
        this.root = createRootNode(this.getTarget(), rowValidFlags, traindf.getWeights());
        expandTree(traindf, root);
    }

    private List<Double> getTarget() {
        return target;
    }

    private List<Boolean> sampleRows(DataFrame df, double sampleRatio) {
        int N = df.size();
        int n = (int) Math.round(N * (1 - sampleRatio));
        assert n >= 0;
        List<Boolean> ret = ArrayUtils.newBooleanList(N, true);
        for (int i = 0; i < n; i++) {
            ret.set(i, false);
        }
        Collections.shuffle(ret);
        return ret;
    }

    private List<String> sampleColumns(DataFrame df, double sampleRatio) {
        List<String> columns = new ArrayList<>();
        for (String colname : df.getColumnNames()) {
            columns.add(colname);
        }
        Collections.shuffle(columns);
        int n = (int) Math.round(columns.size() * sampleRatio);
        List<String> ret = columns.subList(0, n);
        assert (ret.size() > 0);
        return ret;
    }

    private void expandTree(DataFrame traindf, Node node) throws Exception {
        if (maxDepth > 0 && node.getLevel() >= maxDepth) {
            return;
        }
        if (maxNumLeaves > 0 && getNumLeaves() >= maxNumLeaves) {
            return;
        }

        SplitInfo best = findSplit(traindf, node);


        if (best == null || best.getGain() < reg_gamma) {

        } else {

            nodeSplitInfoMap.put(node.getName(), best);

            best.makeTrueFalseChildren(node);

            Node trueChild = node.getTrueChild();
            Node falseChild = node.getFalseChild();
            nodeMap.put(trueChild.getName(), trueChild);
            nodeMap.put(falseChild.getName(), falseChild);


            SplitInfo falseInfo = new SplitInfo(trueChild.getName(), best.getTrueCriterion());
            nodeSplitInfoMap.put(trueChild.getName(), falseInfo);
            SplitInfo trueInfo = new SplitInfo(falseChild.getName(), best.getFalseCriterion());
            nodeSplitInfoMap.put(falseChild.getName(), trueInfo);

            updateRowValidFlags(trueChild, traindf);
            updateRowValidFlags(falseChild, traindf);

            cleanRowValidFlags();

            expandTree(traindf, trueChild);
            expandTree(traindf, falseChild);
        }

    }

    private void updateRowValidFlags(Node node, DataFrame traindf) throws Exception {
        List<Boolean> parentFlags = getRowValidFlags(node.getParent());
        String colName = node.getSplitFeature();
        Column col = traindf.getColumn(colName);
        List<Integer> val = col.getValuesAsList();
        List<Boolean> thisFlags = node.calcValidFlags(val, col.isCategory());
        thisFlags = ArrayUtils.and(parentFlags, thisFlags);
        nodeValidFlagsMap.put(node.getName(), thisFlags);
    }

    public Node createRootNode(List<Double> target, List<Boolean> rowValidFlags, List<Double> weights) throws Exception {
        Pair a = null;
        if (isLossMSE()) {
            a = lossCalculator.nodeEvaluateMSE(target, this.reg_lambda);
        } else if (isLossLogAbs()) {
            a = lossCalculator.nodeEvaluateLogAbs(target, weights, 0.0);
        } else if (isLossMAE()) {
            a = lossCalculator.nodeEvaluateMAE(target);
        }
        double thisCriterion = (double) a.getValue0();
        double thisOutput = (double) a.getValue1();
        Node node = new Node(thisOutput);
        nodeMap.put(node.getName(), node);
        nodeValidFlagsMap.put(node.getName(), rowValidFlags);

        SplitInfo info = new SplitInfo(node.getName(), thisCriterion);
        nodeSplitInfoMap.put(node.getName(), info);

        return node;
    }

    private boolean isLossMAE() {
        return this.lossType.equals("mae");
    }

    private boolean isLossLogAbs() {
        return this.lossType.equals("logabs");
    }

    private boolean isLossMSE() {
        return this.lossType.equals("mse");
    }


    public void cleanRowValidFlags() {
        nodeValidFlagsMap.entrySet().removeIf(entry -> !nodeMap.get(entry.getKey()).isLeave());
    }


    private SplitInfo getSplitInfo(Node node) {
        SplitInfo a = nodeSplitInfoMap.get(node.getName());
        assert a != null;
        return a;
    }

    private List<Boolean> getRowValidFlags(Node node) {
        List<Boolean> a = nodeValidFlagsMap.get(node.getName());
        assert a != null;
        return a;
    }

    private int getNumLeaves() {
        return root.getNumLeaves();
    }

    private SplitInfo findSplit(DataFrame traindf, Node node) throws Exception {
        SplitInfo best = null;
        List<String> columns = sampleColumns(traindf, this.col_sample);
        if (this.multicoreExecutor == null) {
            for (String colname : columns) {
                //System.err.println("processing column " + colname);
                SplitInfo info = findSplit(traindf, node, colname);
                if (info != null) {
                    if (best == null || best.getGain() < info.getGain()) {
                        best = info;
                    }
                }
            }
        } else {
            List<SplitInput> splitInputs = new ArrayList<>();
            List<Boolean> flags = getRowValidFlags(node);
            List<Double> y = ArrayUtils.filterDouble(this.getTarget(), flags);
            List<Double> weights = ArrayUtils.filterDouble(traindf.getWeights(), flags);
            double parentOutput = node.getOutput();
            for (String colname : columns) {
                Column col = traindf.getColumn(colname);
                List<Integer> x = ArrayUtils.filterInteger(col.getValuesAsList(), flags);
                List<SplitInput> inputs = findSplitInputs(colname, x, y, col.isCategory(), weights, parentOutput);
                splitInputs.addAll(inputs);
            }

            System.err.println("#column=" + columns.size() + ", #splitInputs=" + splitInputs.size());
            Quartet bestCriterion = findBestSplitParallel(splitInputs);


            if (bestCriterion != null) {
                double thisCriterion = getSplitInfo(node).getParentCriterion();
                best = new SplitInfo((String) bestCriterion.getValue3(), thisCriterion, (Pair) bestCriterion.getValue0(),
                        (Integer) bestCriterion.getValue1(), (Pair) bestCriterion.getValue2());
            }
        }
        //System.err.println("At node "+node.getName()+", best split: "+best);
        return best;
    }

    private Quartet findBestSplitParallel(List<SplitInput> splitInputs) throws Exception {
        if (false) {
            Quartet bestCriterion = null;
            double best_sumcriterion = -Double.MAX_VALUE;
            for (SplitInput splitInput : splitInputs) {
                Quartet a = findSplitSingle(splitInput.colname, splitInput.x, splitInput.y, splitInput.splitValue,
                        splitInput.parentOutput, splitInput.weights, splitInput.comparator);
                if (a != null) {
                    //System.out.println("BBB "+best_sumcriterion);
                    Pair tmp = (Pair) a.getValue0();
                    double sumcriterion = (double) tmp.getValue0() + (double) tmp.getValue1();
                    if (bestCriterion == null) {
                        bestCriterion = a;
                        best_sumcriterion = sumcriterion;
                    } else {
                        if (best_sumcriterion < sumcriterion) {
                            bestCriterion = a;
                            best_sumcriterion = sumcriterion;
                        }
                    }
                }
            }
            return bestCriterion;
        } else {
            Quartet bestCriterion = null;
            double best_sumcriterion = -Double.MAX_VALUE;
            List<SplitTask> tasks = new ArrayList<>(splitInputs.size());
            for (int j = 0; j < splitInputs.size(); j++) {
                if (true) {
                    if (Math.random() < 0.5) {
                        tasks.add(new SplitTask(splitInputs.get(j)));
                    }
                } else {
                    tasks.add(new SplitTask(splitInputs.get(j)));
                }
            }
            Collections.shuffle(tasks);

            for (Quartet a : multicoreExecutor.run(tasks)) {
                if (a != null) {
                    //System.out.println("BBB "+best_sumcriterion);
                    Pair tmp = (Pair) a.getValue0();
                    double sumcriterion = (double) tmp.getValue0() + (double) tmp.getValue1();
                    if (bestCriterion == null) {
                        bestCriterion = a;
                        best_sumcriterion = sumcriterion;
                    } else {
                        if (best_sumcriterion < sumcriterion) {
                            bestCriterion = a;
                            best_sumcriterion = sumcriterion;
                        }
                    }
                }
            }

            return bestCriterion;
        }
    }

    public int getNumThread() {
        if (this.multicoreExecutor == null) {
            return 1;
        } else {
            return multicoreExecutor.getNumThread();
        }

    }

    public RegressionTree setMultiCoreExecutor(MulticoreExecutor executor) {
        this.multicoreExecutor = executor;
        return this;
    }

    public RegressionTree setLossType(String lossType) throws Exception {
        lossType = lossType.toLowerCase();
        this.lossType = lossType;
        if (lossType.equals("mse")) {
            this.lossCalculator = new MSELoss();
        } else if (lossType.equals("logabs")) {
            this.lossCalculator = new LogAbsLoss();
        } else if (lossType.equals("mae")) {
            this.lossCalculator = new MAELoss();
        } else {
            throw new Exception("not supported loss: " + lossType);
        }
        return this;
    }

    public RegressionTree setColSample(double v) {
        this.col_sample = v;
        return this;
    }


    class SplitTask implements Callable<Quartet> {


        private final SplitInput splitInput;

        SplitTask(SplitInput input) {
            this.splitInput = input;
        }

        @Override
        public Quartet call() {
            try {
                return findSplitSingle(splitInput.colname, splitInput.x, splitInput.y,
                        splitInput.splitValue, splitInput.parentOutput, splitInput.weights, splitInput.comparator);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public int getNumberNode() {
        return this.nodeMap.size();
    }

    public int getNumberLeave() {
        int s = 0;
        for (Node n : nodeMap.values()) {
            if (n.isLeave()) {
                s++;
            }
        }
        return s;
    }

    public void showNodes() {
        nodeMap.values().forEach(o -> System.out.println(o));
    }

    public void showLeaveNodeValue() {
        List<Integer> a = nodeMap.values().stream().
                filter(o -> o.isLeave()).map(o -> o.getSplitValue()).collect(Collectors.toList());
        Collections.sort(a);
        System.out.println(a);
    }


    public int getNumberSplitInfo() {
        return nodeSplitInfoMap.size();
    }

    public int getNumberValidFlag() {
        return nodeValidFlagsMap == null ? 0 : nodeValidFlagsMap.size();
    }

    public void clearValidFlag() {
        nodeValidFlagsMap.entrySet().removeIf(entry -> true);
        nodeValidFlagsMap = null;
        this.target = null;
    }

    public Integer getNumLevel() {
        return nodeMap.values().stream().map(o -> (o.isLeave() ? o.getLevel() : -1)).reduce(Integer::max).get();
    }

    public List<Integer> getNumLevels() {
        List<Integer> a = nodeMap.values().stream().filter(o -> o.isLeave()).map(o -> o.getLevel()).collect(Collectors.toList());
        Collections.sort(a);
        return a;
    }

    public RegressionTree setMaxNumLeave(int i) {
        this.maxNumLeaves = i;
        return this;
    }

    public RegressionTree setMaxDepth(int i) {
        this.maxDepth = i;
        return this;
    }

    public List<Double> predict(DataFrame traindf) throws Exception {
        return root.predict(traindf);
    }

    public Loss getLossCalculator() {
        return this.lossCalculator;
    }


    interface SplitCompareInterface {

        List<Boolean> operation(List<Integer> a, int b);
    }

    private List<SplitInput> findSplitInputs(String colname, List<Integer> x, List<Double> y,
                                             boolean category, List<Double> weights, double parentOutput) {
        List<SplitInput> inputs = new ArrayList<>();
        for (int featV : ArrayUtils.unique(x)) {
            SplitInput si = new SplitInput(colname, x, y, featV,
                    category ? this.categoricalSplitComparator : this.numericalSplitComparator);
            si.weights = weights;
            si.parentOutput = parentOutput;
            inputs.add(si);
        }
        return inputs;
    }

    private SplitInfo findSplit(DataFrame traindf, Node node, String colname) throws Exception {
        List<Boolean> flags = getRowValidFlags(node);
        //System.out.println("At node "+node.getName()+", #data: "+ArrayUtils.countBool(flags));
        Column column = traindf.getColumn(colname);
        List<Integer> x = column.getValuesAsList();
        List<Double> y = this.getTarget();
        x = ArrayUtils.filterInteger(x, flags);
        y = ArrayUtils.filterDouble(y, flags);
        double parentOutput = node.getOutput();
        List<Double> weights = traindf.getWeights();
        weights = ArrayUtils.filterDouble(weights, flags);
        SplitCompareInterface splitCompareInterface;
        if (column.isCategory()) {
            splitCompareInterface = this.categoricalSplitComparator;
        } else {
            splitCompareInterface = this.numericalSplitComparator;
        }
        Quartet bestSplit;
        bestSplit = findSplit(colname, x, y, parentOutput, weights, splitCompareInterface);

        if (bestSplit == null) {
            return null;
        } else {
            double thisCriterion = getSplitInfo(node).getParentCriterion();
            return new SplitInfo(colname, thisCriterion, (Pair) bestSplit.getValue0(),
                    (Integer) bestSplit.getValue1(), (Pair) bestSplit.getValue2());
        }
    }

    private Quartet findSplitSingle(String colname, List<Integer> x, List<Double> y, int featV, double parentOutput, List<Double> weights, SplitCompareInterface splitCompareInterface) throws Exception {
        List<Boolean> trueFlags = splitCompareInterface.operation(x, featV);
        List<Boolean> falseFlags = ArrayUtils.not(trueFlags);
        if (ArrayUtils.countBool(trueFlags) < minDataInLeaf || ArrayUtils.countBool(falseFlags) < minDataInLeaf) {
            return null;
        }
        List<Double> truey = ArrayUtils.filterDouble(y, trueFlags);
        List<Double> falsey = ArrayUtils.filterDouble(y, falseFlags);
        List<Double> trueWeights = ArrayUtils.filterDouble(weights, trueFlags);
        List<Double> falseWeights = ArrayUtils.filterDouble(weights, falseFlags);

        Pair trueCriterionAndOutput = null;
        Pair falseCriterionAndOutput = null;

        if (this.isLossMSE()) {
            trueCriterionAndOutput = lossCalculator.nodeEvaluateMSE(truey, reg_lambda);
            falseCriterionAndOutput = lossCalculator.nodeEvaluateMSE(falsey, reg_lambda);
        } else if (this.isLossLogAbs()) {
            trueCriterionAndOutput = lossCalculator.nodeEvaluateLogAbs(truey, trueWeights, parentOutput);
            falseCriterionAndOutput = lossCalculator.nodeEvaluateLogAbs(falsey, falseWeights, parentOutput);
        } else if (this.isLossMAE()) {
            trueCriterionAndOutput = lossCalculator.nodeEvaluateMAE(truey);
            falseCriterionAndOutput = lossCalculator.nodeEvaluateMAE(falsey);
        } else {

        }
        Pair criterion = new Pair(trueCriterionAndOutput.getValue0(), falseCriterionAndOutput.getValue0());
        Pair outputs = new Pair(trueCriterionAndOutput.getValue1(), falseCriterionAndOutput.getValue1());
        return new Quartet(criterion, featV, outputs, colname);
    }

    private Quartet findSplit(String colname, List<Integer> x, List<Double> y, double parentOutput, List<Double> weights, SplitCompareInterface splitCompareInterface) throws Exception {
        double best_sumcriterion = -Double.MAX_VALUE; //the bigger the better
        Integer bestSplitValue = null;
        Pair bestOutput = null;
        Pair bestCriterion = null;

        for (int featV : x) {
            List<Boolean> trueFlags = splitCompareInterface.operation(x, featV);
            List<Boolean> falseFlags = ArrayUtils.not(trueFlags);
            if (ArrayUtils.countBool(trueFlags) < minDataInLeaf || ArrayUtils.countBool(falseFlags) < minDataInLeaf) {
                continue;
            }
            List<Double> truey = ArrayUtils.filterDouble(y, trueFlags);
            List<Double> falsey = ArrayUtils.filterDouble(y, falseFlags);
            List<Double> trueWeights = ArrayUtils.filterDouble(weights, trueFlags);
            List<Double> falseWeights = ArrayUtils.filterDouble(weights, falseFlags);

            Pair trueCriterionAndOutput = null;
            Pair falseCriterionAndOutput = null;
            if (this.isLossMSE()) {
                trueCriterionAndOutput = lossCalculator.nodeEvaluateMSE(truey, reg_lambda);
                falseCriterionAndOutput = lossCalculator.nodeEvaluateMSE(falsey, reg_lambda);
            } else if (this.isLossLogAbs()) {
                trueCriterionAndOutput = lossCalculator.nodeEvaluateLogAbs(truey, trueWeights, parentOutput);
                falseCriterionAndOutput = lossCalculator.nodeEvaluateLogAbs(falsey, falseWeights, parentOutput);
            } else if (this.isLossMAE()) {
                trueCriterionAndOutput = lossCalculator.nodeEvaluateMAE(truey);
                falseCriterionAndOutput = lossCalculator.nodeEvaluateMAE(falsey);
            } else {

            }
            double sumcriterion = (Double) trueCriterionAndOutput.getValue0() + (Double) falseCriterionAndOutput.getValue0();
            Pair criterion = new Pair(trueCriterionAndOutput.getValue0(), falseCriterionAndOutput.getValue0());
            Pair outputs = new Pair(trueCriterionAndOutput.getValue1(), falseCriterionAndOutput.getValue1());
            if (best_sumcriterion < sumcriterion) {
                best_sumcriterion = sumcriterion;
                bestSplitValue = featV;
                bestOutput = outputs;
                bestCriterion = criterion;
            }
        }

        if (bestCriterion == null) {
            return null;
        } else {
            Quartet ret = new Quartet(bestCriterion, bestSplitValue, bestOutput, colname);
            return ret;
        }

    }

    private double getDefaultInitialScore(List<Double> target) {
        if (lossType == "mse") {
            double mean = ArrayUtils.mean(target);
            return mean;

        } else {
            return 0.0;
        }
    }

    @Override
    public String toString() {
        return "RegressionTree{" +
                "lossType='" + lossType + '\'' +
                ", maxDepth=" + maxDepth +
                ", maxNumLeaves=" + maxNumLeaves +
                ", minDataInLeaf=" + minDataInLeaf +
                ", root=" + root +
                ", lossCalculator=" + lossCalculator +
                ", #nodeMap=" + nodeMap.size() +
                ", #nodeSplitInfoMap=" + nodeSplitInfoMap.size() +
                ", #nodeValidFlagsMap=" + nodeValidFlagsMap.size() +
                ", reg_lambda=" + reg_lambda +
                ", reg_gamma=" + reg_gamma +
                '}';
    }

    private class SplitInput {
        final String colname;
        final int splitValue;
        final List<Integer> x;
        final List<Double> y;
        private final SplitCompareInterface comparator;
        public double parentOutput;
        public List<Double> weights;

        public SplitInput(String colname, List<Integer> x, List<Double> y, int splitValue, SplitCompareInterface splitCompareInterface) {
            this.colname = colname;
            this.splitValue = splitValue;
            this.x = x;
            this.y = y;
            this.comparator = splitCompareInterface;
        }
    }
}
